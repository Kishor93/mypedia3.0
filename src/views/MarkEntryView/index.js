import React, {Component} from 'react';
import { connect } from 'react-redux';
import ErrorHandle from '../../components/features/ErrorHandle/index';
import {getStudentData} from '../../state/studentData/studentAction';
import MarkEntry from "../../components/widget/MarkEntry/index";
import './index.css';

class MarkEntryView extends Component {


    render() {
        return (
           <div className="tab">
                <div className="container-fluid markentry__tabsection">
                    <div className="row">
                        <div className="col-12">
                            <MarkEntry/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
  return {studentReducer:typeof state.studentReducer!=='undefined'?state.studentReducer:{},APIResponseStatus:state.studentReducer.APIResponseStatus};
};
const mapdispatchtoprops = { getStudentData };
export default connect(mapStateToProps,mapdispatchtoprops)(ErrorHandle(MarkEntryView));

