import React, {Component} from 'react';
import MarkApproval from "../../components/widget/MarkApproval/index.js";

class MarkApprovalView extends Component {
    constructor() {
        super();
        this.state = {};
    }

    render() {
        return (
            <div className="tab">
                <div className="container-fluid markentry__tabsection">
                    <div className="row">
                        <div className="col-12">
                            <MarkApproval />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MarkApprovalView;

