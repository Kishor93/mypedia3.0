import React, {Component} from 'react';
import AssessmentMap from "../../components/widget/AssessmentMap";

class AssessmentMapView extends Component {
    constructor() {
        super();
        this.state = {};
    }

    render() {
        return (
            <div className="tab">
                <div className="container-fluid markentry__tabsection">
                    <div className="row">
                        <div className="col-12">
                            <AssessmentMap />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AssessmentMapView;
