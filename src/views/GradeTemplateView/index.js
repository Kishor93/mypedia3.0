import React, { Component } from 'react';
import ErrorHandle from '../../components/features/ErrorHandle/index';
import GradeTemplate from "../../components/widget/GradeTemplate/index";
import './index.css';

class GradeTemplateView extends Component {
    render() {
        return (
            <div className="tab">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12 p-0">
                             	<GradeTemplate />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ErrorHandle(GradeTemplateView);

