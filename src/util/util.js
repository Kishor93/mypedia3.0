// Read the Mark Entry excel(xlsx) and validate the rows
import * as XLSX from 'xlsx'
import _ from 'lodash'
import JSZip from 'jszip';
import { saveAs } from 'file-saver';
import axios from 'axios';

export class Validation {
  constructor(row) {
    this.excelRows = row;
    this.data = [];
    this.sheetHeader = [];
    this.sheetTmpHeader = [];

  }

  validateMarkentryRecordsNew(schooldata) {
    try {
      let exceldata = this.data[0];
      let sheetHeader = this.sheetHeader[0];
      let successList = [], faliureList = [], school_headers = [], successListobj = {}, faliureListobj = {};
      let schoollColumn = schooldata.successList.columns;
      const school_Overall_users = schooldata.successList.data.concat(schooldata.failureList.data);
      //exceldata = _.uniqBy(exceldata, 'UserName');//console.log(exceldata);return false;
      if (typeof exceldata === 'undefined') {
        return 'Error:  Sheet has no data';
      } else if (exceldata.length > (school_Overall_users.length)) {
        return 'Error: Excel Sheet has more student data than Actuals!';
      } else if (exceldata.length < (school_Overall_users.length)) {
        return 'Error: Excel Sheet has less student data than Actuals!';
      }
      //let sl = {};
      const s_h = _.find(schoollColumn, function (o) {
        if (o.Header !== 'Total mark' && o.Header !== 'Grade') { // excel sheet wont have these columns
          if (o.Header == 'User Name') {
            o.Header = 'UserName'
          }
          school_headers.push(o.Header);
        }
        //sl[o.accessor] = {};
      })

      if (_.isEqual(sheetHeader.sort(), school_headers.sort()) === false) {
        return 'Error: Headers are not matching';
      }

      var successindex = 0;
      var failureindex = 0;
      let excelusernames = [];
      var err = '';
      // exceldata.forEach(function (sheet_value, sheet_key) {
      for (var sheet_value of exceldata) {
        if (err) break;
        var isFail = false;
        let rubricarray = [];
        let sl = {};
        sheet_value.RollNo = sheet_value['Roll No'];
        sheet_value = _.omit(sheet_value, ['Roll No']);
        schoollColumn.forEach(function (col_val, col_ind) {

          if (col_val.Header === 'Name') { // name field validation
            sl.name = {}
            sl.name.name = sheet_value.Name;
            sl.name.image = "";
            if (typeof sheet_value.Name == 'undefined') {
              isFail = true;
              err = "Error: Empty Name Found in Excel sheet ";
            }
          } else if (col_val.Header === 'Roll No') { //roll no validation
            sl.rollNo = {}
            sl.rollNo.value = sheet_value.RollNo;
            if (typeof sheet_value.RollNo == 'undefined') {
              err = "Error: Empty Roll No Found in Excel sheet ";
            }
            // else if(isNaN(sheet_value.RollNo)){
            //   err = "Error: Invalid Roll No Found in Excel sheet ";
            // }
          } else if (col_val.Header === 'UserName') {
            sl.userName = {};
            sl.userName.value = sheet_value.UserName;
            if (typeof sheet_value.UserName == 'undefined') {
              isFail = true;
              err = "Error: Empty User Name Found in Excel sheet ";
            } else { // chk username available or not
              let findname = '';
              let a = _.find(school_Overall_users, function (o) {
                // return o.userName.value === sheet_value.UserName;
                if (o.userName.value === sheet_value.UserName) {
                  findname = o.name.name;
                  return findname;
                }
              })
              // if (typeof a == 'undefined') {
              //   isFail = true;
              //   err = "Error: Invalid UserName '" + sheet_value.UserName + "' entered in Excel Sheet";
              //   //  break;
              // }

              if (findname) {
                if (findname.toLowerCase() !== sheet_value.Name.toLowerCase()) {
                  err = "Error: Student Name is not matching for the User '" + sheet_value.UserName + "'";
                }
              } else {
                err = "Error: Invalid User Name '" + sheet_value.UserName + "' entered in Excel Sheet";
              }
            }
          }

          else if (col_val.Header != 'Total mark' && col_val.Header != 'Grade') {//  remaining all roobrics validation
            rubricarray.push(col_val.Header);
            sl[col_val.Header] = {};
            sl[col_val.Header].id = school_Overall_users[0][col_val.accessor].id;
            sl[col_val.Header].value = sheet_value[col_val.Header];
            let minMark = col_val.Validation.condition.min;
            let maxMark = col_val.Validation.condition.max;
            if (typeof sheet_value[col_val.Header] === 'undefined') {
              isFail = true;
            }
            else if (isNaN(sheet_value[col_val.Header]) && sheet_value[col_val.Header] !== 'AB' && sheet_value[col_val.Header] !== 'NT') {
              isFail = true;
            } else if ((sheet_value[col_val.Header] > maxMark || sheet_value[col_val.Header] < minMark) && (sheet_value[col_val.Header] !== 'AB' && sheet_value[col_val.Header] !== 'NT')) {
              isFail = true;
            }

          } else { //
            sl.totalScore = {};
            sl.grade = {};
            sl.totalScore.value = 0;
            sl.grade.value = '';
          }

        })
        // duplicate username check
        if (excelusernames.includes(sheet_value.UserName.toLowerCase())) {
          err = "Error : UserName '" + sheet_value.UserName + "' is duplicated in excel sheet.";
        }
        if (isFail) {
          rubricarray.forEach(function (rubval, rubkey) {
            sl[rubval].index = failureindex;
          })
          faliureList.push(sl);
          excelusernames.push(sheet_value.UserName.toLowerCase());
          failureindex++;
        } else {
          rubricarray.forEach(function (rubval, rubkey) {
            sl[rubval].index = successindex;
          })
          successList.push(sl);
          excelusernames.push(sheet_value.UserName.toLowerCase());
          successindex++;
        }
      }
      // })

      if (!err) {
        successListobj.columns = schoollColumn;
        successListobj.data = successList;
        faliureListobj.columns = schoollColumn;
        faliureListobj.data = faliureList;
        let out = { successList: successListobj, failureList: faliureListobj };
        //console.log(JSON.stringify(out)); //return false;
        return out;
      } else {
        return err;
      }

    } catch (error) {
      alert('Oops! some thing went wrong');
    }


  }


  getDataArray() {
    let excelRow = this.excelRows.target.result;
    var cfb = XLSX.CFB.read(excelRow, { type: 'binary' });
    var wb = XLSX.parse_xlscfb(cfb);
    wb.SheetNames.forEach((sheetName) => {
      var oJS = XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);
      var aoa = XLSX.utils.sheet_to_json(wb.Sheets[sheetName], { header: 1 });
      if (oJS.length !== 0) {
        this.sheetHeader.push(aoa[0]);
        this.data.push(oJS);
      }
    });

  }


}

export function toAddStylesForColumns(columnStyles, studentData) {
  return studentData.columns.forEach((values, i) => {
    studentData.columns[i] = { ...values, ...columnStyles[i] }
  })
};


export function downloadFiles(data) {
  getFile(data).then(function (results) {
    var zip = new JSZip();
    results.map((result, index) => {
      let fileName = data[index].fileName + data[index].ext;
      zip.file(fileName, result.data, { binary: true });
    });
    zip.generateAsync({ type: "blob" })
      .then(function (content) {
        // see FileSaver.js
        saveAs(content, "report.zip");
      });
  }).catch(function (err) {
    console.log(err);
  });
}

function requestAsync(url) {
  return new Promise(function (resolve, reject) {
    axios({
      method: 'post',
      url: url,
      responseType: 'blob',
      data: { "data": "ew0KICAibWV0YWRhdGEiOiBbDQogICAgew0KICAgICAgInNjaG9vbGlkIjogIiIsDQogICAgICAiY2xhc3NfaWQiOiAiIiwNCiAgICAgICJhc3Nlc3NtZW50X2lkIjogIiIsDQogICAgICAiYXBpbmFtZSI6ICJQcmV2aWV3UmVwb3J0VGVtcGxhdGUiLA0KICAgICAgIm1vZHVsZSI6ICJtYXJrRW50cnkiLA0KICAgICAgImFjdGlvbiI6ICJQUkVWSUVXUkVQT1JURE9XTkxPQUQiLA0KICAgICAgInRyYW5zYWN0aW9uc3RhdHVzIjogInJlcXVlc3RlZCINCiAgICB9DQogIF0sDQogICJrZXkiOiAiUkVBQ1RfQVBJX1NURyIsDQogICJkYXRlIjogIjE1NTAyMjkyMzUiLA0KICAidG9rZW4iOiAiIg0KfQ==" }
    }).then((response) => {
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
  });
}


function getFile(endpoints) {
  return Promise.all(endpoints.map(function ({ file }) {
    return requestAsync(file);
  }));
}

export class GridValidations {
  constructor(data) {
    this.data = data;
  }

  validationRangeAndString(value) {
    if (this.data.condition) {
      const { min, max, enumerate } = this.data.condition;
      return typeof value === "string" && isNaN(parseInt(value)) ? enumerate.indexOf(value) !== -1 ? true : false : min <= value && max >= value ? true : false;
    }
  }

  validationRange(value) {
    if (this.data.condition) {
      const { min, max } = this.data.condition;
      return min <= value && max >= value ? true : false;
    }
  }

  validateMarks(e) {
    let targetDom = e.target;
    let val = isNaN(targetDom.value) ? targetDom.value : Math.round(targetDom.value);
    let valid = true;
    if (!this.validationRangeAndString(val) && targetDom.className.indexOf('bg__warninginput') === -1) {
      targetDom.classList.add('bg__warninginput');
      valid = false;
    } else if (this.validationRangeAndString(val) && targetDom.className.indexOf('bg__warninginput') !== -1) {
      targetDom.classList.remove('bg__warninginput');
      valid = true;
    }
    if (targetDom.className.indexOf('bg__warninginput') !== -1) {
      valid = false;
    }
    return valid;

  }

}

// Functions for Aggregation of Marks starts


export function calculateSumOfMarks(marksData, totalMarkProps, gradeList, rubricProps) {
  let i = 0;
  let dataArr = [];
  for (let key in marksData) {
    if (marksData.hasOwnProperty(key)) {
      dataArr[i] = marksData[key];
      let totalScore = 0.00;
      for (let key1 in dataArr[i]) {
        if (dataArr[i].hasOwnProperty(key1) && typeof rubricProps[key1] !== 'undefined' && rubricProps[key1]['includeInTotal'] && typeof dataArr[i][key1]['index'] !== 'undefined') {
          let tmpScore = !isNaN(dataArr[i][key1]['value']) ? parseFloat(dataArr[i][key1]['value']) : 0.00;
          if (rubricProps[key1]['isConvertable'] && rubricProps[key1]['convertTo'] > 0)
            tmpScore = tmpScore / parseFloat(rubricProps[key1]['maxScore']) * rubricProps[key1]['convertTo'];
          //console.log('tmpScore',tmpScore);
          totalScore += rubricProps[key1]['roundMethod'] == 'ceil' ? Math.ceil(tmpScore) : rubricProps[key1]['roundMethod'] = 'floor' ? Math.floor(tmpScore) : parseFloat(tmpScore).toFixed(2);
          // console.log('TS',rubricProps[key1]['roundMethod'],totalScore);
        }
        else if (dataArr[i].hasOwnProperty(key1) && key1 == 'totalScore' && typeof dataArr[i][key1]['index'] !== 'undefined') {
          totalScore = parseFloat(totalScore).toFixed(2);
          if (totalMarkProps['isConvertable'] && totalMarkProps['convertTo'] > 0)
            totalScore = totalScore / parseFloat(totalMarkProps['maxScore']) * totalMarkProps['convertTo'];
          // console.log('TS1Before', totalMarkProps['roundMethod'],totalScore);
          totalScore = totalMarkProps['roundMethod'] == 'ceil' ? Math.ceil(totalScore) : totalMarkProps['roundMethod'] == 'floor' ? Math.floor(totalScore) : parseFloat(totalScore).toFixed(2);
          //console.log('TS1After', totalMarkProps['roundMethod'],totalScore,'ceil:',Math.ceil(totalScore));
          dataArr[i][key1]['value'] = totalScore;
          for (let key3 in gradeList) {
            if (gradeList.hasOwnProperty(key3) && (totalScore == 0 || gradeList[key3].minRange < totalScore) && gradeList[key3].maxRange >= totalScore) {
              dataArr[i]['grade']['value'] = key3;
              break;
            }
          }
        }
      }
      i++;
    }
  }
  return dataArr;
}

