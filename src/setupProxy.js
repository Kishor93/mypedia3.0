const proxy = require('http-proxy-middleware');
module.exports = function(app){
app.use(proxy("/SchoolNetTeacherPortal/", {
    target: "https://id-qa.pearsonmypedia.com",
    changeOrigin: true,
    onProxyReq(proxyReq) {
     proxyReq.setHeader("env", "localhost");
    },
    logLevel: "debug"
  }))
}

