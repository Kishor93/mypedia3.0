export default ({ dispatch, getState }) => {
  return next => action => {
    const {
      types,
      callAPI,
      shouldCallAPI,
      payload = {}
    } = action

    if (!types) {
      // Normal action: pass it on
      return next(action)
    }

    const [successType, failureType,fetchType] = types
    
    if (shouldCallAPI && !shouldCallAPI(getState())) {
          return dispatch({type:fetchType})
    }

    return callAPI().then(
      response =>{
        if(response.data.code == 200) {
            dispatch({...payload, response, type: successType})
        }else{
            console.log(response.data);
            dispatch({type: failureType})
        }
        },
      error =>dispatch({...payload,error,type: failureType})
    )
  }
}