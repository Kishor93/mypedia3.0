import axios from 'axios';
export function getStudentData(lineItemId) {
  return {
    types: ["GET_STUDENTDATA_SUCCESS", "GET_STUDENTDATA_FAILURE", "GET_STUDENTDATA_STORE"],
    shouldCallAPI: (state) => true,
    callAPI: () => axios.get('/mockData/studentsList.json'),
    //callAPI: () => axios.get(`/SchoolNetTeacherPortal/API/serviceprovider?class_id=${window.classSectionId}&ins_id=${window.schoolId}&line_item_id=${lineItemId}&opt=json&apiname=DownloadeTemplate`),
    payload: null
  }
}

export function uploadedDataResponse(data){
  return {
    type: "BULK_UPLOADED_DATA_RESPONSE",
    payload: data
  }
}

export function bulkUploadCompleteFalse(data){
  return {
    type: "BULK_UPLOAD_COMPLETE_FALSE",
    payload: data
  }
}

export function bulkUploadCompleteStatus(data){
  return {
    type: "BULK_UPLOAD_COMPLETE_STATUS",
    payload: data
  }
}



export function getStudentData1(lineItemId) {
  return {
    types: ["GET_STUDENTDATA_SUCCESS", "GET_STUDENTDATA_FAILURE", "GET_STUDENTDATA_STORE"],
    shouldCallAPI: (state) => true ,
    callAPI: () => axios.get(`/SchoolNetTeacherPortal/API/serviceprovider?class_id=${window.classSectionId}&ins_id=${window.schoolId}&line_item_id=${lineItemId}&opt=json&apiname=DownloadeTemplate`),
    //callAPI: () => axios.get(proxyurl+'https://stg.pearsonmypedia.com/SchoolNetTeacherPortal/API/serviceprovider?class_id=3245&ins_id=427&line_item_id=3274&opt=json&apiname=DownloadeTemplate'),
    payload: null
  }
}

export  function getDataForBulkUpload(lineItemId) {
  return {
    types: ["GET_STUDENTBULKDATA_SUCCESS", "GET_STUDENTBULKDATA_FAILURE", "GET_STUDENTBULKDATA_STORE"],
  //  shouldCallAPI: (state) => true,
    shouldCallAPI: (state) => { return !(typeof state.studentReducer.sucessStudentMarkList !== 'undefined' && typeof state.studentReducer.failureStudentMarkList !== 'undefined') },
   //callAPI: () => axios.get(`/SchoolNetTeacherPortal/API/serviceprovider?class_id=${window.classSectionId}&ins_id=${window.schoolId}&line_item_id=${lineItemId}&opt=json&apiname=DownloadeTemplate`),
  callAPI: () => axios.get('/mockData/bulkuplloadorg1.json'),
    payload: null
  }
}

export function updateStudentData(sucessStudentMarkList,failureStudentMarkList) {
  return {
    type: "UPDATE_STUDENT_DATA",
    payload: {sucessStudentMarkList: sucessStudentMarkList,failureStudentMarkList:failureStudentMarkList}
  }
}

export function updateSucessStudentListData(data) {
  return {
    type: "UPDATE_STUDENT_SUCESSLIST_DATA",
    payload: data
  }
}

export function updateFailureStudentListData(data) {
  return {
    type: "UPDATE_STUDENT_FAILURELIST_DATA",
    payload: data
  }
}

export function onEditDisableMarkApprovalBtn(data) {
  return {
    type: "ONEDIT_DISABLE_MARK_APPROVAL_BUTTON",
    payload: data
  }
}

export function postStudentData(data) {
  return {
    type: "POST_STUDENT_DATA",
    payload: data
  }
}

export function postStatusFailureListData(data) {
  return {
    type: "POST_STUDENT_FAILURELIST_DATA",
    payload: data
  }
}

export function postStatusSucessListData(data) {
  return {
    type: "POST_STUDENT_SUCESSLIST_DATA",
    payload: data
  }
}

export function updateMarkApprovalStatus(value) {
  return {
    type: "UPDATE_MARKAPPROVAL_STATUS",
    payload: value
  }
}

export function updateMarkApprovalStatusFailureList(value) {
  return {
    type: "UPDATE_MARKAPPROVAL_FAILURELIST_STATUS",
    payload: value
  }
}
export function updateMarkApprovalStatusSucessList(value) {
  return {
    type: "UPDATE_MARKAPPROVAL_SUCESSLIST_STATUS",
    payload: value
  }
}
export function getDataForStudentReport(lineItemId){
       return {
        types:["GET_STUDENTREPORT_SUCCESS","GET_STUDENTREPORT_FAILURE","GET_STUDENTDATA_STORE"],
        shouldCallAPI:(state)=>{return true},
        callAPI:()=>axios.get(`/SchoolNetTeacherPortal/API/serviceprovider?line_item_id=${lineItemId}&apiname=StudentListforStudentReportPage`),
        payload:null
    }
}




