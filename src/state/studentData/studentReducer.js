import _ from 'lodash';
var init = {
    studentList: {},
    studentListMarkApproval: {},
    postStatus: { postData: false, markApproved: false },
    APIfailed: false,
    APIStatus: 'NA',
    bulkuploadcomplete: false,
    studentReport: [],
    APIResponseStatus:true
}
export default (state = init, action) => {
    switch (action.type) {
        case "GET_STUDENTDATA_SUCCESS":
            return { ...state,APIResponseStatus:true, studentList: action.response.data.data}
        case "GET_STUDENTDATA_FAILURE":
            return { ...state,APIResponseStatus:false}
        case "GET_STUDENTDATA_STORE":
            return { ...state };
        case "UPDATE_STUDENT_DATA":
            return { ...state,APIResponseStatus:true, sucessStudentMarkList: action.payload.sucessStudentMarkList,failureStudentMarkList: action.payload.failureStudentMarkList }
        case "UPDATE_STUDENT_SUCESSLIST_DATA":
            return { ...state, sucessStudentMarkList: action.payload }
        case "UPDATE_STUDENT_FAILURELIST_DATA":
            return { ...state, failureStudentMarkList: action.payload }
        case "POST_STUDENT_DATA":
            if (action.payload) { };
            return { ...state, postStatus: { postData: true, markApproved: false } }
        case "POST_STUDENT_SUCESSLIST_DATA":
            if (action.payload) { };
            return { ...state, postStatusSucessList: { postData: true, markApproved: false } }
        case "POST_STUDENT_FAILURELIST_DATA":
            if (action.payload) { };
            return { ...state, postStatusFailureList: { postData: true, markApproved: false } }
        case "BULK_UPLOADED_DATA_RESPONSE":
            let sucessStudentMarkdata = action.payload.successList;
            let failureStudentMarkdata = action.payload.failureList;
            return { ...state,APIStatus:'NA',postStatus: { postData: false, markApproved: false },sucessStudentMarkList: sucessStudentMarkdata, failureStudentMarkList: failureStudentMarkdata }
        case "UPDATE_MARKAPPROVAL_STATUS":
            return { ...state, postStatus: { postData: true, markApproved: true } }
        case "UPDATE_MARKAPPROVAL_SUCESSLIST_STATUS":
            return { ...state, postStatusSucessList: { postData: true, markApproved: true } }
        case "UPDATE_MARKAPPROVAL_FAILURELIST_STATUS":
            return { ...state, postStatusFailureList: { postData: true, markApproved: true } }
        case "GET_STUDENTREPORT_SUCCESS":
            return { ...state, studentReport: action.response.data.data }
        case "GET_STUDENTREPORT_FAILURE":
            return { ...state,APIResponseStatus:false }
        case "GET_STUDENTBULKDATA_SUCCESS":
            let APIStatus = action.response.data.data.status;
            let sucessStudentMarkList = { ...action.response.data.data.successList };
            let failureStudentMarkList = { ...action.response.data.data.failureList };
            let gradeList = { ...action.response.data.data.grades };
            let rubricProps = { ...action.response.data.data.rubricProperties };
            let totalMarkProperties = { ...action.response.data.data.totalMarkProperties };
            return { ...state,totalMarkProperties:totalMarkProperties,rubricProps:rubricProps,gradeList:gradeList,APIResponseStatus:true, APIStatus:APIStatus,studentList: action.response.data.data, sucessStudentMarkList: sucessStudentMarkList, failureStudentMarkList: failureStudentMarkList }
        case "GET_STUDENTBULKDATA_FAILURE":
            return {...state,APIResponseStatus:false}
        case "ONEDIT_DISABLE_MARK_APPROVAL_BUTTON":
            return { ...state, postStatus: { postData: false, markApproved: false } }      
        case "BULK_UPLOAD_COMPLETE_FALSE":
            return { ...state, bulkuploadcomplete: false } 
        case "BULK_UPLOAD_COMPLETE_STATUS":
            return { ...state, bulkuploadcomplete: true }                        
        default:
            return { ...state };
    }
}
