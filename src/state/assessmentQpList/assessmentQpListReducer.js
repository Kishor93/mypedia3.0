var init = {
    assessmentQpSubjects:{},
    assessmentQpList: {},
    APIResponseStatus:true
  }
export default (state = init, action) => {
      switch (action.type) {
        case "GET_ASSESSMENT_QP_LIST_SUCCESS":
            state['assessmentQpList'] = action.response.data.data;
            return {...state,APIResponseStatus:true};
        case "GET_ASSESSMENT_QP_LIST_FAILURE":
              return { ...state,APIResponseStatus:false}
        case "GET_ASSESSMENT_QP_LIST_STORE":
              return {...state};
        case "GET_ASSESSMENT_QP_SUBJECT_SUCCESS":
              state['assessmentQpSubjects'] = action.response.data.data;
              return {...state,APIResponseStatus:true};
          case "GET_ASSESSMENT_QP_SUBJECT_FAILURE":
              return { ...state,APIResponseStatus:false}
          case "GET_ASSESSMENT_QP_SUBJECT_STORE":
              return {...state};
        default:
              return {...state};
      }
  }
