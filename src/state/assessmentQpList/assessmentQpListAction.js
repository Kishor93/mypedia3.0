import axios from 'axios';
export function getAssessmentQpSubject(lineItemId){
    return {
        types:["GET_ASSESSMENT_QP_SUBJECT_SUCCESS","GET_ASSESSMENT_QP_SUBJECT_FAILURE","GET_ASSESSMENT_QP_SUBJECT_STORE"],
        shouldCallAPI:(state)=>{return true},
        callAPI:()=>axios.get('mockData/assessmentQpListSubject.json'),
        payload: null
    }
}
export function getAssessmentQpList(parentLineItemId,lineItemId){
    return {
        types:["GET_ASSESSMENT_QP_LIST_SUCCESS","GET_ASSESSMENT_QP_LIST_FAILURE","GET_ASSESSMENT_QP_LIST_STORE"],
        shouldCallAPI:(state)=>{return true},
        callAPI:()=>axios.get('mockData/assessmentQpList.json'),
        payload: null
    }
}