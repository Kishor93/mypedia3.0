import _ from 'lodash';
var init = {
    gradeColorScaleData: {},
    APIResponseStatus:true
}
export default (state = init, action) => {
    switch (action.type) {
        case "GET_GRADE_COLOR_SUCCESS":
            return { ...state,APIResponseStatus:true, gradeColorScaleData: action.response.data.data}
        case "GET_GRADE_COLOR_FAILURE":
            return { ...state,APIResponseStatus:false}
        case "GET_GRADE_COLOR_STORE":
            return { ...state };              
        default:
            return { ...state };
    }
}
