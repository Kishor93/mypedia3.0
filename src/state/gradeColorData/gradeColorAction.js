import axios from 'axios';
export function getGradeColorScale() {
  return {
    types: ["GET_GRADE_COLOR_SUCCESS", "GET_GRADE_COLOR_FAILURE", "GET_GRADE_COLOR_STORE"],
    shouldCallAPI: (state) => true,
    callAPI: () => axios.get('/mockData/gradeColorDetails.json'),
    //callAPI: () => axios.get(`/SchoolNetTeacherPortal/API/serviceprovider?class_id=${window.classSectionId}&ins_id=${window.schoolId}&line_item_id=${lineItemId}&opt=json&apiname=DownloadeTemplate`),
    payload: null
  }
}
