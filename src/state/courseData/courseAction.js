import axios from 'axios';
export function getCourseData(){
    return {
        types:["GET_COURSEDATA_SUCCESS","GET_COURSEDATA_FAILURE","GET_COURSEDATA_STORE"],
        shouldCallAPI:(state)=>{return !(typeof state.courseReducer.summary['Type'] !== 'undefined'&&state.courseReducer.summary['Type'])},
        callAPI:()=>axios.get(`/mockData/summary.json`),
        payload:null
    }
}
