import _ from 'lodash';
var init = {
    gradeScaleList: {},
    postStatus: { postData: false, markApproved: false },
    APIfailed: false,
    APIStatus: 'NA',
    APIResponseStatus: true
}

export default (state = init, action) => {
    switch (action.type) {
        case "GET_GRADESCALEDATA_SUCCESS":
            return { ...state, APIResponseStatus: true, gradeScaleList: action.response.data.data }
        case "GET_GRADESCALEDATA_FAILURE":
            return { ...state, APIResponseStatus: false }
        case "GET_GRADESCALEDATA_STORE":
            return { ...state };
        case "UPDATE_GRADESCALE_DATA":
            return { ...state, APIResponseStatus: true, sucessStudentMarkList: action.payload.gradeScaleList }
        default:
            return { ...state };
    }
}