import axios from 'axios';

export function getGradeScaleData(lineItemId) {
    return {
        types: ["GET_GRADESCALEDATA_SUCCESS", "GET_GRADESCALEDATA_FAILURE", "GET_GRADESCALEDATA_STORE"],
        shouldCallAPI: (state) => { return (typeof state.gradeAssignReducer.gradeScaleList !== 'undefined' && state.gradeAssignReducer.gradeScaleList) },
        callAPI: () => axios.get('/mockData/gradeScaleList.json'),
        //callAPI: () => axios.get(`/SchoolNetTeacherPortal/API/serviceprovider?class_id=${window.classSectionId}&ins_id=${window.schoolId}&line_item_id=${lineItemId}&opt=json&apiname=DownloadeTemplate`),
        payload: null
    }
}

export function updateGradeScaleData(gradeScaleList) {
    return {
        type: "UPDATE_GRADESCALE_DATA",
        payload: { gradeScaleList: gradeScaleList }
    }
}