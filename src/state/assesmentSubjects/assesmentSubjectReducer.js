var init = {
    assesmentSubjects: {},
    APIResponseStatus:true
  }
export default (state = init, action) => {
      switch (action.type) {
          case "GET_ASSESMENT_SUBJECTS_SUCCESS":
              state['assesmentSubjects'] = action.response.data.data;
              return {...state,APIResponseStatus:true};
        case "GET_ASSESMENT_SUBJECTS_FAILURE":
              return { ...state,APIResponseStatus:false}
        case "GET_ASSESMENT_SUBJECTS_STORE":
              return {...state};
        default:
              return {...state};
      }
  }