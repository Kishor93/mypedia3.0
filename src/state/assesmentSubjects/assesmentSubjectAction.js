import axios from 'axios';
export function getAssesmentSubjects(lineItemId){
    return {
        types:["GET_ASSESMENT_SUBJECTS_SUCCESS","GET_ASSESMENT_SUBJECTS_FAILURE","GET_ASSESMENT_SUBJECTS_STORE"],
        shouldCallAPI:(state)=>{return true},
        callAPI:()=>axios.get(`/mockdata/assesmentSubjects.json`),
        payload: null
    }
}
