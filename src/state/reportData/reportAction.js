import axios from 'axios';
export function getStudentData(){
    return {
        types:["GET_STUDENT_DETAILS_SUCCESS","GET_STUDENT_DETAILS_FAILURE","GET_STUDENT_DETAILS_STORE"],
        shouldCallAPI:(state)=>{return !(typeof state.reportReducer.studentDetails !== 'undefined' && state.reportReducer.studentReducer)},
        callAPI:()=>axios.get('/mockData/studentDetails.json'),
        payload:null
    }
}
export function getTeacherData(lineItemId){
     return {
        types:["GET_TEACHER_DETAILS_SUCCESS","GET_TEACHER_DETAILS_FAILURE","GET_TEACHER_DETAILS_STORE"],
        shouldCallAPI:(state)=>true,
        callAPI:()=>axios.get(`/mockdata/teacherDetails.json`),
        payload:null
    }
}

export function generateReport(genReportReqParams) {
    //console.log('Generate Report actions');
    //console.log('genReportReqParams=>',genReportReqParams);
    //let proxyurl = "https://cors-anywhere.herokuapp.com/";
    //proxyurl = '';
    //let url ='/mockData/generateReport.json';
    let url ='/SchoolNetTeacherPortal/API/serviceprovider';
    return {
        types: ["GENERATE_REPORT_SUCCESS", "GENERATE_REPORT_FAILURE", "GENERATE_REPORT_STORE"],
        shouldCallAPI: null,
        callAPI: () => axios.post(url, 
             {       data:btoa(JSON.stringify(genReportReqParams))        }
        ),
        payload: null
        // type:"GENERATE_REPORT_SUCCESS",
        // payload:{"requestId":"PRC34324325345","code":"200","statusMessage":"Report Generation request Posted in Queue"}
    }
}

export function resetPrintableReport() {
    return {
        type: "RESET_PRINTABLE_REPORT"
    }
}

export function getPrintableReport(printableURL) {
    return {
        types:["GET_PRINTABLE_REPORT_SUCCESS","GET_PRINTABLE_REPORT_FAILURE","GET_PRINTABLE_REPORT_STORE"],
        shouldCallAPI:null,
        callAPI:()=>axios.get(printableURL),
        payload:null
    }
}
