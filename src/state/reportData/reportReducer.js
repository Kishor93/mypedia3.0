var init = {APIResponseStatus:true};
export default (state = init, action) => {
    switch (action.type) {
        case "GET_STUDENT_DETAILS_SUCCESS":
            return {...state,APIResponseStatus:true, studentDetails: action.response.data}
        case "GET_STUDENT_DETAILS_FAILURE":
            return {...state,APIResponseStatus:false}
        case "GET_STUDENT_DETAILS_STORE":
            return {...state};
        case "GET_TEACHER_DETAILS_SUCCESS":
            return {...state, APIResponseStatus:true,teacherDetails: action.response.data}
        case "GET_TEACHER_DETAILS_FAILURE":
            return {...state, APIResponseStatus:false}
        case "GET_TEACHER_DETAILS_STORE":
            return {...state};
        case "GENERATE_REPORT_SUCCESS":
            //console.log('MyOut=>',action.response);
            return {...state, generateReportResponse: action.response.data, APIResponseStatus:true}
        case "GENERATE_REPORT_FAILURE":
            //console.log("Err Out =>");
            return {...state,generateReportResponse: null, APIResponseStatus:false}
        case "GENERATE_REPORT_STORE":
            return {...state};
        case "GET_PRINTABLE_REPORT_SUCCESS":
            return {...state, printableReportResponse: action.response.data, APIResponseStatus:true};
        case "GET_PRINTABLE_REPORT_FAILURE":
            return {...state, printableReportResponse: null, APIResponseStatus:false};
        case "RESET_PRINTABLE_REPORT":
            return {...state, printableReportResponse: null }
        default:
            return {...state};
    }
}
