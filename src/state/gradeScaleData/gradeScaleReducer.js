import _ from 'lodash';
var init = {
    gradeList: {},
    APIResponseStatus:true
}
export default (state = init, action) => {
    switch (action.type) {
        case "GET_GRADE_DATA_SUCCESS":
            return { ...state,APIResponseStatus:true, gradeList: action.response.data.data}
        case "GET_GRADE_DATA_FAILURE":
            return { ...state,APIResponseStatus:false}
        case "GET_GRADE_DATA_STORE":
            return { ...state };              
        default:
            return { ...state };
    }
}
