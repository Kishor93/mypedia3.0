import axios from 'axios';
export function getGradeScaleData() {
  return {
    types: ["GET_GRADE_DATA_SUCCESS", "GET_GRADE_DATA_FAILURE", "GET_GRADE_DATA_STORE"],
    shouldCallAPI: (state) => true,
    callAPI: () => axios.get('/mockData/gradeList.json'),
    //callAPI: () => axios.get(`/SchoolNetTeacherPortal/API/serviceprovider?class_id=${window.classSectionId}&ins_id=${window.schoolId}&line_item_id=${lineItemId}&opt=json&apiname=DownloadeTemplate`),
    payload: null
  }
}
