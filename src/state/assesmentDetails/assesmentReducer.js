var init = {
    assesmentDetails: [],
    APIResponseStatus:true
  }
export default (state = init, action) => {
      switch (action.type) {
        case "GET_ASSESMENT_DETAILS_SUCCESS":
              state['assesmentDetails'] = action.response.data.data;
              return {...state,APIResponseStatus:true};
        case "GET_ASSESMENT_DETAILS_FAILURE":
              return { ...state,APIResponseStatus:false}
        case "GET_ASSESMENT_DETAILS_STORE":
              return {...state};
        default:
              return {...state};
      }
  }
