import axios from 'axios';
export function getAssesmentDetails(){
    
    return {
        types:["GET_ASSESMENT_DETAILS_SUCCESS","GET_ASSESMENT_DETAILS_FAILURE","GET_ASSESMENT_DETAILS_STORE"],
        shouldCallAPI: (state) => { return !(typeof state.assesmentReducer !== 'undefined' && Object.keys(state.assesmentReducer.assesmentDetails).length > 0) },
        callAPI:()=>axios.get(`/mockdata/assesmentDetails.json`),
         payload:null
    }
}
