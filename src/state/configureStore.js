import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import callAPIMiddleware from './callAPIMiddleware';
import courseReducer from './courseData/courseReducer';
import studentReducer from './studentData/studentReducer';
import reportReducer from './reportData/reportReducer';
import assementReducer from './assesmentDetails/assesmentReducer';
import assesmentSubjectReducer from './assesmentSubjects/assesmentSubjectReducer';
import assessmentQpListReducer from './assessmentQpList/assessmentQpListReducer';
import gradeScaleReducer from './gradeScaleData/gradeScaleReducer';
import gradeAssignReducer from './gradeAssignList/gradeAssignReducer';
import gradeColorReducer from './gradeColorData/gradeColorReducer';

export default () => {
    return createStore(combineReducers({
        courseReducer,
        studentReducer,
        gradeScaleReducer,
        gradeColorReducer,
        reportReducer,
        assementReducer,
        assesmentSubjectReducer,
        assessmentQpListReducer,
        gradeAssignReducer
    }), compose(
        applyMiddleware(thunkMiddleware, callAPIMiddleware),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    ));
}
