import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import MarkEntryView from './views/MarkEntryView';
import MarkApprovalView from './views/MarkApprovalView';
import StudentReportView from './views/StudentReportView';
import AssessmentMapView from './views/AssessmentMapView';
import GradeTemplateView from './views/GradeTemplateView'
import GradeScaleComponentParent from './components/features/GradeScaleComponentParent'

class App extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={MarkEntryView} />
                <Route exact path="/markApprovalView" component={MarkApprovalView} />
                <Route exact path="/studentReportView" component={StudentReportView} />
                <Route exact path="/assessmentMapView" component={AssessmentMapView} />
                <Route exact path="/gradetemplate" component={GradeTemplateView} />
                <Route exact path="/gradescale" component={GradeScaleComponentParent} />
            </Switch>
        );
    }
}

export default App;
