import React, { Component } from 'react';
import { connect } from 'react-redux';
import AssesmentDetailsTab from '../../features/AssesmentDetailsTab';
import {getAssesmentDetails} from '../../../state/assesmentDetails/assesmentAction';
import ErrorHandle from '../../features/ErrorHandle';

class StudentReport extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.props.getAssesmentDetails();
    }

    render() {

        const {assementDetails} = this.props.assesmentDetails;
        return (
            <React.Fragment>
                <div className='col-lg-12'>
                    <AssesmentDetailsTab assesmentDetails={assementDetails} generateReportResponse={{}} generateReport={{}}/>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        assesmentDetails:typeof state.assementReducer.assesmentDetails !== 'undefined' && Object.keys(state.assementReducer.assesmentDetails).length > 0?state.assementReducer.assesmentDetails:{},
        APIResponseStatus:state.assementReducer.APIResponseStatus

    };
};
const mapdispatchtoprops = { getAssesmentDetails }
export default connect(mapStateToProps, mapdispatchtoprops)(ErrorHandle(StudentReport));