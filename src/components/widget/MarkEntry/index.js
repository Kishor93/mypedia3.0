import React, {Component} from 'react';
import {connect} from 'react-redux';
import ErrorHandle from '../../features/ErrorHandle'
import MarkEntryTab from "../../features/MarkEntryTab";
import Summary from '../../features/Summary';
import {getCourseData} from '../../../state/courseData/courseAction';
import './index.css';


class MarkEntry extends Component {

    componentDidMount() {
        this.props.getCourseData();
    }
    
    render() {
        const {courseData} = this.props;
        let lineItemId = courseData.summary.length > 0 ? courseData.summary[0].lineItemId : 1;
        
        return (
            <div className="markEntryTab pb-0">
                <Summary summaryData={courseData.summary}/>
                {lineItemId ? <MarkEntryTab {...this.props}/>:''}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {courseData: typeof state.courseReducer !== 'undefined' ? state.courseReducer : {},APIResponseStatus:state.courseReducer.APIResponseStatus};
};
const mapdispatchtoprops = {getCourseData};
export default connect(mapStateToProps, mapdispatchtoprops)(ErrorHandle(MarkEntry));
