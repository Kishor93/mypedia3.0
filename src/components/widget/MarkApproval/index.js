import React, {Component} from 'react';
import { connect } from 'react-redux';
import {getAssesmentDetails} from '../../../state/assesmentDetails/assesmentAction';
import AssesmentDetailsTab from '../../features/AssesmentDetailsTab';
import {generateReport} from '../../../state/reportData/reportAction';
import ErrorHandle from '../../features/ErrorHandle';

class MarkApproval extends Component {

  constructor(props) {
    super(props);
    this.state = {}
  }
  componentDidMount () {
    this.props.getAssesmentDetails();
  }

  render() {
    const {assementDetails} = this.props.assesmentDetails;
    const {generateReport, generateReportResponse} = this.props;
    return (
      <div className="markEntryTab pb-0">
        <AssesmentDetailsTab assesmentDetails={assementDetails} generateReportResponse={generateReportResponse} generateReport={generateReport}/>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
      assesmentDetails:typeof state.assementReducer.assesmentDetails !== 'undefined' && Object.keys(state.assementReducer.assesmentDetails).length > 0?state.assementReducer.assesmentDetails:{},
      generateReportResponse: state.reportReducer.generateReport ? state.reportReducer.generateReport : null,
      APIResponseStatus:state.assementReducer.APIResponseStatus && state.reportReducer.APIResponseStatus?true:false
  };
};
const mapDispatchToprops = { getAssesmentDetails,generateReport }
export default connect(mapStateToProps,mapDispatchToprops)(ErrorHandle(MarkApproval));
