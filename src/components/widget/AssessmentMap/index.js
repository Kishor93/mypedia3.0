import React, {Component} from 'react';
import { connect } from 'react-redux';
import {getAssesmentDetails} from '../../../state/assesmentDetails/assesmentAction';
import AssesmentMappingTab from '../../features/AssesmentMappingTab';
import ErrorHandle from '../../features/ErrorHandle';

class AssessmentMap extends Component {

  constructor(props) {
    super(props);
    this.state = {}
  }
  componentDidMount () {
    this.props.getAssesmentDetails();
  }

  render() {
    const {assementDetails} = this.props.assesmentDetails;
    return (
      <div className="markEntryTab pb-0">
        <AssesmentMappingTab assesmentDetails={assementDetails} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
      assesmentDetails:typeof state.assementReducer.assesmentDetails !== 'undefined' && Object.keys(state.assementReducer.assesmentDetails).length > 0?state.assementReducer.assesmentDetails:{},
      APIResponseStatus:state.assementReducer.APIResponseStatus && state.reportReducer.APIResponseStatus?true:false
  };
};
const mapDispatchToprops = { getAssesmentDetails }
export default connect(mapStateToProps,mapDispatchToprops)(ErrorHandle(AssessmentMap));
