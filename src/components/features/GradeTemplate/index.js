import React, { Component } from "react";
import TabsWrapper from "../../features/TabsWrapper";
import ErrorHandle from "../../features/ErrorHandle";
import GradeScaleTemplate from "../../features/GradeScaleTemplate";
import ManageRemarksTemplate from "../../features/ManageRemarksTemplate";

class GradeTemplate extends Component {
  constructor(props) {
    super(props);
    this.data = {
        orientation: "horizontal",
        defaultActiveKey: "gradeScaleTemplate",
        navStyles: "nav nav-tabs nav-fill default__tab",
        navLinkStyles: "nav-item nav-link nav__tab",
        navLinkStylesParent: "col-lg-2 col-md-3 col-sm-6 col-6",
        tabs: [
          {
            eventKey: "gradeScaleTemplate",
            navComponent: "Grade scale template",
            contentComponent: <GradeScaleTemplate />
          },
          {
            eventKey: "manageRemarks",
            navComponent: "Manage remarks",
            contentComponent: <ManageRemarksTemplate />
          }
        ]
      }
  }

  render() {
    return <TabsWrapper data={this.data} />;
  }
}


export default ErrorHandle(GradeTemplate);
