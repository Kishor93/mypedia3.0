import React from "react";
import "./Loader.css";
import ErrorHandle from '../ErrorHandle';

class Loader extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		let show = this.props.loading?'show':'';
		return (
			<div className={`loading__spinner ${show}`}>
              <img src ="images/spinner.gif" className="loading__spinnerimg" alt="spinner img"/>
            </div>
		);
	}
}

export default ErrorHandle(Loader);
