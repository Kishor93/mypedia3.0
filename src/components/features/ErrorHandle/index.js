import React from 'react';
import NotificationPopup from '../NotificationPopup/NotificationPopup.js'
export default function ErrorHandle(WrappedComponent) {
    return class extends React.Component {
        constructor() {
            super();
            this.state = {hasError: false,errorMessage:{},NotificationPopup: false};
        }

        static getDerivedStateFromError(error) {
            // Update state so the next render will show the fallback UI.
            return {hasError: true,errorMessage:error,NotificationPopup:true};
        }

        componentDidCatch(error, info) {
            // You can also log the error to an error reporting service
            //logErrorToMyService(error, info);
            
        }
        closeNotificationpopup = () => this.setState((prevState) => ({ NotificationPopup: !prevState.NotificationPopup }))

        render() {
            //console.log('this.props.APIResponseStatus',this.props.APIResponseStatus);
            if(typeof this.props.APIResponseStatus !== 'undefined' && !this.props.APIResponseStatus){
                return <NotificationPopup text={'Oops! Something went wrong. Please <button id="refreshButton" style="background-color: transparent; text-decoration: underline; color: #0089ff; padding: 0px;">Refresh</button> the page after sometime.'} status={3}/>
            }
            if (this.state.hasError) {
                // You can render any custom fallback UI
                return this.state.NotificationPopup ? <NotificationPopup text={'Oops! Something went wrong'} status={3} closepopup={this.closeNotificationpopup} /> : ""
            }


            return <WrappedComponent {...this.props}/>
        }
    }
}