import React from 'react';
import ErrorHandle from '../ErrorHandle';
import Subject from '../Subject';

class GradeScaleSubject extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            allSelected: new Array(this.props.subjectList.classes.length).fill(0),
            allUnselected: new Array(this.props.subjectList.classes.length).fill(1),
            currentCategories: new Array(this.props.subjectList.classes.length).fill(0),
            subjectFilter: "allsub"
        }
    }

    componentDidMount() {
        this.props.subjectList.classes.forEach((classObject, classIndex) => {
            if (this.checkIfAllChecked(this.props.subjectList, classIndex)) {
                let allSelected = this.state.allSelected;
                allSelected[classIndex] = true;
                this.setState({
                    allSelected: allSelected
                })
            }
            else {
                let allSelected = this.state.allSelected;
                allSelected[classIndex] = false;
                this.setState({
                    allSelected: allSelected
                })
            }
        })
    }

    componentWillReceiveProps() {
        this.props.subjectList.classes.forEach((classObject, classIndex) => {
            if (this.checkIfAllChecked(this.props.subjectList, classIndex)) {
                let allSelected = this.state.allSelected;
                allSelected[classIndex] = true;
                this.setState({
                    allSelected: allSelected
                })
            }
            else {
                let allSelected = this.state.allSelected;
                allSelected[classIndex] = false;
                this.setState({
                    allSelected: allSelected
                })
            }
        })
    }

    mypediaSelectCheck = () => {
        console.log("select subject")
    }

    handleAllSelected = (e, index) => {
        let allSelectedArray = this.state.allSelected;
        let subjectList = this.props.subjectList;
        allSelectedArray[index] = !allSelectedArray[index];
        subjectList.classes[index].subjects.forEach((subject, subjectIndex) => {
            subject.checked = allSelectedArray[index];
        })
        this.setState({
            allSelected: allSelectedArray
        }, () => {
            this.props.changeSubjectList(this.props.subjectList);
        })
    }

    handleSubjectFilterChange = e => {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            subjectFilter: e.target.value
        });
    }

    handleSubjectClick = (e, index, classIndex) => {
        let subjectList = this.props.subjectList;
        subjectList.classes[classIndex].subjects[index].checked = !subjectList.classes[classIndex].subjects[index].checked;
        let isAllSelected = this.checkIfAllChecked(subjectList, classIndex);
        // let isAllUnselected = this.checkIfAllUnchecked(subjectList, classIndex);
        let allSelected = this.state.allSelected;
        allSelected[classIndex] = isAllSelected;
        // let allUnselected = this.state.allUnselected;
        // allUnselected[classIndex] = isAllUnselected;
        this.setState({
            allSelected: allSelected
        }, () => {
            this.props.changeSubjectList(this.props.subjectList);
        });
    }

    checkIfAllUnchecked = (subjectList, classIndex) => {
        let allSelected = false;
        subjectList.classes[classIndex].subjects.forEach((subject, index) => {
            if (subject.checked === true) {
                allSelected = true;
            }
        })
        return !allSelected;
    }

    checkIfAllChecked = (subjectList, classIndex) => {
        let allSelected = true;
        subjectList.classes && subjectList.classes.length && subjectList.classes[classIndex] && subjectList.classes[classIndex].subjects && subjectList.classes[classIndex].subjects.length && subjectList.classes[classIndex].subjects.forEach((subject, subjectIndex) => {
            if (subject.checked === false) {
                allSelected = false;
            }
        })
        return allSelected;
    }

    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <div className="assign__grade__template__exam" id="grade__scale__assign__category__exam__set" style={{ display: "block" }}>
                        <div className="row">
                            <div className="col-12">
                                <div className="nav__button">
                                    <ul className="nav__button--tab nav">
                                        {
                                            this.props.subjectList.classes.map((classObject, classIndex) => {
                                                return <li key={classIndex} className="nav__button--tablist">
                                                    <a className={this.props.currentClass === classIndex ? "nav__button--tabbutton nav-item active show" : "nav__button--tabbutton nav-item"} onClick={e => this.props.selectClass(e, classIndex)} data-toggle="tab" role="tab" aria-label={classObject.className} title={classObject.className} aria-controls={"tab__" + classIndex} aria-selected={this.props.currentClass === classIndex} tabIndex="">{classObject.className}</a>
                                                </li>
                                            })
                                        }
                                    </ul>
                                </div>
                                {
                                    this.props.subjectList.classes.map((classObject, classIndex) => {
                                        return this.props.currentClass === classIndex ? <div key={classIndex} className="tab-content">
                                            {
                                                classObject.subjects && classObject.subjects.length ?
                                                    <React.Fragment>
                                                        <div className={this.props.currentClass === classIndex ? "tab-pane active show" : "tab-pane"} role="tabpanel" aria-labelledby={"tab__" + classIndex}>
                                                            <div className="container-fluid">
                                                                <div className="row">
                                                                    <div className="col-12 p-0">
                                                                        <div className="tab__conatiner">
                                                                            <React.Fragment>
                                                                                <div className="tab__conatiner__heading d-flex">
                                                                                    <div className="col-lg-6 col-md-6 col-sm-6 col-12 align-self-center d-flex p-0">
                                                                                        <div className="tab__conatiner__heading__checkbox align-self-center mr-3">
                                                                                            <label className="material__checkbox">
                                                                                                <input type="checkbox" name="inline__checkbox" id="inline__checkboxclass__main" value="Select all" onChange={e => this.handleAllSelected(e, classIndex)} checked={this.state.allSelected[classIndex]} />
                                                                                                <span>Select all</span>
                                                                                            </label>
                                                                                        </div>
                                                                                        {
                                                                                            classObject.subjectFilters && classObject.subjectFilters.length ?
                                                                                                <div className="tab__conatiner__heading__selectbox align-self-center mx-4">
                                                                                                    <div className="form-group form__formgroup form__formgroup--sub d-flex">
                                                                                                        <label className="form__label form__label--sub align-self-center">Show</label>
                                                                                                        <select onChange={this.handleSubjectFilterChange} id="grade__scale__assign__subject" className="form-control form__select form__select--sub">
                                                                                                            {
                                                                                                                classObject.subjectFilters.map((filter, filterIndex) => {
                                                                                                                    return <option key={filterIndex} value={filter.accessor} selected={this.state.currentCategories[classIndex] === filter.accesor}>{filter.name}</option>
                                                                                                                })
                                                                                                            }
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                                :
                                                                                                null
                                                                                        }
                                                                                    </div>
                                                                                    <div className="col-lg-6 col-md-6 col-sm-6 col-12 align-self-center d-flex justify-content-end p-0">
                                                                                        <div className="tab__conatiner__heading__subjectbox">
                                                                                            <img className="tab__conatiner__heading__subjectbox__img justify-content-start align-self-center" alt="" src="images/pearson-sub-icon.svg" />
                                                                                            <span className="tab__conatiner__heading__subjectbox__content">MyPedia subjects</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="tab__conatiner__content">
                                                                                    <Subject subjectList={this.props.subjectList.classes[this.props.currentClass].subjects}
                                                                                        filter={this.state.subjectFilter}
                                                                                        handleClick={this.handleSubjectClick}
                                                                                        classIndex={classIndex}
                                                                                    />
                                                                                </div>
                                                                            </React.Fragment>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </React.Fragment>
                                                    :
                                                    null
                                            }
                                        </div>
                                            :
                                            null
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ErrorHandle(GradeScaleSubject);