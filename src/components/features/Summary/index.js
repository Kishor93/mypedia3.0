import React from 'react';
import './index.css';
import ErrorHandle from '../../features/ErrorHandle';

class Summary extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            moredetails: false,
            error: false,
            info: null
        };
    }

    viewmoredetails = () => {
        this.setState((prevState) => ({moredetails: !prevState.moredetails}))
    }

    render() {
        const {summaryData} = this.props;
        const summary = summaryData !== 'undefined' && summaryData.length > 0 ? summaryData[0] : summaryData;
        //let id= `${window.qpId}`;
        //alert(id);
        return (
            <div>
                <div
                    className="d-md-flex d-sm-flex d-xl-flex justify-content-md-between justify-content-sm-between justify-content-xl-between font-weight-bold markEntry__notificationheader ViewForMobile">
                    <div className="d-flex">
                        {/* <a href="#">
                            <img className="unitestLeftArrow" src="images/chevron-right.svg"
                                alt="chevron-right image" />
                        </a> */}
                        <h6 className="unitestHeading unitestHeading--topMargin d-flex"
                            tabIndex="0"> {summary.title} </h6>
                        <span className="subjectsTag ml-2 mt-2"> {summary.gradeSection}</span>
                    </div>

                    <ul className="d-sm-flex d-md-flex d-lg-flex">
                        <li className="inline" id="headingOne">
                            <button type="button"
                                    className="accordion-toggle ButtonTransparent add__btn btn"
                                    data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne" tabIndex="0"
                                    onClick={this.viewmoredetails}>
                                {!this.state.moredetails ?
                                    <div><span className="markEntry__cardviewmore">More details</span><i
                                        className="material-icons markEntry__icon--add arrow-change">expand_more</i>
                                    </div> : <div><span className="markEntry__cardviewmore">Less details</span><i
                                        className="material-icons markEntry__icon--add arrow-change">expand_less</i>
                                    </div>}
                            </button>
                            {/* <button type="button"
                                className="accordion-toggle ButtonTransparent add__btn btn"
                                tabIndex="0">
                                <span onClick={this.openModal2} className="markEntry__cardviewmore">View Report</span>
                            </button> */}
                        </li>
                    </ul>
                </div>

                {this.state.moredetails ? <div id="collapseOne" className="collapse__dm show">
                    <div className="unitTestSection d-flex justify-content-around">
                        <div className="col-3 unitTestSection__contentBox unitTestSection__contentBox--firstchild">
                            <h6 className="unitestHeading mb-1"><span>Type:</span> {summary.Type}</h6>
                            <h6 className="unitestHeading mb-1"><span>Subject:</span> <span
                                className="unitestHeading__subject">{summary.Subject}</span>
                            </h6>
                            <h6 className="unitestHeading mb-1" tabIndex="0"
                                aria-label="The test has been conducted for the chapter <Invasion of Mughals>">
                                <span className="unitestHeading__chapter">Chapter:</span>
                                <span className="unitestHeading__chaptersmark">{summary.Chapter.split(',')[0]}</span>
                                <div className="unitestHeading__tooltipr">
                                    {
                                        summary.Chapter.split(',').length > 1 ?
                                            <div className="unitestHeading__span">{summary.Chapter.split(',').length}
                                                <div className="unitestHeading__tooltiphover">
                                                    <i></i>
                                                    {summary.Chapter && summary.Chapter.split(',').map((chapter) => {
                                                        return <p
                                                            className="unitestHeading__chaptersmarkprg">{chapter}</p>
                                                    })}
                                                </div>
                                            </div> : ""

                                    }

                                </div>
                            </h6>
                        </div>
                        <div className="col-2 text-center unitTestSection__contentBox">
                            <h6 className="unitestHeading mb-1">{summary.Points}
                                <a className="hover" href="#"><img className="pointScaleIcon"
                                                                   src="images/info-outline.svg"
                                                                   alt=""/>
                                    <div className="total__schools total__schools--gradescale"><i></i>
                                        <h6 className="unitestHeading mb-2"><b>Grade Scale Template</b></h6>
                                        <p className="mb-2">Grade Scale is selected as per your school board. You cannot
                                            change
                                            the grade scale.</p>
                                        <h6 className="unitestHeading"><b>{summary.Points}</b></h6>
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="grade__scale">
                                                    <table
                                                        className="table table-responsive-md table-borderless grade__colortable">
                                                        <tbody className="col-12">
                                                        <tr className="d-flex">
                                                            {summary.PointsColorCode.map((pointcolor) => {
                                                                return <td className="grade__color"
                                                                           style={{backgroundColor: `${pointcolor.ColorCode}`}}>{pointcolor.GradeName}</td>
                                                            })}
                                                            {/*<td className="garde__colrtd grade__fail--2 text-center col">F2</td>
                                                                <td className="garde__colrtd grade__fail--1 text-center col">F1</td>
                                                                <td className="garde__colrtd grade__fail--2 text-center col">E2</td>
                                                                <td className="garde__colrtd grade__fail--1 text-center col">E1</td>
                                                                <td className="garde__colrtd grade__poor--2 text-center col">D2</td>
                                                                <td className="garde__colrtd grade__poor--1 text-center col">D1</td>
                                                                <td className="garde__colrtd grade__average--2 text-center col">C2</td>
                                                                <td className="garde__colrtd grade__fail--2 text-center col">A</td>
                                                                <td className="garde__colrtd grade__poor--1 text-center col">B</td>
                                                                <td className="garde__colrtd garde__well--1 text-center col">C</td>
                                                                <td className="garde__colrtd grade__average--2 text-center col">D</td>
                                                                <td className="garde__colrtd garde__highest--1 text-center col">E</td>*/}
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </h6>
                            <h6 className="unitestHeading"><span>Grading</span></h6>
                        </div>
                        <div className="col-4 d-flex justify-content-between unitTestSection__contentBox">
                            <div className="unitTestSection__marksInfo text-center">
                                <h6 className="unitestHeading mb-1">{summary.MinMark}</h6>
                                <h6 className="unitestHeading"><span>Min. Marks</span></h6>
                            </div>
                            <div className="unitTestSection__marksInfo text-center">
                                <h6 className="unitestHeading mb-1">{summary.MaxMark}</h6>
                                <h6 className="unitestHeading"><span>Max. Marks</span></h6>
                            </div>
                            <div className="unitTestSection__marksInfo text-center">
                                <h6 className="unitestHeading mb-1">{summary.Duration}
                                    <small>min</small>
                                </h6>
                                <h6 className="unitestHeading"><span>Duration</span></h6>
                            </div>
                        </div>
                        <div
                            className="col-3 d-flex unitTestSection__contentBox unitTestSection__contentBox--lastchild">
                            <h6 className="unitestHeading mr-3 mt-2"><img className="unitTestSection__calendarIcon"
                                                                          src="images/calendar-18.svg"
                                                                          alt=""/> Conducted on
                            </h6>
                            <div className="conductedDate mt-1">{summary.ConductedOn}</div>
                        </div>
                    </div>
                </div> : ""}
            </div>
        );
    }

}

export default ErrorHandle(Summary);
