import React, { Component } from 'react';
import ErrorHandle from '../ErrorHandle';
// import AccordionContent from '../AccordionContent'

class DefaultRemarksTemplate extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    // viewmoredetails = () => this.setState((prevState) => ({ moredetails: !prevState.moredetails }))

    render() {
        let {heading_1, heading_2, description_1, description_2} = this.props;
        return (
            <div>
            <div className="col-lg-12 col-md-12 col-sm-12 col-12 p-0 align-self-center">
                <h6 className="remarksec__heading">Format</h6>
                <h6 className="remark__para"><span className="remark__tag">Student name</span> is performing well in <span className="remark__tag">Subject name</span></h6>
            </div>
            <div className="col-lg-12 col-md-12 col-sm-12 col-12 p-0 align-self-center">
                <h6 className="remarksec__heading">Example</h6>
                <div className="remark__contentexample">
                    <span className="remark__content">Jaison gardner is performing well in Mathamatics</span>
                </div>
            </div>
            </div>
        )
    }

}

export default ErrorHandle(DefaultRemarksTemplate);
