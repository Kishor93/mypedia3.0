import React from 'react';
import './index.css';
import ErrorHandle from '../ErrorHandle';

class DragNDrop extends React.Component {

  constructor() {
    super();
    this.state = {
      active: false,
      target: false,
      hover: false
    }
    this.upload = React.createRef();
  }

  componentDidMount() {
    window.addEventListener('dragover', this.dropTarget);
    window.addEventListener('dragleave', this.dropLeave);
    window.addEventListener('drop', this.handleChange);
  }

  componentWillUnmount() {
    window.removeEventListener('dragover', this.dropTarget);
    window.removeEventListener('dragleave', this.dropLeave);
    window.removeEventListener('drop', this.handleChange);
  }

  handleChange = (e) => {

    var file = e.target.files[0];
 
    this.props.onFileChange({
      file: file,
      name: file.name,
      type: file.type
    });
  }


  
  dropTarget = (e) => {

    if (!this.state.active) {
      this.setState({
        target: true
      });
    }
  }

  dropLeave = (e) => {

    if (e.screenX === 0 && e.screenY === 0) { // Checks for if the leave event is when leaving the window
      this.setState({
        target: false
      });
    }
  }

  handleDrop = (e) => {

    e.preventDefault();
    e.stopPropagation();

    var uploadObj = {
      target: e.nativeEvent.dataTransfer
    };

    this.setState({
      target: false,
      hover: false
    });

    this.handleChange(uploadObj);
    // let imgType = e.nativeEvent.dataTransfer.files.length === 1 ? e.nativeEvent.dataTransfer.files[0].type : null;
    // if (imgType === "application/xls" || imgType === "apllication/xlsx" || imgType == "application/vnd.ms-excel" || imgType == "image/png" || imgType === "image/jpeg") {
    //   this.handleChange(uploadObj);
    // }
  }

  handleDragEnter = (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (!this.state.active) {
      this.setState({
        hover: true
      });
    }
  }

  handleDragLeave = (e) => {

    this.setState({
      hover: false
    });
  }

  handleDragOver = (e) => {
    e.preventDefault();
  }

  render() {
    return (<div className='file-container'>
      <img className={`file-input ${this.props.customStyle ? "" : "image-preview"}`}
        name="upload"
        src={this.props.file}
        ref={this.upload}
        onChange={this.handleChange}
        onDrop={this.handleDrop}
        onDragEnter={this.handleDragEnter}
        onDragOver={this.handleDragOver}
        onDragLeave={this.handleDragLeave}
      />
    </div>)
  }

}

export default ErrorHandle(DragNDrop);
