import React from "react";
import ReactHtmlParser from 'react-html-parser'
import "./NotificationPopup.css";
import ErrorHandle from '../ErrorHandle';

class NotificationPopup extends React.Component {
	constructor() {
		super();
	}
	componentDidMount() {
		if (typeof document.getElementById("refreshButton") !== 'undefined' && document.getElementById("refreshButton")) { document.getElementById("refreshButton").addEventListener("click", function () { window.location.reload() }) }
	}
	render() {
		let classForBorder = this.props.status === 1 ? 'success__color' : this.props.status === 2 ? 'warning__color' : this.props.status === 3 ? 'failure__color' : '';
		return (
			<div className="d-flex justify-content-center">
				<div className={`schoolsetup__notification ${classForBorder}`}>
					{typeof this.props.closepopup !== 'undefined' && <div className="schoolsetup__buttonblock d-block">
						<button className="schoolsetup__notificationbutton" onClick={this.props.closepopup}>
							<i className="material-icons d-block schoolsetup__notificationicon">
								close
							</i>
						</button>
					</div>}
					<div className="markentry__bulkupload__validationtext">
						{ReactHtmlParser(this.props.text)}
					</div>
					<button className="alert__dialoggotitbutton" aria-label="Close" type="close" onClick={this.props.closepopup}>
						<span>Got it</span>
					</button>
				</div>
			</div>
		);
	}
}

export default ErrorHandle(NotificationPopup);
