import React, {Component} from 'react';
import './index.css';
import PDFView from "./pdfView";
import ErrorHandle from '../../ErrorHandle';

class Index extends Component {
    state = {
        numPages: null,
    };

    onDocumentLoadSuccess = (document) => {
        const {numPages} = document;
        this.setState({
            numPages,
        });
    };

    componentDidMount() {
        const {onPrintRef} = this.props;
        if (onPrintRef) {
            onPrintRef(this.printRef);
        }
    }

    render() {
        const {numPages} = this.state;
        const {fileList, scale, onLoadFailure, onLoadSuccess} = this.props;
        return (
            <div ref={el => (this.printRef = el)} id="canvas-holder">
                {fileList && fileList.map((file) => {
                    return (
                        <PDFView onLoadSuccess={onLoadSuccess} onLoadFailure={onLoadFailure} key={file.fileName} scale={scale} pdf={file}/>
                    )
                })}
            </div>
        );
    }
}

export default ErrorHandle(Index);
