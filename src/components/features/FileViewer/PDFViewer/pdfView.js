import React, {Component} from 'react';
import {Document, Page} from 'react-pdf';
import './index.css';
import axios from "axios";
import ErrorHandle from '../../ErrorHandle';

const Loader = () => {
    return (
        <div className="lds-ring">
            <div/>
            <div/>
            <div/>
            <div/>
        </div>
    )
};

class PDFView extends Component {
    state = {
        numPages: null,
        file: null,
        loading: true
    };

    onDocumentLoadSuccess = (document) => {
        const {numPages} = document;
        this.setState({
            numPages,
        });
    };

    fetchFile = () => {
        const {pdf} = this.props;
        axios({
            method: 'post',
            url: pdf.file,
            responseType: 'blob',
            data: {"data":"ew0KICAibWV0YWRhdGEiOiBbDQogICAgew0KICAgICAgInNjaG9vbGlkIjogIiIsDQogICAgICAiY2xhc3NfaWQiOiAiIiwNCiAgICAgICJhc3Nlc3NtZW50X2lkIjogIiIsDQogICAgICAiYXBpbmFtZSI6ICJQcmV2aWV3UmVwb3J0VGVtcGxhdGUiLA0KICAgICAgIm1vZHVsZSI6ICJtYXJrRW50cnkiLA0KICAgICAgImFjdGlvbiI6ICJQUkVWSUVXUkVQT1JURE9XTkxPQUQiLA0KICAgICAgInRyYW5zYWN0aW9uc3RhdHVzIjogInJlcXVlc3RlZCINCiAgICB9DQogIF0sDQogICJrZXkiOiAiUkVBQ1RfQVBJX1NURyIsDQogICJkYXRlIjogIjE1NTAyMjkyMzUiLA0KICAidG9rZW4iOiAiIg0KfQ=="}
        }).then((response) => {
            const {onLoadSuccess} = this.props;
            if(onLoadSuccess) {
                onLoadSuccess();
            }
            this.setState({file: URL.createObjectURL(response.data), loading: false})
        }).catch((err) => {
            const {onLoadFailure} = this.props;
            if(onLoadFailure) {
                onLoadFailure();
            }
            this.setState({file: {err}, loading: false})
        })
    };

    componentDidMount() {
        this.fetchFile();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.pdf.file !== this.props.pdf.file) {
            this.setState({loading: true});
            this.fetchFile();
        }
    }

    render() {
        const {numPages, file, loading} = this.state;
        const {scale, pdf} = this.props;

        return (
            <>
                {
                    loading ?
                        <div className="pdfView_Loader"><Loader/></div>
                        :
                        file && file.err ?
                            <div className="pdfView_Error text-danger">Failed to load PDF file.</div>
                            :
                            file && <Document
                                file={file}
                                onLoadSuccess={this.onDocumentLoadSuccess}
                                onLoadError={(err) => {
                                    const {onLoadFailure} = this.props;
                                    if(onLoadFailure) {
                                        onLoadFailure();
                                    }
                                }}
                                noData={<div className="pdfView_Error text-danger">No PDF file specified</div>}
                                error={<div className="pdfView_Error text-danger">Failed to load PDF file.</div>}
                                onSourceError={() => <div className="pdfView_Error text-danger">Failed to load PDF
                                    file.</div>}
                                loading={<div className="pdfView_DocumentLoader"><Loader/></div>}
                            >
                                {Array.from(
                                    new Array(numPages),
                                    (el, index) => (
                                        <Page
                                            key={`page_${index + 1}`}
                                            pageNumber={index + 1}
                                            scale={scale}
                                        />
                                    ),
                                )}
                            </Document>
                }
            </>
        )

    }
}

export default ErrorHandle(PDFView);
