import React, {Component} from 'react';
import Popup from "../Popup";
import './index.css';
import PDFViewer from "./PDFViewer";
import {downloadFiles} from "../../../util/util";
import ReactToPrint from "react-to-print";
import ErrorHandle from '../ErrorHandle';
import NotificationPopup from '../NotificationPopup/NotificationPopup';

class FileViewer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeKey: props.defaultActive,
            printPreview: props.printPreview || false,
            controlDisabled: false,
            notification: {
                enable: false,
                text: '',
                status: null
            }
        };
    }

    slideLeft = () => {
        const {activeKey} = this.state;
        if (activeKey - 1 >= 0) {
            this.setState((prevState) => {
                return {activeKey: prevState.activeKey - 1};
            })
        }
    };

    slideRight = () => {
        const {fileList} = this.props;
        const {activeKey} = this.state;
        if (activeKey + 1 < fileList.length) {
            this.setState((prevState) => {
                return {activeKey: prevState.activeKey + 1};
            })
        }
    };

    onDownload = () => {
        const {fileList, onDownload} = this.props;
        if (onDownload) {
            onDownload();
        }
        this.setState({
            notification: {
                enable: true,
                text: "Download Initiated! Please Wait",
                status: 1
            }
        });
        setTimeout(() => {
            this.setState({notification: {enable: false}})
        }, 1500);
        downloadFiles(fileList);
    };

    onPrintPreview = () => {
        this.setState({printPreview: true, controlDisabled: false})
    };

    onPrintRef = (printRef) => {
        this.printRef = printRef;
    };

    toggleControl = (state) => {
        this.setState({controlDisabled: state})
    };

    render() {
        const {modalIsOpen, closeModal, fileList} = this.props;
        const {activeKey, printPreview, controlDisabled, notification} = this.state;
        return (
            <div>
                {<Popup modalIsOpen={modalIsOpen} closeModal={closeModal} id="studentreportPopup">
                    {notification. enable && <NotificationPopup status={notification.status} text={notification.text}/>}
                    <div className="modal-dialog">
                        <div className="modal-content bg-transparent">
                            <div className="modal-header studentreportPopup__header">
                                {printPreview && <>
                                    <div className="col-4 d-flex">
                                        <ul className="d-flex studentreportPopupIcon">
                                            <li className="studentreportPopupIcon__list">
                                                <a onClick={() => {
                                                    this.setState({printPreview: false, controlDisabled: false})
                                                }} data-dismiss="modal">
                                                    <img src="images/arrow-left.png" alt=""/>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-4 d-flex justify-content-end">
                                        <ReactToPrint
                                            trigger={() => <li
                                                className={"studentreportPopupIcon__list " + (controlDisabled ? "controlDisabled" : '')}>
                                                <a className={controlDisabled ? "controlDisabled" : ''}
                                                   data-dismiss="modal">
                                                    <img src="images/print-24.svg" alt=""/>
                                                </a>
                                            </li>}
                                            content={() => this.printRef}
                                        />
                                        <ul className="d-flex studentreportPopupIcon">
                                            <li className="studentreportPopupIcon__list studentreportPopupIcon__list--lastchild">
                                                <a onClick={closeModal} data-dismiss="modal">
                                                    <img src="images/close_icon.svg" alt=""/>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </>}
                                {!printPreview && <>
                                    <div className="col-4 d-flex">
                                        <div className="mr-2"><img className="studentreportPopup_pdficon"
                                                                   src="images/file-pdf-24.svg" alt=""/></div>
                                        <p className="studentreportPopup_pdfName">{fileList[activeKey] && fileList[activeKey].name}</p>
                                    </div>
                                    <div className="col-4 text-center">
                                        <p>Page {activeKey + 1} of {fileList.length}</p>
                                    </div>
                                    <div className="col-4 d-flex justify-content-end">
                                        <ul className="d-flex studentreportPopupIcon">
                                            <li className="studentreportPopupIcon__list">
                                                <a className={controlDisabled ? "controlDisabled" : ''}
                                                   onClick={this.onDownload} data-dismiss="modal">
                                                    <img src="images/download-24.svg" alt=""/>
                                                </a>
                                            </li>
                                            <li className="studentreportPopupIcon__list">
                                                <a className={controlDisabled ? "controlDisabled" : ''}
                                                   onClick={this.onPrintPreview} data-dismiss="modal">
                                                    <img src="images/icon-print-preview.svg" alt=""/>
                                                </a>
                                            </li>
                                            <li className="studentreportPopupIcon__list studentreportPopupIcon__list--lastchild">
                                                <a onClick={closeModal} data-dismiss="modal">
                                                    <img src="images/close_icon.svg" alt=""/>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </>}
                            </div>
                            <div className="modal-body">
                                <div className="studentreportPopup__bodycontent">
                                    {!printPreview && fileList.length > 1 &&
                                    <div className="studentreportbodyicons d-flex justify-content-between mb-3">
                                        <div className="d-flex ml-3">
                                            {activeKey - 1 >= 0 && <>
                                                <a onClick={this.slideLeft}
                                                   className="studentreportbodyicons_Icons mr-2"
                                                   data-slide="prev"><i
                                                    className="material-icons">keyboard_arrow_left</i></a>
                                                <p className="studentreportbodyicons_name">{fileList[activeKey - 1].name}</p>
                                            </>
                                            }

                                        </div>
                                        <div className="d-flex mr-3">
                                            {activeKey + 1 < fileList.length && <>
                                                <p className="studentreportbodyicons_name">{fileList[activeKey + 1].name}</p>
                                                <a onClick={this.slideRight}
                                                   className="studentreportbodyicons_Icons ml-2"
                                                   data-slide="next"><i
                                                    className="material-icons">keyboard_arrow_right</i></a>
                                            </>}

                                        </div>
                                    </div>}
                                    <div className="studentreportbodyPdfContent">
                                        {printPreview ?
                                            <>
                                                {/*<PDFPrint onPrintRef={this.onPrintRef} pdfList={fileList}/>*/}
                                                <PDFViewer onLoadSuccess={() => this.toggleControl(false)}
                                                           onLoadFailure={() => this.toggleControl(true)} scale={1.8}
                                                           onPrintRef={this.onPrintRef} fileList={fileList}/>
                                            </>
                                            :
                                            <div id="demo" className="carousel slide" data-ride="carousel">
                                                <div className="carousel-inner">
                                                    <div className="carousel-item active">
                                                        <PDFViewer onLoadSuccess={() => this.toggleControl(false)}
                                                                   onLoadFailure={() => this.toggleControl(true)}
                                                                   scale={1.7} fileList={[fileList[activeKey]]}/>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Popup>}
            </div>
        );
    }
}

export default ErrorHandle(FileViewer);
