import React, {Component} from 'react';
import ErrorHandle from '../ErrorHandle';

class AssessmentBased extends Component {
    render() {
        const {marks} = this.props;
        return (
            <div className="marksentry__table">
                <div className="container-fluid" id="mark__container">
                    <div className="row">
                        <div className="col-4 p-0">
                            <table className="table markname__table">
                                <thead>
                                <th colSpan="1">{React.createElement('h6',{class:'float-left'},'Name')}</th>
                                <th>  <h6 className="float-right text-right roll__call">Roll No</h6></th>
                                <th className="markname__table--exam"><h6 className="float-none text-center">Exam</h6>
                                </th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">
                                        Anjali Subramaniyam Anjali Subramaniyam Anjali Subramaniyam Anjali Subramaniyam
                                        <div className="student__contenttooltiphover--name">
                                            <i/>
                                            <p className="student__contenttooltiphoverprg">Anjali Subramaniyam</p>
                                        </div>
                                    </span>

                                    </h6><h6 className="float-right text-right roll__no">01</h6></td>
                                    <td className="markname__table--exam"><input type="text" className="form-control"
                                                                                 value="52"/></td>
                                </tr>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">Ananth Vaithyanth</span></h6><h6
                                        className="float-right text-right roll__no">02</h6></td>
                                    <td className="markname__table--exam"><input type="text" className="form-control"
                                                                                 value="48"/></td>
                                </tr>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">Bala Ganesh</span></h6><h6
                                        className="float-right text-right roll__no">03</h6></td>
                                    <td className="markname__table--exam"><input type="text"
                                                                                 className="form-control bg__warning"
                                                                                 value="120"/></td>
                                </tr>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">Bindu Shanmugam</span></h6><h6
                                        className="float-right text-right roll__no">04</h6></td>
                                    <td className="markname__table--exam"><input type="text" className="form-control"
                                                                                 value="46"/></td>
                                </tr>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">Baskar Rajan</span></h6><h6
                                        className="float-right text-right roll__no">05</h6></td>
                                    <td className="markname__table--exam"><input type="text" className="form-control"
                                                                                 value="50"/></td>
                                </tr>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">Catherin Joseph</span></h6><h6
                                        className="float-right text-right roll__no">06</h6></td>
                                    <td className="markname__table--exam"><input type="text" className="form-control"
                                                                                 value="57"/></td>
                                </tr>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">Charan Vijay</span></h6><h6
                                        className="float-right text-right roll__no">07</h6></td>
                                    <td className="markname__table--exam"><input type="text" className="form-control"
                                                                                 value="52"/></td>
                                </tr>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">David John</span></h6><h6
                                        className="float-right text-right roll__no">08</h6></td>
                                    <td className="markname__table--exam"><input type="text" className="form-control"
                                                                                 value="55"/></td>
                                </tr>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">Diya Saravanan</span></h6><h6
                                        className="float-right text-right roll__no">09</h6></td>
                                    <td className="markname__table--exam"><input type="text" className="form-control"
                                                                                 value="60"/></td>
                                </tr>
                                <tr>
                                    <td><h6 className="float-left student__content"><img
                                        className="student__content--avatar" src="images/avatar.svg"/><span
                                        className="student__content--name">Naga Surya</span></h6><h6
                                        className="float-right text-right roll__no">10</h6></td>
                                    <td className="markname__table--exam"><input type="text" className="form-control"
                                                                                 value="30"/></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-6 p-0 markentry__table">
                            <table className="table mark__table">
                                <thead>
                                <th><h6 className="text-center">Lab</h6></th>
                                <th><h6 className="text-center">Viva</h6></th>
                                <th><h6 className="text-center">Seminar</h6></th>
                                <th><h6 className="text-center">Presentation</h6></th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <input type="text" className="form-control bg__warning" value="140"/>
                                        <div className="bg__warning--hover">
                                            <i/>
                                            <p className="bg__warning--prg">Invalid mark entry</p>
                                        </div>
                                    </td>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" className="form-control" value="10"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                    <td><input type="text" className="form-control" value="6"/></td>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" className="form-control" value="10"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                    <td><input type="text" className="form-control" value="10"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" className="form-control bg__warning" value="1ab"/></td>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" className="form-control bg__warning" value="1P0"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                    <td><input type="text" className="form-control" value="8"/></td>
                                    <td><input type="text" className="form-control" value="6"/></td>
                                    <td><input type="text" className="form-control" value="7"/></td>
                                </tr>
                                <tr>
                                    <td><input type="text" className="form-control" value="10"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                    <td><input type="text" className="form-control" value="10"/></td>
                                    <td><input type="text" className="form-control" value="9"/></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col-2 p-0">
                            <table className="table gradescore__table">
                                <thead>
                                <th><h6 className="text-right gradescore__table--score">Total Score</h6></th>
                                <th><h6 className="text-center gradescore__table--grade">Grade</h6></th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">99.99</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">A</h6></td>
                                </tr>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">25</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">B</h6></td>
                                </tr>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">78</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">C</h6></td>
                                </tr>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">100</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">D</h6></td>
                                </tr>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">10</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">E</h6></td>
                                </tr>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">99.99</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">A</h6></td>
                                </tr>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">25</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">B</h6></td>
                                </tr>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">78</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">C</h6></td>
                                </tr>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">100</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">D</h6></td>
                                </tr>
                                <tr>
                                    <td><h6 className="text-right gradescore__table--score">10</h6></td>
                                    <td><h6 className="text-center gradescore__table--grade">E</h6></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ErrorHandle(AssessmentBased);
