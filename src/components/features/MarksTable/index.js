import React, { Component } from 'react'
import _ from 'lodash';
import GridComponent from '../GridComponent/GridComponent';
import { toAddStylesForColumns, GridValidations } from '../../../util/util';
import QuestionBased from './QuestionBased';
import './index.css';
import ErrorHandle from '../ErrorHandle';

class MarksTable extends Component {

    constructor(props) {
        super(props)
        this.columnStyles = [];
        this.studentRowData = [];
        if (props.studentList !== 'undefined' && Object.keys(props.studentList).length > 0) {
            this.createHeader(props.studentList);
        }
    }
    createHeader = (props) =>{
        let nestedElementFunc = (data,studentData)=>{
            let columnEle = [],studentDatas={};
            data.forEach((ele,i)=>{
                columnEle.push({ 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': {} ,'text':data.Header}] });
            });
            studentDatas = {'columns':data,data:[]};
            toAddStylesForColumns(columnEle,studentDatas);
            return studentDatas;
        };
        let nestedCellFunc = (inputData,studentData,index) => {
            let CellEle = [],datas=[],studentDatas={};
            inputData.forEach((ele,i)=>{
                if (typeof ele.Validation !== 'undefined' && Object.keys(ele.Validation).length > 0) {
                    CellEle.push({'Cell': (data) => [{ 'tag': 'input', 'elementProps': { 'type': 'text', 'disabled': typeof this.props.inputBoxStatus !== 'undefined' && this.props.inputBoxStatus ? 1 : 0, 'class': `form-control form-label ${this.validateInput_UpdateStyle(data, ele['accessor'])}` + `${[data.valid]}`, "key": `${[data.index]}_${[data.id]}_${[ele.accessor]}_${Math.random()*1000|0}_${Math.random()*10000|0}`, "id": `${[data.index]}_${[data.id]}_${ele['accessor']}_Activites${inputData[0].Header}`, 'defaultValue': data.value, "onBlur": (e) => { this.handleonBlur(e, data) } } }, { 'tag': 'div', 'elementProps': { 'class': 'bg__warninginput--hover' }, "elements": [{ "tag": "p", "elementProps": {}, "text": `Range:${data.condition.min}-${data.condition.max},Text:${data.condition.enumerate}` }] }] });
                }else if (typeof ele.Activites !== 'undefined' && ele.Activites.length > 0) {
                    CellEle.push({nestedElement:nestedElementFunc(ele.Activites,props), nestedCell:nestedCellFunc(ele.Activites,props)});
                }  else {
                    CellEle.push({'Cell': (data) => [{ 'tag': 'h6', 'elementProps': { 'class': 'student__content student__contentdetails' }, 'elements': [{ 'tag': 'span', 'elementProps': { }, 'text': data.value } ]}] });
                }
            });
            studentData.data.map((el)=>{
                datas.push(el[`Activites${inputData[0].Header}`])
            });
            studentDatas = {'columns':inputData.map((el=>({'Header':el['Header'],'accessor':el['accessor'],Validation:el['Validation']}))),data:datas};
            toAddStylesForColumns(CellEle,studentDatas);
            return studentDatas;
        }
        props.columns.forEach((ele,i)=>{
                if(typeof props.data[i] !== 'undefined' && typeof props.data[i][ele.accessor] !== 'undefined' && typeof props.data[i][ele.accessor]['name'] !== 'undefined'){
                    this.columnStyles.push({ 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': {} ,'text':data.Header}], 'Cell': (data) => [{ 'tag': 'img', 'elementProps': { 'class': 'student__content--avatar', 'src': data.image } }, { 'tag': 'h6', 'elementProps': { 'class': 'student__content student__contentdetails' }, 'elements': [{ 'tag': 'span', 'elementProps': { 'class': 'student__content--name' }, 'text': data.name }] }] });
                } else if (typeof ele.Validation !== 'undefined' && Object.keys(ele.Validation).length > 0) {
                    this.columnStyles.push({ 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': {} ,'text':data.Header}], 'Cell': (data) => [{ 'tag': 'input', 'elementProps': { 'type': 'text', 'disabled': typeof this.props.inputBoxStatus !== 'undefined' && this.props.inputBoxStatus ? 1 : 0, 'class': `form-control form-label ${this.validateInput_UpdateStyle(data, ele['accessor'])}` + `${[data.valid]}`, "key": `${[data.index]}_${[data.id]}_${[ele.accessor]}_${Math.random()*1000|0}_${Math.random()*10000|0}`, "id": `${[data.index]}_${[data.id]}_${ele['accessor']}`, 'defaultValue': data.value, "onBlur": (e) => { this.handleonBlur(e, data) } } }, { 'tag': 'div', 'elementProps': { 'class': 'bg__warninginput--hover' }, "elements": [{ "tag": "p", "elementProps": {}, "text": `Range:${data.condition.min}-${data.condition.max},Text:${data.condition.enumerate}` }] }] });
                }else if (typeof ele.Activites !== 'undefined' && ele.Activites.length > 0) {
                    this.columnStyles.push({nestedElement:nestedElementFunc(ele.Activites,props), nestedCell:nestedCellFunc(ele.Activites,props,i)});
                }  else {
                    this.columnStyles.push({ 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': {} ,'text':data.Header}], 'Cell': (data) => [{ 'tag': 'h6', 'elementProps': { 'class': 'student__content student__contentdetails' }, 'elements': [{ 'tag': 'span', 'elementProps': { }, 'text': data.value } ]}] });
                }
            })
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if (typeof nextProps.studentList !== 'undefined' && Object.keys(nextProps.studentList).length > 0) {
               this.createHeader(nextProps.studentList);
        }
    }


    validateInput_UpdateStyle(data, col) {
        let validatator = new GridValidations(data);
        let cssStyle = '';
        if (!validatator.validationRangeAndString(isNaN(parseInt(data.value)) ? data.value : Math.round(data.value))) {
            this.props.UpdateInputValidState(`${[data.index]}_${[data.id]}_${col}`);
            cssStyle = 'bg__warninginput';
        }
        return cssStyle;
    }

    handleonBlur(e, data) {
        // Validation Starts
        let arrOffset = e.target.id.split("_");
        let inputVal = e.target.value;
        let validatator = new GridValidations(data);
        let IsInputValid = validatator.validateMarks(e);
        this.props.OnChangeData(IsInputValid, arrOffset, inputVal);
    }

    render() {
        const { marksEntryType, studentList } = this.props // will come from parent which in turn will be connected to store. Reusable by marks approval page
        if (typeof studentList !== 'undefined' && Object.keys(studentList).length > 0) { toAddStylesForColumns(this.columnStyles, studentList);console.log(studentList); }
        return (<div className="marksentry__table">
            <div className="container-fluid" id="mark__container">
                <div className="row">
                    <div className="col-12 p-0">
                        {(typeof studentList !== 'undefined' && Object.keys(studentList).length > 0) && (marksEntryType && marksEntryType === 'assessment') ?
                            <GridComponent {...this.props} /> : <QuestionBased {...this.props} keys={new Date().getTime()} />}
                    </div>

                </div>
            </div>
        </div>
        )
    }
}
export default ErrorHandle(MarksTable);
