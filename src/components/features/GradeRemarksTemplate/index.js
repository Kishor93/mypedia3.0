import React, { Component } from "react";
import AccordionWrapper from "../AccordionWrapper";
import ErrorHandle from '../ErrorHandle';
import DefaultRemarksTemplate from '../../features/DefaultRemarksTemplate';
import GradeColorScale from '../../features/GradeColorScale';
// import Button;

class GradeRemarksTemplate extends Component {

    render() {
        const acc_data = {
            "containerId": "remarks_accordion",
            "containerStyles": "accordion",
            "headerWrapperStyles": "card-header d-flex",
            "headerCol": "col-lg-9 col-md-6 col-sm-7 col-7 p-0",
            "headerStyles": "remark__heading",
            "buttonWrapperStyles": "col-lg-3 col-md-6 col-sm-5 col-5 p-0 align-self-center d-flex justify-content-end"
        }
        const data = {
            "accordions": [
                {
                    "header": {
                        "headingContent": "Performing Well",
                        "actionItems": {
                            "tag": "button",
                            "elementProps": {
                                "class": "btn btn-link remarksacc__btn",
                                "type": "button",
                                "data-toggle": "collapse",
                                "data-target": "#collapseOne",
                                "aria-expanded": "true",
                                "aria-controls": "headingOne"
                            },
                            'elements': [{
                                "tag": "i",
                                "ellementProps": {
                                    "class": "material-icons acc__icon"
                                },
                                "text": "more"
                            }]
                        }
                    },
                    'body': [{
                        "tag": DefaultRemarksTemplate,
                        'elementProps': { "class": "" }
                    }],
                    "showAccordion": true
                },
                {
                    "header": {
                        "headingContent": "Average",
                        "actionItems": {
                            "tag": "button",
                            "elementProps": {
                                "class": "btn btn-link remarksacc__btn",
                                "type": "button",
                                "data-toggle": "collapse",
                                "data-target": "#collapseTwo",
                                "aria-expanded": "true",
                                "aria-controls": "headingTwo"
                            },
                            'elements': [{
                                "tag": "i",
                                "ellementProps": {
                                    "class": "material-icons acc__icon"
                                },
                                "text": "more"
                            }]
                        }
                    },
                    'body': {
                        "tag": DefaultRemarksTemplate, 'elementProps': {}
                    },
                    "showAccordion": true
                }
            ]
        };
        return (
            <AccordionWrapper accordions={data.accordions} acc_data={acc_data} />
        )
    }
}

export default ErrorHandle(GradeRemarksTemplate);

