import React from 'react';
import ErrorHandle from '../ErrorHandle';

class GradeColorScale extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            parentStyle: "grade__level table table-sm grade__percentage grade__percentage__pt3",
            labelStyle: "grade__percentage__level",
            bodyStyle: "col-12",
            rowStyle: "d-flex",
            ariaLabel: "The grade scales for T1 template are: G3; from 0 to 35, G2; from 36 to 70, G1; from 71 to 100",
            element: [{
                tableStyle: "table table-sm grade__scale",
                grade:
                [{ minMarks: "0", maxMarks: "35", label:"Fail", iconStyle: "grade__fail grade__icon",class:"grade__scale__level text-center col"},
            ],
                grade:
                    [{ minMarks: "0", maxMarks: "35", label: "Lowest", iconStyle: "grade__por grade__icon", class: "grade__scale__level grade__scale--lowest text-center col" },
                    { minMarks: "36", maxMarks: "70", label: "Average", iconStyle: "grade__avg grade__icon", class: "grade__scale__level grade__scale--average text-center col" },
                    { minMarks: "71", maxMarks: "100", label: "Highest", iconStyle: "grade__high grade__icon", class: "grade__scale__level grade__scale--highest text-center col" }]
            }, {
                tableStyle: "table table-sm table-borderless grade__color__indicator",
                grade:
                    [{ label: "C", bgStyle: "grade__color grade__color--lowest text-center col" },
                    { label: "B", bgStyle: "grade__color grade__color--average text-center col" },
                    { label: "A", bgStyle: "grade__color grade__color--highest text-center col" }]
            }]
        }
    }
    render() {
        return (
            <div>
                <div className={this.state.parentStyle} aria-label={this.state.ariaLabel}>
                    {this.state.element.map((val, i) => {
                        return (
                            <table className={val.tableStyle}>
                                <tbody className={this.state.bodyStyle}>
                                    <tr className={this.state.rowStyle}>
                                        {val.grade.map((value) => {
                                            return (
                                                (i == 1) ? <td className={value.bgStyle}>{value.label}</td>
                                                    : <td className={value.class}><span><span className={value.iconStyle}></span>
                                                    </span>{value.label} ({value.minMarks} to {value.maxMarks})</td>
                                            )
                                        })}
                                    </tr>
                                </tbody>
                            </table>
                        )
                    })}
                    <div>
                        {Array.apply(null, { length: this.state.element[0].grade.length + 1 }).map((value) => {
                            return <div className={this.state.labelStyle}></div>
                        })}
                    </div>
                </div>
            </div>
        )
    }
}
export default ErrorHandle(GradeColorScale);