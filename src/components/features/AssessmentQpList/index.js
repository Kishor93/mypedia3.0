import React, { Component } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import './index.css';
import ErrorHandle from '../ErrorHandle';
import {getAssessmentQpList} from '../../../state/assessmentQpList/assessmentQpListAction';

class AssessmentQpList extends Component {
    constructor(props) {
        super(props);
        this.radioRef = React.createRef();
    }
    componentDidMount(){
        this.props.getAssessmentQpList(this.props.parentLineItemID,this.props.lineItemId);
    }

    onMapAssessment = (mapStatus) => {
        return false;
        let assessmentId = mapStatus?this.radioRef.current.id:"";
        let postData = {
            "metadata": [
                {
                    "schoolid": window.schoolId,
                    "line_item_id": this.props.lineItemId,
                    "qpid": assessmentId,
                    "apiname": "MapQPWithAssessment",
                    "module": "MARKENTRY",
                    "action": "MAPASSESSMENT"
                }
            ],
            "key": "REACT_API_STG",
            "date": new Date().getTime(),
            "token": ""
        }
        axios.post('',{data: btoa(JSON.stringify(postData))});
    }

    render() {
        let {assessmentQpList,markEntryStatus,mappingStatus} = this.props;
        return (
            <section class="questionpaper">
                <div class="questionpaper__head">
                    <h5 class="questionpaper__heading my-3">Associated Question Paper</h5>
                    <p class="questionpaper__para mb-3">"Mathematics" question papers associated for 'Periodic Test 1'</p>
                    {mappingStatus?<button className="button button--small button--grey mr-2" onClick={this.onMapAssessment} disabled={markEntryStatus}>UnMap QP</button>:''}
                </div>
                <div class="questionpaper__body">
                    {Object.keys(assessmentQpList).length > 0 ? assessmentQpList.subjectQPDetails.map((values,i)=>{
                        return <div className="questionpaper__item" id={values.qpId}>
                            <input type="radio" className="questionpaper__radio" ref={this.radioRef} name="question__radio" id={`${values.qpId}`} />
                                <label className="questionpaper__label" htmlFor={`${values.qpId}`}>
                                <span className="questionpaper__labelhead font-weight-bold d-inline-block  mb-3">{values.qpName}</span>
                                <span className="questionpaper__labelsub d-block">Dated <span
                                    className="questionpaper__labeldate ml-2">{values.examDate}</span></span>
                            </label>
                        </div>

                    }):''}

                    <div class="questionpaper__footer my-3 text-right">
                        <button class="button button--small button--grey mr-2">Cancel</button>
                        <button class="button button--small button--primary" onClick={this.onMapAssessment} disabled={markEntryStatus}>Map</button>
                    </div>
                </div>

            </section>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        assessmentQpList: typeof state.assessmentQpListReducer !== 'undefined' ? state.assessmentQpListReducer.assessmentQpList : {},
        APIResponseStatus:state.assessmentQpListReducer.APIResponseStatus
    };
};
const mapDispatchToprops = { getAssessmentQpList }
export default connect(mapStateToProps, mapDispatchToprops)(ErrorHandle(AssessmentQpList));
