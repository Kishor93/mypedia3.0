import React from 'react';
import GridComponent from '../GridComponent/GridComponent';
import ErrorHandle from '../ErrorHandle';
import { toAddStylesForColumns, GridValidations } from '../../../util/util';
import NotificationPopup from '../NotificationPopup/NotificationPopup';

class GradeScale extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            allSelected: false,
            studentList: this.props.studentList,
            notificationPopup: this.props.classSection ? true : false,
            notify: {
                msg: 'By default, "8 Point Grade" template is applied to all the classes/sections, as per the CBSE guidelines. You can still over-ride with custom grade template.',
                status: 1
            },
            classNameList: {
                table_className: "table table-responsive table-sm table-bordered assign__grade__template__exam__table",
                th_className: "text-center",
                td_className: "text-center"
            }
        }

        this.columnStyles = [];
        this.studentRowData = [];

        if (this.props.studentList !== 'undefined' && this.props.studentList.length > 0) {
            this.createHeadersForClasses(this.props.studentList);
        }
    }

    componentDidMount() {
        this.initialCheck();
    }

    componentWillReceiveProps() {
        if (this.checkIfEverythingChecked(this.props.studentList)) {
            this.setState({
                allSelected: true
            });
        }
        else {
            this.setState({
                allSelected: false
            });
        }
    }

    closePopup = () => this.setState((prevState) => ({ notificationPopup: !prevState.notificationPopup }));

    initialCheck = () => {
        var studentList = this.props.studentList;
        studentList.forEach((classObject, classIndex) => {
            if (this.checkIfAllColumnsChecked(studentList, classIndex)) {
                studentList[classIndex].columns[0].checked = true;
            }
            else {
                studentList[classIndex].columns[0].checked = false;
            }
        })

        if (this.checkIfEverythingChecked(studentList)) {
            this.setState({
                allSelected: true
            });
        }
        else {
            this.setState({
                allSelected: false
            });
        }
        studentList = this.syncCheckAndPointGrade(studentList);

        this.props.changeStudentList(studentList, "mount");
    }

    syncCheckAndPointGrade = studentList => {
        studentList.forEach((classObject, classIndex) => {
            classObject.columns.forEach((column, index) => {
                if (column.checked) {
                    if (classObject.data[0][column.accessor].status === "set") {
                        classObject.data[0][column.accessor].status = "updated";
                    }
                }
                else {
                    if (classObject.data[0][column.accessor].status === "updated") {
                        classObject.data[0][column.accessor].status = "set";
                    }
                }
            });
        });
        return studentList;
    }

    createHeadersForClasses = (studentList) => {
        studentList = studentList.forEach((classObject, classIndex) => {
            if (classObject !== 'undefined' && Object.keys(classObject).length > 0) {
                this.createHeader(classObject);
            }
        });
    }

    validateInput_UpdateStyle(data, col) {
        let cssStyle = '';
        return cssStyle;
    }

    createHeader = (data) => {
        data.columns.forEach((ele, i) => {
            if (typeof data.data[i] !== 'undefined' && typeof data.data[i][ele.accessor] !== 'undefined' && typeof data.data[i][ele.accessor]['name'] !== 'undefined') {
                this.columnStyles.push({
                    'Element': (data) => [{
                        'tag': 'div',
                        'elementProps': {
                            'class': 'md__check md__check--inline assign__grade__template__exam__table--inline',
                            onClick: (e) => { this.handleOnClick(e, data, i); }
                        },
                        'elements': [{
                            'tag': 'label',
                            'elementProps': {
                                'class': 'material__checkbox assign__grade__template__exam__table--material',
                                'htmlFor': `${[data.index]}_${[data.id]}_${ele['accessor']}_${[i]}`
                            },
                            'text': 'label',
                            "elements": [{
                                'tag': 'input',
                                'elementProps': {

                                    'type': 'checkbox',
                                    'disabled': typeof this.props.inputBoxStatus !== 'undefined' && this.props.inputBoxStatus ? 1 : 0,
                                    'class': `assign__grade__template__exam__table__checkboxone ${this.validateInput_UpdateStyle(data, ele['accessor'])}` + `${[data.valid]}`,
                                    "key": `${[data.index]}_${[data.id]}_${[ele.accessor]}_${Math.random() * 1000 | 0}_${Math.random() * 10000 | 0}`,
                                    "id": `${[data.index]}_${[data.id]}_${ele['accessor']}`,
                                    "name": `${[data.index]}_${[data.id]}_${ele['accessor']}_${[i]}`,
                                    "checked": data.checked
                                }
                            },
                            {
                                'tag': 'span',
                                'elementProps': {
                                    'class': ''
                                },
                                'text': data.className
                            }]
                        },
                        {
                            'tag': 'div',
                            'elementProps': {
                                'class': 'bg__warninginput--hover'
                            }
                        }]
                    }]
                });
            } else if (typeof ele.Validation !== 'undefined' && Object.keys(ele.Validation).length > 0) {
                this.columnStyles.push({
                    'Element': (data) => [{
                        'tag': 'div',
                        'elementProps': {
                            'class': 'md__check md__check--inline assign__grade__template__exam__table--inline',
                            onClick: (e) => { this.handleOnClick(e, data, i); },
                        },
                        'elements': [{
                            'tag': 'label',
                            'elementProps': {
                                'class': data.index == 0 ? 'material__checkbox assign__grade__template__exam__table__main--material' : 'material__checkbox assign__grade__template__exam__table--material',
                                'htmlFor': `${[data.index]}_${[data.id]}_${ele['accessor']}_${[i]}`
                            },
                            'text': 'label',
                            "elements": [{
                                'tag': 'input',
                                'elementProps': {

                                    'type': 'checkbox',
                                    'disabled': typeof this.props.inputBoxStatus !== 'undefined' && this.props.inputBoxStatus ? 1 : 0,
                                    'class': `assign__grade__template__exam__table__checkboxone ${this.validateInput_UpdateStyle(data, ele['accessor'])}` + `${[data.valid]}`,
                                    "key": `${[data.index]}_${[data.id]}_${[ele.accessor]}_${Math.random() * 1000 | 0}_${Math.random() * 10000 | 0}`,
                                    "id": `${[data.index]}_${[data.id]}_${ele['accessor']}`,
                                    "name": `${[data.index]}_${[data.id]}_${ele['accessor']}_${[i]}`,
                                    "checked": data.checked
                                }
                            },
                            {
                                'tag': 'span',
                                'elementProps': {
                                    'class': ''
                                },
                                'text': `${[data.value ? data.value : data.Header]}`
                            }]
                        },
                        {
                            'tag': 'div',
                            'elementProps': {
                                'class': 'bg__warninginput--hover'
                            }
                        }]
                    }],
                    'Cell': (data) => [{
                        'tag': 'div',
                        'elementProps': {
                            'class': 'assign__grade__template__exam__table__content'
                        },
                        'elements': [{
                            'tag': 'p',
                            'elementProps': {
                                'class': data.status == "apply" ? 'assign__grade__template__exam__table__content__contxt__apply' : 'assign__grade__template__exam__table__content__contxt'
                            },
                            'elements': [{
                                'tag': 'img',
                                'elementProps': {
                                    'class': data.status == "apply" ? 'assign__grade__template__exam__table__content__contxt__img__apply' : 'assign__grade__template__exam__table__content__contxt__img',
                                    'src': data.status == "set" || data.status == "updated" ? 'images/standard__temp.svg' : data.status == "apply" ? 'images/applying-icon.svg' : ''
                                }
                            },
                            {
                                'tag': 'span',
                                'text': data.status == "apply" ? "Applying..." : data.value && data.status == "updated" ? data.scaleValue : data.value
                            }]
                        }]
                    }]
                });
            } else {
                this.columnStyles.push({
                    'Element': (data) => [{
                        'tag': 'h6',
                        'elementProps': {},
                        'text': data.Header
                    }],
                    'Cell': (data) => [{
                        'tag': 'h6',
                        'elementProps': {
                            'class': 'student__content student__contentdetails'
                        },
                        'elements': [{
                            'tag': 'span',
                            'elementProps': {},
                            'text': data.value
                        }]
                    }]
                });
            }
        })
    }

    selectAll = (e) => {
        let that = this;
        this.setState({
            allSelected: !this.state.allSelected
        }, () => {
            this.props.studentList.forEach((classObject, classIndex) => {
                classObject.columns.forEach((column, columnIndex) => {
                    column.checked = that.state.allSelected;
                });
            });
            var newList = this.props.studentList;
            that.createHeadersForClasses(newList);
            that.forceUpdate();
        });
    }

    handleOnClick = (e, data, columnIndex) => {
        let tableIndex = [...e.currentTarget.closest('table').parentElement.children].indexOf(e.currentTarget.closest('table')) - 1;
        if (columnIndex == 0) {
            this.props.studentList[tableIndex].columns.forEach((column, index) => {
                if (index == 0) {
                    column.checked = !this.props.studentList[tableIndex].columns[columnIndex].checked;
                }
                if (index > 0) {
                    column.checked = this.props.studentList[tableIndex].columns[columnIndex].checked;
                }
                if (this.checkIfEverythingChecked(this.props.studentList)) {
                    this.setState({
                        allSelected: true
                    })
                }
                else {
                    this.setState({
                        allSelected: false
                    })
                }
            })
        }
        else {
            this.props.studentList[tableIndex].columns[columnIndex].checked = !data.checked;
            if (this.checkIfAllColumnsChecked(this.props.studentList, tableIndex)) {
                this.props.studentList[tableIndex].columns[0].checked = true;
            }
            else {
                this.props.studentList[tableIndex].columns[0].checked = false;
            }
            if (this.checkIfEverythingChecked(this.props.studentList)) {
                this.setState({
                    allSelected: true
                })
            }
            else {
                this.setState({
                    allSelected: false
                })
            }
        }
        var newList = this.props.studentList;
        this.createHeadersForClasses(newList);
        this.forceUpdate();
    }

    checkIfEverythingChecked = (studentList) => {
        let allChecked = true;
        studentList.forEach((classObject, index) => {
            if (classObject.columns[0].checked == false) {
                allChecked = false;
            }
        })
        return allChecked;
    }

    checkIfAllColumnsChecked = (studentList, tableIndex) => {
        let allChecked = true;
        studentList[tableIndex].columns.forEach((column, index) => {
            if (index > 0 && column.checked == false) {
                allChecked = false;
            }
        })
        return allChecked;
    }

    render() {
        if (typeof this.props.studentList !== 'undefined' && this.props.studentList.length > 0) {
            this.props.studentList.forEach((classObject, classIndex) => {
                if (typeof classObject !== 'undefined' && Object.keys(classObject).length > 0) {
                    toAddStylesForColumns(this.columnStyles, classObject);
                }
            })
        }
        return (
            <div className="row">
                <div className="col-12">
                    {this.state.notificationPopup ? <NotificationPopup text={this.state.notify.msg} closepopup={this.closePopup} status={this.state.notify.status} /> : ""}
                    <div id="grade__scale__assign__category__class" style={{ display: 'block' }}>
                        <div className="assign__grade__template__exam">
                            <table className="table table-responsive table-sm table-bordered assign__grade__template__exam__table__class">
                                <thead>
                                    <tr>
                                        <th className="text-center">
                                            <div className="md__check md__check--inline assign__grade__template__exam__table__main--inline">
                                                <label className="material__checkbox assign__grade__template__exam__table__main--material">
                                                    <input
                                                        type="checkbox"
                                                        className="assign__grade__template__exam__table__checkboxclass"
                                                        id="inline__checkboxclass__main"
                                                        value="Class 1"
                                                        checked={this.state.allSelected}
                                                        onChange={this.selectAll}
                                                    />
                                                    <span>All classes &amp; sections</span>
                                                </label>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            {
                                (typeof this.props.studentList !== 'undefined' && this.props.studentList.length > 0) ?
                                    this.props.studentList.map((classObject, classIndex) => {
                                        if (typeof classObject !== 'undefined' && Object.keys(classObject).length > 0) {
                                            return <GridComponent key={classIndex} {...this.props} studentList={classObject} classNameList={this.state.classNameList} />
                                        }
                                    })
                                    :
                                    ''
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ErrorHandle(GradeScale);