import React from 'react';
import ErrorHandle from '../ErrorHandle'

class Subject extends React.Component {
    render() {
        let { subjectList } = this.props;
        return (
            <div className="row d-flex">
                {
                    subjectList && subjectList.length && subjectList.map((subject, index) => {
                        return subject[this.props.filter] || this.props.filter === "allsub" ?
                            <div key={index} className="col-lg-2 col-md-3 col-sm-3 col-6 pr-0 mypedia__subject">
                                <div className="tab__conatiner__content__product">
                                    <div className="tab__conatiner__content__product__checkbox">
                                        <label className="material__checkbox">
                                            <input type="checkbox" name="inline__checkbox" checked={subject.checked} id="inline__checkboxone" value={subject.name} onClick={e => this.props.handleClick(e, index, this.props.classIndex)} />
                                            <span></span>
                                        </label>
                                    </div>
                                    {
                                        subject.mypedia ?
                                            <div className="tab__conatiner__content__product__course">
                                                <img src="images/pearson-sub-icon.svg" alt="" />
                                            </div>
                                            :
                                            null
                                    }
                                    {
                                        subject.imgSrc ?
                                            <div className="tab__conatiner__content__product__image">
                                                <img className="tab__conatiner__content__product__image__img" src={subject.imgSrc} alt="" />
                                            </div>
                                            :
                                            <div className="tab__conatiner__content__product__icon">
                                                <span className="tab__conatiner__content__product__icon__alpha">{subject.name.substring(0, 1).toUpperCase()}</span>
                                            </div>
                                    }


                                    <div className="tab__conatiner__content__product__heading">
                                        <h6 className="tab__conatiner__content__product__heading__title tooltip__center">{subject.name}</h6>
                                    </div>
                                    <div className={subject.status === "apply" ? "tab__conatiner__content__product__grade tab__conatiner__content__product__grade--applying" : "tab__conatiner__content__product__grade"}>
                                        <img className={subject.status === "apply" ? "tab__conatiner__content__product__grade__img tab__conatiner__content__product__grade__img--applying" : "tab__conatiner__content__product__grade__img"} src={subject.status === "apply" ? "images/applying-icon.svg" : "images/standard__temp.svg"} alt="" />
                                        <span className="tab__conatiner__content__product__grade__content">{subject.status === "updated" ? "6 Point Grade Template" : subject.status === "apply" ? "Applying..." : "8 Point Grade Template"}</span>
                                    </div>
                                </div>
                            </div>
                            :
                            null
                    })
                }
            </div>
        );
    }
}

export default ErrorHandle(Subject);