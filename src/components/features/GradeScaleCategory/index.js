import React from 'react';
import ErrorHandle from '../ErrorHandle';

class GradeScaleCategory extends React.Component {
    constructor(props) {
        super(props);
    }

    admSelectCheck = (e, index, category) => {
        let currentOption = -1;
        category.selectList.map((option, optionIndex) => {
            if (option.value === e.target.value) {
                currentOption = optionIndex;
            }
        });
        this.props.handleCategoryChange(e, index, currentOption);
    }

    render() {
        let { examList } = this.props;
        return (
            <div className="row">
                <div className="col-12">
                    <div className="assign__grade__template__select d-lg-flex d-md-flex d-sm-flex">
                        <div className="col-lg-6 col-md-6 col-sm-6 col-12 d-flex">
                            <h6 className="assign__grade__template__select__heading align-self-center">{examList.title}</h6>
                            {
                                examList.categories.length && examList.categories.map((category, index) => {
                                    return <div key={index} className={(index == 0) ? "form-group form__formgroup assign__grade__template__select__heading__formgroup" : "form-group form__formgroup assign__grade__template__select__heading__formgroup ml-5"} style={(((this.props.currentCategories[0] === -1) && (index === 1 || index === 2)) || (this.props.currentCategories[0] === 1 && (index === 1 || index === 2)) || (this.props.currentCategories[0] === 2 && (index === 1 || index === 2) || (this.props.currentCategories[0] === 0 && index === 2) || (this.props.currentCategories[0] === 3 && index === 1))) ? { display: "none" } : null}>
                                        <label className="form__label assign__grade__template__select__heading__formgroup__label">{category.title}</label>
                                        <select onChange={e => this.admSelectCheck(e, index, category)} id="grade__scale__assign__category" className="form-control form__select assign__grade__template__select__heading__formgroup__select">
                                            <option value="select" selected={this.props.currentCategories[index] == -1} className="assign__grade__template__select__heading__formgroup__select__option">{category.title}</option>
                                            {
                                                category.selectList.length && category.selectList.map((select, listIndex) => {
                                                    return <option key={listIndex} selected={this.props.currentCategories[index] == listIndex} value={select.value} className="assign__grade__template__select__heading__formgroup__select__option">{select.text}</option>
                                                })
                                            }
                                        </select>
                                    </div>
                                })
                            }
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-6 col-12 d-flex align-self-center justify-content-lg-end justify-content-md-end justify-content-sm-end">
                            <div className="assign__grade__template__select__variation" id="assign__grade__template__select__variation" style={{ display: "flex" }}>
                                {
                                    examList.gradeTemplateVariations.length && examList.gradeTemplateVariations.map((variation, index) => {
                                        return <p key={index} className={(index == 0) ? "assign__grade__template__select__variation__standard" : "assign__grade__template__select__variation__applied"}>
                                            <img className={(index == 0) ? "assign__grade__template__select__variation__standard__img" : "assign__grade__template__select__variation__applied__img"} src={variation.imgSrc} />
                                            <span>{variation.text}</span>
                                        </p>
                                    })

                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ErrorHandle(GradeScaleCategory);