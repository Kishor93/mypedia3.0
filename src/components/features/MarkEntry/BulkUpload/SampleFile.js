import React from 'react';

export const SampleSheet = () => {
    return <div class="markentry__bulkupload__entrytemp">
        <div class="markentry__bulkupload__markwarp mx-auto">
            <span class="markentry__bulkupload__markwarplink">
                <img src="images/shape.svg" class="markentry__bulkupload__marimg" alt="shape image" />Marks Entry Template
            </span>
            <ul class="markentry__bulkupload__detailslink">
                {/* <li class="markentry__bulkupload-list">                                                           
                                                            <a class="markentry__bulkupload-listlink" href="#" tabindex="">
                                                                <img src="images/view-icon.svg" class="markentry__bulkupload__marimg" alt="view image"/> 
                                                                View
                                                            </a>     
                                                        </li> */}
                <li class="markentry__bulkupload-list">
                    <a class="markentry__bulkupload-listlink markentry__bulkupload-leftline" href="#" tabindex="">
                        <img src="images/download.svg" class="markentry__bulkupload__marimg" alt="download image" />
                        Download
                                                            </a>
                </li>
            </ul>
        </div>
    </div>
}