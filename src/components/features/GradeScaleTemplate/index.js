import React, { Component } from "react";
import ErrorHandle from "../../features/ErrorHandle";
import TabsWrapper from "../../features/TabsWrapper";
import "./index.css";
import GradeScaleTable from "../GradeScaleTable/index";
import GradeScaleComponentParent from "../GradeScaleComponentParent";

class GradeScaleTemplate extends Component {
  constructor(props) {
    super(props);
    this.state={
      showAssign: false
    }
    this.acc_data = {
      containerId: "remarks_accordion",
      containerStyles: "accordion",
      headerWrapperStyles: "grade__template__header d-flex",
      headerCol: "col-lg-7 col-md-6 col-sm-7 col-4 p-0",
      headerStyles: "remark__heading",
      buttonWrapperStyles:
        "col-lg-5 col-md-6 col-sm-5 col-8 p-0 align-self-center d-flex justify-content-end"
    };
    this.data_acc = {
      accordions: [
        {
          header: {
            headingContent: "3 Point Grade Template"
          },
          buttons: {
            threeDots: {},
            showMore: {}
          },
          showAccordion: true
        }
      ]
    };

    this.data = {
      orientation: "vertical",
      defaultActiveKey: "3pointGradeTemplate",
      navStyles: "nav-item nav-link nav__tab nav  ",
      navLinkStyles: "nav-link tab__content--link active",
      navCol: "nav col-lg-3 col-md-4 col-sm-12 col-12 px--0 flex-column",
      contentCol: "tab-content col-lg-9 col-md-8 col-sm-12 col-12 px--0",
      tabs: [
        {
          navComponent: "Pearson standard template",
          linkStyle: "grade__para",
          contentComponent: "<readonly />"
        },
        {
          eventKey: "3pointGradeTemplate",
          navComponent: "3 Point Grade Template",
          contentComponent: <GradeScaleTable  
                             title={"3 Point Grade Template"} 
                             showAssign={this.showAssign} 
                             disabled/>
        },
        {
          eventKey: "5pointGradeTemplate",
          navComponent: "5 Point Grade Template",
          contentComponent: <GradeScaleTable  
                              title={"5 Point Grade Template"} 
                              showAssign={this.showAssign} 
                              disabled/>
        },
        {
          eventKey: "cbse8pointGradeTemplate",
          navComponent: "CBSE 8 Point Grade Template",
          contentComponent: "<cbse8PointGradeTemplate />"
        },
        {
          navComponent: "Custom Templates",
          linkStyle: "grade__para",
          contentComponent: "<readonly />"
        },
        {
          navComponent: "Custom new template",
          linkStyle: "grade__newitem__link",
          contentComponent: "<readonly />"
        }
      ]
    }
  }

  showAssign = () =>{
    this.setState({showAssign:!this.state.showAssign})
  }

  render() {
    return !this.state.showAssign ? <TabsWrapper data={this.data} /> : <GradeScaleComponentParent goBack={this.showAssign} />
  }
}

export default ErrorHandle(GradeScaleTemplate);
