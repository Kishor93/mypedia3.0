import React from 'react';
import './index.css';
import ErrorHandle from '../ErrorHandle';

class Popup extends React.Component {

    componentDidMount() {
      //  document.body.classList.add('modal-open');
    }

    componentWillUnmount() {
        //document.body.classList.remove('modal-open');
    }

    render() {
        const {closeModal, modalIsOpen, children, id} = this.props;
        const modalClassess = modalIsOpen ? "modal fade popup__show show modal--default" : "modal fade popup__hide modal--default";
        return (
            <>
                {modalIsOpen && <div onClick={closeModal} className="modal-backdrop show fade"></div>}
            
                <section className={modalClassess} id={id}>
                    {children}
                </section>
            </>

        )
    }

}

export default ErrorHandle(Popup);
