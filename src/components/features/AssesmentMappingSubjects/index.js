import React, { Component } from 'react';
import './index.css';
import ErrorHandle from '../ErrorHandle';
import AssessmentQpList from '../AssessmentQpList';

class AssesmentMappingSubjects extends Component {
    constructor(props) {
        super(props);
        this.onTabClick = this.onTabClick.bind(this);
        this.state = { mappingStatus: false, displayAssementQpDetails: false, displayDownArrow: false,lineItemId:'',markEntryStatus:0,NotificationPopup:true  };
    }

    onTabClick(mappingStatus, e,index,lineItemId,markEntryStatus,assessmentId) {
        e.preventDefault();
        this.setState({ mappingStatus: mappingStatus?true:false, displayAssementQpDetails: true, displayDownArrow: true,selectedCnt:index,lineItemId:lineItemId,markEntryStatus:markEntryStatus });
    }
    
    render() {
        const { assesmentSubjects, lineItemId } = this.props;
        let arrowclass = this.state.displayDownArrow ? 'active' : '';
        return (
            <div className="col-12" key={Math.random()*10000}>
                <nav>
                    <ul className="nav nav-tabs marksapproval__cardtabs" id="nav-tabs" role="tablist">
                        {typeof assesmentSubjects !== 'undefined' && assesmentSubjects.subjects.map((values, i) => {
                            return (
                                <li className="nav-link marksapproval__cardnavelink col-2" key={`${values.lineItemId}_${i}`} style={{ cursor: "pointer" }} onClick={(e) => this.onTabClick(values.mappingStatus, e,i,values.lineItemId,values.markEntryStatus)}>
                                    <a className={`nav-item marksapproval__cardlink ${this.state.selectedCnt==i?arrowclass:''}`} href="#" role="tab" aria-controls="nav__mathematics" aria-selected="true" aria-label="" >
                                        <div className="marksapproval__card">
                                            <img className="marksapproval__card--img" src='images/default.png' />
                                            {values.mappingStatus ?
                                                <div className="marksapproval__cardtick">
                                                    <div className="marksapproved__cardtickradius">
                                                        <i className="material-icons marksapproved__cardtickicon">done</i>
                                                    </div>
                                                </div>
                                            : ''}
                                            <div className="marksapproval__cardblock">
                                                <h4 className="marksapproval__card--title mt-3">{values.subjectName}</h4>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                </nav>
                {this.state.displayAssementQpDetails ? <AssessmentQpList mappingStatus={this.state.mappingStatus} markEntryStatus={this.state.markEntryStatus} parentLineItemID={lineItemId} lineItemId={this.state.lineItemId} /> : ''}
            </div>
        );
    }
}
export default ErrorHandle(AssesmentMappingSubjects);