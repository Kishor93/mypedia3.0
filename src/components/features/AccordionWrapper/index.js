import React, { Component } from 'react';
import ErrorHandle from '../ErrorHandle';
import AccordionContent from '../../features/AccordionContent';
import CreateNewRemark from '../../features/CreateNewRemark';
import GradeColorScale from '../../features/GradeColorScale';

class AccordionWrapper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            moredetails: false,
            accordions: props.accordions,
            acc_data: props.acc_data
        };
        this.viewmoredetails = this.viewmoredetails.bind(this);
    }

    viewmoredetails = (index) => {
        let {accordions} = this.state;
        accordions.forEach((accordion, i) => {
            accordion.showAccordion = index === i ? !accordion.showAccordion : false
        });
        this.setState({accordions});
    } 

    render() {
        const { accordions } = this.state;
        const { acc_data } = this.state;
        return (
            <div>
                <div className={this.state.containerStyles} id={this.state.containerId}>
                    <AccordionContent accordions={accordions} acc_data={ acc_data }  viewmoredetails={this.viewmoredetails} />
                </div>
            </div>
        )
    }

}

export default ErrorHandle(AccordionWrapper);
