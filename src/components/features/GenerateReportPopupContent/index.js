import React from 'react';
import './index.css';
import ErrorHandle from '../ErrorHandle';
import Loader from '../../features/Loader/Loader';


class GenerateReportPopupContent extends React.Component {
    constructor(props){
        super(props)
        this.state = {loading:false};
    }
    generateReport = () => {
        this.setState({loading:true});
        const reportStatusVal=(this.props.reportStatus!=='report_generated'?'not_generated':'report_generated');
        const genReportReqParams = {
            "metadata": [
                {
                    "schoolid": window.schoolId,
                    "classid": window.classSectionId,
                    "lineitemid": this.props.selectedId,
                    "apiname": "ReportcardGenerationRequest",
                    "module": "MARKENTRY",
                    "action": "WRITE",
                    "transactionstatus": "requested",
                    "reportstatus":reportStatusVal
                }
            ],
            "key": "REACT_API_STG",
            "date": new Date().getTime(),
            "token": "null"
        };
        this.props.generateReport(genReportReqParams);
    }
    closeModal = () => {
        //this.props.generateReportResponse.code
        this.props.closeModal();
    }
    
    
    render() {
        const { closeModal, generateReportResponse, assesmentSubjects, selectedId ,reportStatus} = this.props;
        
        //let show = this.state.loading && this.props.generateReportResponse.code==='undefined'?"show":""; 
        let show = "";
        return (
            <React.Fragment>
                <Loader loading={this.state.loading && this.props.generateReportResponse=== null?"show":""}/>
                {generateReportResponse && generateReportResponse.code  == "200" &&
                    <div>
                        <div className="modal-dialog modal-dialog-centered modal__dialog">
                            <div className="modal-content modal__content">
                                <div className="modal__generatereportsuccess">
                                    <div className="modal__generatereportsuccessbox">
                                        <div className="modal__generatereportsuccesstextbox">
                                            <img className="modal__generatereportsuccessimg"
                                                src="images/icon-modal-progress-requestgenerate.svg" alt="" />
                                            <h3 className="modal__generatereportsuccesshead mb-4">"Report card generation"
                                            in progress.</h3>
                                            <p className="modal__generatereportsuccesstext">It might take some time to
                                                generate reports.</p>
                                            <p className="modal__generatereportsuccesstext mb-4">Kindly go to <a href={`/SchoolNetTeacherPortal/Presentation/questionpapermgt/build/index.php?schoolId=${window.schoolId}&teacherId=${window.teacherId}&className=${window.className}&classSectionId=${window.classSectionId}&sectionName=${window.sectionName}#/studentreportview`}
                                                className="modal__generatereportlink">Reports </a>tab
                                            to view the reports.</p>
                                            <button onClick={this.closeModal}
                                                className="button button--small button--primary button--gotit"
                                                data-dismiss="modal">Got it!
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>}
                {!generateReportResponse &&
                    <div className="modal-dialog modal-dialog-centered modal__dialog">
                        <div className="modal-content modal__content">
                            <div className="modal__generatereportoption">
                                <div className="modal-body modal__body modal__body--preview">
                                    <div className="modal__generatereport p-0">
                                        <h4 className="modal__generatereportheading mb-3">Generate Report Card</h4>
                                        {/* <p className="modal__generatereportpara mb-3">The report will be generated based
                                            on your
                                            choice.</p> */}
                                        {/* <div className="modal__generatereportoption mb-3">
                                            <div
                                                className="form-check form-check-inline modal__previewreportcheckbox mr-5">
                                                <input className="form-check-input modal__previewreportcheckboxinput"
                                                    type="checkbox" id="inlineCheckbox1" checked="true" />
                                                <label className="form-check-label modal__previewreportcheckboxlabel"
                                                    htmlFor="inlineCheckbox1">Colour</label>
                                            </div>
                                            <div className="form-check form-check-inline modal__previewreportcheckbox">
                                                <input className="form-check-input modal__previewreportcheckboxinput"
                                                    type="checkbox" id="inlineCheckbox2" checked="" />
                                                <label className="form-check-label modal__previewreportcheckboxlabel"
                                                    htmlFor="inlineCheckbox2">Black and White</label>
                                            </div>
                                        </div> */}
                                        <p className="modal__generatereportpara mb-3">Are you sure you want to Generate ?</p>
                                    </div>
                                </div>

                                <div className="modal-footer modal__footer">
                                    <div className="d-flex d-sm-flex d-md-flex d-lg-flex justify-content-end">
                                        <button onClick={this.closeModal}
                                            className="button button--small button--grey mr-2"
                                            data-dismiss="modal">Cancel
                                        </button>
                                        <button onClick={this.generateReport}
                                            className="button button--small button--primary button--generatereport  ml-2">Confirm
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {generateReportResponse && generateReportResponse.code != "200" &&
                    <div>
                        <div className="modal-dialog modal-dialog-centered modal__dialog">
                            <div className="modal-content modal__content">
                                <div className="modal__generatereportsuccess">
                                    <div className="modal__generatereportsuccessbox modal__generatereportsuccessbox--failure">
                                        <div className="modal__generatereportsuccesstextbox modal__generatereportsuccessbox--failuretextbox">
                                            {/* <img className="modal__generatereportsuccessimg"
                                                src="images/icon-modal-progress-requestgenerate.svg" alt="" /> */}
                                            <h3 className="modal__generatereportsuccesshead modal__generatereportfailuretextbox mb-4">Report card generation
                                            Failed</h3>
                                            <p className="modal__generatereportsuccesstext">Please contact Administrator</p>
                                            <button onClick={this.closeModal}
                                                className="button button--small button--primary button--gotit"
                                                data-dismiss="modal">Close
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </React.Fragment>

        )
    }

}

export default ErrorHandle(GenerateReportPopupContent);