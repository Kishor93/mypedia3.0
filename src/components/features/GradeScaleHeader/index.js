import React from 'react';
import ErrorHandle from '../ErrorHandle';

class GradeScaleHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    applyExamCancel = (e) => {
        this.props.handleCancel(e);
    }

    applyExamShow = (e) => {
        this.props.handleAssign(e);
    }

    applyBack = (e) => {
        this.props.handleBack(e);
    }

    render() {
        let { headerComponents } = this.props;
        let leftButtonsExist = false;
        return (
            <div className="row">
                <div className="col-12">
                    <div className="assign__grade__template__header d-lg-flex d-md-flex d-sm-flex">
                        <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                            {
                                headerComponents.buttons.length && headerComponents.buttons.map((button, index) => {
                                    if (button.orientation == "left") {
                                        leftButtonsExist = true;
                                        return <button type="button" className={button.class} onClick={this.applyBack}>
                                            {
                                                button.icon ?
                                                    <i className="material-icons assign__grade__template__header__btn__icon">{button.icon.name}</i>
                                                    :
                                                    null
                                            }

                                            {button.title}
                                        </button>
                                    }
                                })
                            }
                            {
                                leftButtonsExist ?
                                    <div className="assign__grade__template__header__spacing"></div>
                                    :
                                    null
                            }

                            <h6 className="assign__grade__template__header__heading">Assign - 6 Point Scale</h6>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-6 col-12 align-self-center d-flex justify-content-lg-end justify-content-md-end justify-content-sm-end">
                            {
                                headerComponents.buttons && headerComponents.buttons.map((button, index) => {
                                    if (button.orientation == "right") {
                                        return <button type="button" onClick={button.title == "Cancel" ? this.applyExamCancel : button.title == "Assign" ? this.applyExamShow : null} className={button.class}>
                                            {button.title}
                                        </button>
                                    }
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ErrorHandle(GradeScaleHeader);