import React from 'react';
import {shallow} from 'enzyme';
import GridComponent from './GridComponent';
import {toAddStylesForColumns} from "../../../util/util";

it('renders without crashing', () => {

    let studentList = {
        "columns": [
            {
                "Header": "Name",
                "accessor": "name",
                "Validation": {}
            }
        ],
        "data": [
            {
                "name": {
                    "name": "AADYA JOSHI",
                    "image": "images\/avatar.svg"
                }
            }
        ]
    };

    var columnStyles = [
        {
            'Element': {'tag': 'h6', 'elementProps': {}},
            'Cell': (data) => [{
                'tag': 'img',
                'elementProps': {'class': 'student__content--avatar', 'src': data.image}
            }, {
                'tag': 'h6',
                'elementProps': {'class': 'student__content student__contentdetails'},
                'elements': {'tag': 'span', 'elementProps': {'class': 'student__content--name'}, 'text': data.name}
            }]
        }
    ];

    toAddStylesForColumns(columnStyles, studentList);

    shallow(<GridComponent studentList={studentList}/>);
});


it('renders 2 rows and 2 columns with a header', () => {
    let studentList = {
        "columns": [
            {
                "Header": "Name",
                "accessor": "name",
                "Validation": {}
            },
            {
                "Header": "Roll No",
                "accessor": "rollNo",
                "Validation": {}
            },
        ],
        "data": [
            {
                "name": {
                    "name": "AADYA JOSHI",
                    "image": "images\/avatar.svg"
                },
                "rollNo": {
                    "value": "763497530"
                },
            },
            {
                "name": {
                    "name": "AADYA JOSHI",
                    "image": "images\/avatar.svg"
                },
                "rollNo": {
                    "value": "763497530"
                },
            }
        ]
    };

    var columnStyles = [
        {
            'Element': {'tag': 'h6', 'elementProps': {}},
            'Cell': (data) => [{
                'tag': 'img',
                'elementProps': {'class': 'student__content--avatar', 'src': data.image}
            }, {
                'tag': 'h6',
                'elementProps': {'class': 'student__content student__contentdetails'},
                'elements': {'tag': 'span', 'elementProps': {'class': 'student__content--name'}, 'text': data.name}
            }]
        },
        {
            'Element': {'tag': 'h6', 'elementProps': {'class': ""}},
            'Cell': (data) => [{'tag': 'h6', 'elementProps': {'class': 'text-center'}, 'text': data.value}]
        },
    ];

    toAddStylesForColumns(columnStyles, studentList);

    let wrapper = shallow(<GridComponent studentList={studentList}/>);
    let table = wrapper.find('.table');
    expect(table).toHaveLength(1);
    expect(table.find('thead').find('th')).toHaveLength(2);
    expect(table.find('tbody').find('tr')).toHaveLength(2);

});


it('renders checkbox column without header', () => {
    let studentList = {
        "columns": [
            {
                "Header":"Check Box",
                "accessor":"id",
                "Validation":{  }
            },
            {
                "Header": "Name",
                "accessor": "name",
                "Validation": {}
            }
        ],
        "data": [
            {
                "id":{
                    "value":"266243",
                    "name":"AADYA JOSHI",
                    "link":"https:\/\/mypedia-restapi.appspot.com\/downloadreport?pdfPath=2018 - 2019\/434\/Class 4\/SectionA\/studentcumulativereport\/266243.pdf&requestId=PRC_1552477045431_239"
                },
                "name": {
                    "name": "AADYA JOSHI",
                    "image": "images\/avatar.svg"
                }
            }
        ]
    };

    var columnStyles = [
        {
            'Element': {'tag': 'span', 'elementProps': {}},
            'Cell': (data) => [{
                'tag': 'input',
                'elementProps': {
                    'type': 'checkbox',
                    'class': 'form-control form-label checkboxClass',
                    'disabled': data.link ? false : true,
                    "id": `${[data.value]}`,
                    "onClick": (e) => {
                        this.validateSelectInput(e, data)
                    }
                }
            }]
        },
        {
            'Element': {'tag': 'h6', 'elementProps': {}},
            'Cell': (data) => [{
                'tag': 'img',
                'elementProps': {'class': 'student__content--avatar', 'src': data.image}
            }, {
                'tag': 'h6',
                'elementProps': {'class': 'student__content student__contentdetails'},
                'elements': {'tag': 'span', 'elementProps': {'class': 'student__content--name'}, 'text': data.name}
            }]
        }
    ];

    toAddStylesForColumns(columnStyles, studentList);

    let wrapper = shallow(<GridComponent studentList={studentList}/>);
    let table = wrapper.find('.table');
    expect(table.find('thead').find('th').find('span').text()).toBe("");
});




