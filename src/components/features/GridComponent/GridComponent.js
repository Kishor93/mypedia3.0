import React, { Component } from 'react'

export default class GridComponent extends Component {
    render() {
        if(this.props.classNameList){
            var {table_className, table_id, thead_className, tbody_className, td_className, tr_className, th_className} = this.props.classNameList;
        }
        if (typeof this.props.studentList === 'undefined') { return false; }
        let { columns, data } = this.props.studentList;
        return <table className={table_className ? table_className : "table"} id={table_id ? table_id : null} >
            <thead>
                {
                    columns.length > 0 ? columns.map((values, i) => {
                        if (typeof values.nestedElement !== 'undefined') {
                            return <th key={i} className={th_className ? th_className : null}><GridComponent studentList={values.nestedElement} /></th>
                        } else if (typeof values.Element !== 'undefined') {
                            return <th key={i} className={th_className ? th_className : null}>{renderElement(React, values.Element, { ...values })}</th>
                        }

                    }) : ''
                }
            </thead>
            <tbody>
                {
                    data.length > 0 ? data.map((values, i) => {
                        let td = Object.keys(values);
                        return <tr key={i}>{td.map((value, key) => {
                            let col = columns[key], rowData = values[value];
                            if (typeof col !== 'undefined' && typeof col.nestedCell !== 'undefined') {
                                return <td className={td_className ? td_className : null}><GridComponent studentList={{ columns: col.nestedCell.columns, data: [col.nestedCell.data[i]] }} /></td>;
                            }
                            return <td key={key} className={td_className ? td_className : null}>{typeof col !== 'undefined' && typeof col.Cell !== 'undefined' ? renderElement(React, col.Cell, { ...rowData, ...col.Validation }) : ''}</td>
                        })}</tr>
                    }) : ''
                }

            </tbody>
        </table>
    }
}

function renderElement({ createElement }, cellData, rowData) {
    let recursive = (data) => {
        return createElement(data.tag, { ...data.elementProps },
            typeof data.elements !== 'undefined' && typeof data.text !== 'undefined' ?
                (data.text, data.elements.map((ele) => recursive(ele))) :
                typeof data.elements !== 'undefined' ? data.elements.map((ele) => recursive(ele)) :
                    typeof data.text !== 'undefined' ? data.text : null);
    }
    return cellData(rowData).map((values, i) => recursive(values));
}

export class GridValidations {
    constructor() {
        this.data = {}; this.changedData = []; this.errorCol = {};
    }
    setData(data) {
        this.data = data;
    }
    getData() {
        return this.data;
    }
    validationRangeAndString(value) {
        const { min, max, enumerate } = this.data.condition;
        return typeof value === "string" && isNaN(parseInt(value)) ? enumerate.indexOf(value) !== -1 ? true : false : min <= value && max >= value ? true : false;
    }
    incrementError(ele) {
        this.errorCol[ele] = 1;
    }

}




