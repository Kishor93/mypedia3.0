import React, { Component } from 'react';
// import {Nav, Tab, Row, Col} from "react-bootstrap";
import DefaultRemarksTemplate from '../../features/DefaultRemarksTemplate'

class AccordionContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            moredetails: false,
            error: false,
            info: null
        };
        
    }
    
    
    render() {
        // let recursive;
        const {accordions} = this.props;
        const {headerWrapperStyles, headerCol, headerStyles, buttonWrapperStyles, containerId} = this.props.acc_data;
        // let accc
        return (
            <div>
                {accordions && accordions.map((acc, index) => {
                    return (
                        <div key={index} className="card">
                            <div className={headerWrapperStyles} id={`heading${index}`}>
                                <div className={headerCol}>
                                    <h6 className={headerStyles}>{acc.header.headingContent}</h6>
                                </div>
                                <div className={buttonWrapperStyles}>
                                {/* {React.createElement('h6',{class:'float-left'},'Name')} */}
                                {/* {renderElement(React, acc.header.actionItems)} */}
                                <i id={`rmk--${index}`} className="material-icons rmk__icon active">more_vert</i>
                                    <div className="duplicate__remarks">

                                    </div>
                                    <button className="btn btn-link remarksacc__btn" type="button" data-toggle="collapse" data-target={`#collapse${index}`} aria-expanded="true" aria-controls={`heading${index}`} onClick={() =>this.props.viewmoredetails(index)}>
                                        {acc.showAccordion ? <i className="material-icons acc__icon">remove</i> : <i className="material-icons acc__icon">add</i>}
                                    </button>
                                    

                                </div>
                            </div>
                            <div id={`collapse${index}`} className={`collapse ${acc.showAccordion ? 'show' : ''} remacc__cols`} aria-labelledby={`heading${index}`} data-parent={`#${containerId}`}>
                                <div className="card-body">
                                {acc.body.length > 0 && acc.body.map((content, key) => {
                                    return (
                                        React.createElement(content.tag, {...content.elementProps}, null)    
                                    )
                                    })
                                 }       
                                </div>
                            </div>
                        </div>

                    )
                })
                }
            </div>
        )
    }
}

// function renderElement ({ createElement }, actionItems) {
//     let recursive = (actionItems) => {
//         console.log("data", actionItems)
//         return createElement(actionItems.tag, { ...actionItems.elementProps },
//             typeof actionItems.elements !== 'undefined'&& typeof actionItems.text !== 'undefined'?
//                 (actionItems.text,actionItems.elements.map((ele)=>recursive(ele))):
//                 typeof actionItems.elements !== 'undefined'?actionItems.elements.map((ele)=>recursive(ele)):
//                     typeof actionItems.text !== 'undefined'?actionItems.text:null);
//     }
//     // return cellData(rowData).map((values, i) => recursive(values));
// }

export default AccordionContent;
