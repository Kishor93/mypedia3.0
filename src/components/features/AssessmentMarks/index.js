import React, { Component } from 'react';
import axios from 'axios';
import {connect} from 'react-redux'
import { getStudentData, updateStudentData, postStudentData, updateMarkApprovalStatus,updateSucessStudentListData,postStatusSucessListData } from '../../../state/studentData/studentAction';
import { getAssesmentSubjects } from '../../../state/assesmentSubjects/assesmentSubjectAction';
import MarksTable from "../MarksTable";
import './index.css';
import NotificationPopup from '../NotificationPopup/NotificationPopup.js';
import ErrorHandle from '../ErrorHandle';
import Loader from '../../features/Loader/Loader';
import {calculateSumOfMarks} from '../../../util/util.js';

class AssessmentMarks extends Component {

    constructor(props) {
        super(props)
        this.OnApproveClick = this.OnApproveClick.bind(this);
        this.OnPopUpClose = this.OnPopUpClose.bind(this);
        this.OnChangeData = this.OnChangeData.bind(this);
        this.UpdateInputValidState = this.UpdateInputValidState.bind(this);
        this.sucessStudentMarkList = [];
        this.failureStudentMarkList = [];
        this.validInputState = [];
        this.state = { sucessTabActive: 1, NotificationPopup: false, notifiy: { msg: '', status: 0 },errMsg: '', popup: 0, approved: props.approved,loading:true,rerender :false };
    }


    OnChangeData(IsInputValid, arrOffset, inputVal) {
        this.setState({rerender:true})
        let key = arrOffset[0] + "_" + arrOffset[1] + "_" + arrOffset[2];
        this.validInputState[key] = IsInputValid ? 'valid' : 'Invalid';
        if (IsInputValid) {
            delete (this.validInputState[key]);
            this.sucessStudentMarkList.data[arrOffset[0]][arrOffset[2]]['value'] = inputVal;
            console.log("After Changing--");
            console.log(this.sucessStudentMarkList.data);
        }
        if (this.state.errMsg != '') {
            this.setState({ errMsg: '', popup: 0 });
        }
    }

    UpdateInputValidState(key) {
        if (this.state.sucessTabActive)
            this.validInputStateSuccessList[key] = 'Invalid';
        else
            this.validInputStateFailureList[key] = 'Invalid';
    }
    componentWillReceiveProps(nextProps, nextContext) {
        // if(Object.keys(nextProps.sucessStudentMarkList).length > 0 && this.state.loading){
        //     this.setState({loading:false});
        // }else if(Object.keys(nextProps.sucessStudentMarkList).length == 0 && !this.state.loading){
        //     this.setState({loading:true});
        // }
        if(Object.keys(nextProps.sucessStudentMarkList).length > 0 && this.state.loading){
            this.setState({loading:false});
        }else if(Object.keys(nextProps.sucessStudentMarkList).length == 0 && !this.state.loading){
            this.setState({loading:true},()=>setTimeout(()=>this.setState({loading:false})),1000);
        }
    }


    OnPopUpClose() {
        this.setState({ errMsg: '', popup: 0 });
    }

    UpdateInputValidState(key) {
        this.validInputState[key] = 'Invalid';
    }

    componentDidMount() {
        this.props.getStudentData(this.props.lineItemId);
    }

    closeNotificationpopup = () => this.setState((prevState) => ({ NotificationPopup: !prevState.NotificationPopup }))


    OnApproveClick(status) {
        this.setState({loading:true})
        if (Object.keys(this.validInputState).length === 0) {
            let postJsonData = '', encodedJsonData = '';
            let apiParams = {
                "metadata": [
                    {
                        "schoolid": window.schoolId,
                        "classid": window.classSectionId,
                        "lineitemid": this.props.lineItemId,
                        "apiname": "MARKENTRY_API",
                        "module": "MARKENTRY",
                        "action": "WRITE",
                        "transactionstatus": status
                    }
                ],
                "key": "REACT_API_STG",
                "date": new Date().getTime(),
                "token": "null"
            };
            apiParams = btoa(JSON.stringify(apiParams));
            let apiMsg = 'Marks approved';
                this.sucessStudentMarkList.data  = calculateSumOfMarks(this.sucessStudentMarkList.data,this.props.totalMarkProps,this.props.gradeList,this.props.rubricProps); //return false;
                this.props.updateSucessStudentListData(this.sucessStudentMarkList);
                postJsonData = { successList: this.sucessStudentMarkList, failureList: { columns:this.sucessStudentMarkList.columns,data: [] } };
                encodedJsonData = btoa(JSON.stringify(postJsonData));
            // {"code":"200","status":"Success","msg":"Marks data captured successfully","data":""}
            // {"code":null,"status":"Error","msg":"Invalid JSON format,data object not defined","data":""}
            let url = '/SchoolNetTeacherPortal/API/serviceprovider';
            axios.post(url, {
                    mark_data: encodedJsonData,
                    data: apiParams
                }
            )
                .then(response => {
                    apiMsg = response.data.code == '200' ? status === "approved"?apiMsg:"Unlocked succuessfully" : 'Marks approve operation unsuccessful';
                    if (this.state.sucessTabActive) {
                        if (response.data.code == '200')
                            if(status === "approved"){
                                this.props.postStatusSucessListData(response.data)
                            }
                            this.props.getAssesmentSubjects(this.props.parentLineItemId);
                        this.setState({ NotificationPopup: true, notifiy: { msg: apiMsg, status: 1 },approved:status==="approved"?true:false,loading:false })

                    }
                })
                .catch(error => {
                    console.log(error);
                    this.setState({ NotificationPopup: true, notifiy: { msg: "Network Problem.Try Again Later", status: 3 },loading:false })
                });
        }
        else {
            this.setState({ NotificationPopup: true, notifiy: { msg: 'Enter valid marks to save', status: 3 },loading:false })


        }

    }
    render() {
        const { marks, sucessStudentMarkList, failureStudentMarkList, postStatusSucessList, postStatusFailureList, courseData, totalMarkProps,rubricProps,gradeList} = this.props;
        console.log(totalMarkProps)
        let enableApprovalBtn = '', saveBtn = '', btnSucessState = '', btnFailureState = '', lockiconStatus = false;
        let successListCnt = 0, failureListCnt = 0;
        this.sucessStudentMarkList = sucessStudentMarkList;
        this.failureStudentMarkList = failureStudentMarkList;
        if (this.state.sucessTabActive) {
            enableApprovalBtn = postStatusSucessList.postData && !postStatusSucessList.markApproved ? 'active' : 'disabled';
            saveBtn = postStatusSucessList.markApproved ? 'disabled' : 'active';
            btnSucessState = " active show";
            lockiconStatus = postStatusSucessList.markApproved ? true : false;
        }
        else {
            enableApprovalBtn = postStatusFailureList.postData && !postStatusFailureList.markApproved ? 'active' : 'disabled';
            saveBtn = postStatusFailureList.markApproved ? 'disabled' : 'active';
            btnFailureState = " active show";
            lockiconStatus = postStatusFailureList.markApproved ? true : false;
        }
        if (sucessStudentMarkList.data !== undefined){
            successListCnt = Object.keys(sucessStudentMarkList.data).length;
            if(successListCnt>0)
            this.sucessStudentMarkList.data = calculateSumOfMarks(this.sucessStudentMarkList.data,totalMarkProps,gradeList,rubricProps)
        
        }
        if (failureStudentMarkList.data !== undefined){
            failureListCnt = Object.keys(failureStudentMarkList.data).length;
            if(failureListCnt>0)
            this.failureStudentMarkList.data = calculateSumOfMarks(this.failureStudentMarkList.data,totalMarkProps,gradeList,rubricProps)
        
        }

        const marksEntryType = 'assessment';
        // let divNeedtoShow = this.props.approvalState==='approve_pending' || this.props.approvalState === 'approved';
        return (
            <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                {this.state.NotificationPopup ? <NotificationPopup text={this.state.notifiy.msg} closepopup={this.closeNotificationpopup} status={this.state.notifiy.status} /> : ""}
                <Loader loading={this.state.loading?"show":""}/>
                {this.props.approvalState === "approve_pending" || this.props.approvalState === "approved" || this.props.approvalState === "requested" || this.props.approvalState === "report_generated" || this.props.approvalState === "unlocked" ? <React.Fragment>
                <div className="row">
                    <div className="col-12">
                        <div className="mark__approvalsub--subjheading d-flex" aria-label="">
                            <div className="col-6 p-0">
                                <h6 className="mark__approvalsub--heading" aria-label="students">Students</h6>
                                <span className="mark__approvalsub--number" aria-label="The total number of students are 50">{(typeof sucessStudentMarkList !== 'undefined' && Object.keys(sucessStudentMarkList).length > 0) ? sucessStudentMarkList.data.length: 0}</span>
                            </div>
                            <div className="col-6 align-self-center d-flex justify-content-end">
                                <i className="material-icons mark__approvalsub--lockicon">lock</i>
                                {!this.state.approved ? <button type="button" className="btn btn-primary mark__approvalsub--approve" onClick={()=>this.OnApproveClick('approved')} aria-label="Click to approve and lock marks">Approve and Lock</button> :
                                    <button type="button" disabled={this.props.reportGeneratedRequest?true:false} className="btn btn-primary mark__approvalsub--approve" aria-label="Click to uplock and edit marks" onClick={()=>this.OnApproveClick('unlocked')}>Unlock and Edit</button>}
                                <span className="mark__approvalsub--assessmentcontainer"><img class="mark__approvalsub--assessmenticon" src="images/mark__assessments.svg" /></span>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="markentryNavigation__student d-flex justify-content-between">
                    <div align="center" className="unitestHeading__subject" style={{ color: '#f00' }}>{this.state.errMsg}</div>
                </div>
                <div className="markentry__tabtext__details">
                    <div id="bulk__upload" role="tabpanel" aria-labelledby="bulk__upload">
                        <MarksTable key={Math.random()*10000-0} studentList={sucessStudentMarkList} inputBoxStatus={(this.state.approved?true:false)} UpdateInputValidState={this.UpdateInputValidState} OnChangeData={this.OnChangeData} marksEntryType={marksEntryType} marks={marks} {...this.props} />
                    </div>
                </div>
                </React.Fragment>:<div className='markentry__submitnotapproval' key={Math.round()*10000}>Subject Teacher is not submitted for Approval</div>}

            </div>
        )
    }
}

const mapState = (state) => {
    return {
        approvalState :typeof state.studentReducer.studentList.status !== 'undefined' ? state.studentReducer.studentList.status:'',
        sucessStudentMarkList: typeof state.studentReducer.studentList.successList !== 'undefined' ? state.studentReducer.studentList.successList: {},
        failureStudentMarkList: typeof state.studentReducer.studentList.failureList !== 'undefined' ? state.studentReducer.studentList.failureList : {},
        postStatusSucessList: typeof state.studentReducer.postStatusSucessList !== 'undefined' ? state.studentReducer.postStatusSucessList : {},
        postStatusFailureList: typeof state.studentReducer.postStatusFailureList !== 'undefined' ? state.studentReducer.postStatusFailureList : {},
        APIResponseStatus:state.studentReducer.APIResponseStatus,
        totalMarkProps:  typeof state.studentReducer.studentList.totalMarkProperties !== 'undefined'? state.studentReducer.studentList.totalMarkProperties :{},
        rubricProps: typeof state.studentReducer.studentList.rubricProperties !== 'undefined'? state.studentReducer.studentList.rubricProperties :{},
        gradeList : typeof state.studentReducer.studentList.grades !== 'undefined' ? state.studentReducer.studentList.grades :{},
    }
}

export default connect(mapState, { getStudentData, updateStudentData, postStudentData, updateMarkApprovalStatus, getAssesmentSubjects,updateSucessStudentListData,postStatusSucessListData })(ErrorHandle(AssessmentMarks));
