import React, { Component } from 'react';
import './index.css';
import AssessmentMarks from "../../features/AssessmentMarks";
import ErrorHandle from '../ErrorHandle';
import NotificationPopup from '../../features/NotificationPopup/NotificationPopup';

class AssesmentSubjects extends Component {
    constructor(props) {
        super(props);
        this.onTabClick = this.onTabClick.bind(this);
        this.state = { currentAssesment: 'defaultAssessment', displayAssementMark: false, displayDownArrow: false,lineItemId:'',selectedCnt:0,approved:0,report_generated_request:false,NotificationPopup:true  };
    }

    onTabClick(subjectName, e,index,lineItemId,approved,request) {
        e.preventDefault();
        this.setState({ currentAssesment: subjectName, displayAssementMark: true, displayDownArrow: true,selectedCnt:index,lineItemId:lineItemId,approved:approved,report_generated_request:request });
    }
    closepopup = ()=>this.setState((prevState)=>({NotificationPopup:!prevState.NotificationPopup}));
    
    render() {
        const { assesmentSubjects, lineItemId } = this.props;
        let reportGenStatus = assesmentSubjects[0].subjects[0].report_status;
        let showNotification = ((reportGenStatus === 'report_generated'||reportGenStatus === 'request_failed') && this.state.NotificationPopup)?true:false;
        let toastmsg,status ='';
        let cond = reportGenStatus === 'report_generated'?(toastmsg  =  'Reports has been generated for this assessment',status = 1):(toastmsg='Reports generation failed for this assessment',status=3)
        let arrowclass = this.state.displayDownArrow ? 'active' : '';
        return (
            <div className="col-12" key={Math.round()*10000}>
                            {showNotification?<NotificationPopup text={toastmsg} closepopup ={this.closepopup} status ={status} />:""}

                <nav>
                    <ul className="nav nav-tabs marksapproval__cardtabs" id="nav-tabs" role="tablist">
                        {typeof assesmentSubjects !== 'undefined' && assesmentSubjects[0].subjects.map((values, i) => {
                            return (
                                <li className="nav-link marksapproval__cardnavelink col-2" key={`${values.lineItemId}_${i}`} style={{ cursor: "pointer" }} onClick={(e) => this.onTabClick(values.subjectName, e,i,values.lineItemId,values.approved,values.report_status==="requested"?true:false)}>
                                    <a className={`nav-item marksapproval__cardlink ${this.state.selectedCnt==i?arrowclass:''}`} href="#" role="tab" aria-controls="nav__mathematics" aria-selected="true" aria-label="" >
                                        <div className="marksapproval__card">
                                            <img className="marksapproval__card--img" src='images/default.png' />
                                            {values.approved ?
                                                <div className="marksapproval__cardtick">
                                                    <div className="marksapproved__cardtickradius">
                                                        <i className="material-icons marksapproved__cardtickicon">done</i>
                                                    </div>
                                                </div>
                                            : ''}
                                            <div className="marksapproval__cardblock">
                                                {values.approved?
                                                    <figure className="marksapproval__cardtext">
                                                        <h6 className="marksapproval__cardtext--details">Approved on {values.approvedOn}</h6>
                                                    </figure>
                                                : ''}
                                                <h4 className="marksapproval__card--title mt-3">{values.subjectName}</h4>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                </nav>
                {this.state.displayAssementMark ? <AssessmentMarks subject={this.state.currentAssesment} approved={this.state.approved} parentLineItemId={this.props.lineItemId} lineItemId={this.state.lineItemId} key={this.state.lineItemId} reportGeneratedRequest={this.state.report_generated_request}/> : ''}
            </div>
        );
    }
}
export default ErrorHandle(AssesmentSubjects);