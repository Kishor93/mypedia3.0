import React, { Component } from "react";
import ErrorHandle from "../ErrorHandle";
import _ from "lodash";
import { connect } from "react-redux";
import { getGradeScaleData } from "../../../state/gradeScaleData/gradeScaleAction";
import GridComponent from "../GridComponent/GridComponent";
import { toAddStylesForColumns, GridValidations } from "../../../util/util";
import "./index.css";
import GradeColorScale from "../GradeColorScale";

class GradeScaleTable extends Component {
  constructor(props) {
    super(props);
    this.columnStyles = [];
    this.gradeRowData = [];
    this.classNameList = {
      table_className:'table gradescale__table table-responsive',
      table_id: 'grade3level__table'
    }
    this.state={
      showAssign: false
    }
  }

  componentWillMount() {
    this.props.getGradeScaleData();
  }

  componentWillReceiveProps(nextProps) {
    if (typeof nextProps.gradeList !== "undefined" && Object.keys(nextProps.gradeList).length > 0) {
      this.createHeader(nextProps.gradeList);
    }
  }

  createHeader = props1 => {
    const { disabled } = this.props;
    let nestedElementFunc = (data, gradeData) => {
      let columnEle = [],
        gradeDatas = {};
      data.forEach((ele, i) => {
        columnEle.push({
          Element: data => [{ tag: "h6", elementProps: {}, text: data.Header }]
        });
      });
      gradeDatas = { columns: data, data: [] };
      toAddStylesForColumns(columnEle, gradeDatas);
      return gradeDatas;
    };
    let nestedCellFunc = (inputData, gradeData, index) => {
      let CellEle = [],
        datas = [],
        gradeDatas = {};
      inputData.forEach((ele, i) => {
        if (
          typeof ele.Validation !== "undefined" &&
          Object.keys(ele.Validation).length > 0
        ) {
          CellEle.push({
            Cell: data => [
              {
                tag: "input",
                elementProps: {
                  type: "text",
                  disabled:
                    typeof this.props.inputBoxStatus !== "undefined" &&
                    this.props.inputBoxStatus
                      ? 1
                      : 0,
                  class:
                    `form-control form-label ${this.validateInput_UpdateStyle(
                      data,
                      ele["accessor"]
                    )}` + `${[data.valid]}`,
                  key: `${[data.index]}_${[data.id]}_${[
                    ele.accessor
                  ]}_${(Math.random() * 1000) | 0}_${(Math.random() * 10000) |
                    0}`,
                  id: `${[data.index]}_${[data.id]}_${
                    ele["accessor"]
                  }_Activites${inputData[0].Header}`,
                  defaultValue: data.value,
                  onBlur: e => {
                    this.handleonBlur(e, data);
                  }
                }
              },
              {
                tag: "div",
                elementProps: { class: "bg__warninginput--hover" },
                elements: [
                  {
                    tag: "p",
                    elementProps: {},
                    text: `Range:${data.condition.min}-${
                      data.condition.max
                    },Text:${data.condition.enumerate}`
                  }
                ]
              }
            ]
          });
        } else if (
          typeof ele.Activites !== "undefined" &&
          ele.Activites.length > 0
        ) {
          CellEle.push({
            nestedElement: nestedElementFunc(ele.Activites, props1),
            nestedCell: nestedCellFunc(ele.Activites, props1)
          });
        } else {
          CellEle.push({
            Cell: data => [
              {
                tag: "h6",
                elementProps: { class: "grade__content grade__contentdetails" },
                elements: [{ tag: "span", elementProps: {}, text: data.value }]
              }
            ]
          });
        }
      });
      gradeData.data.map(el => {
        datas.push(el[`Activites${inputData[0].Header}`]);
      });
      gradeDatas = {
        columns: inputData.map(el => ({
          Header: el["Header"],
          accessor: el["accessor"],
          Validation: el["Validation"]
        })),
        data: datas
      };
      toAddStylesForColumns(CellEle, gradeDatas);
      return gradeDatas;
    };
    props1.columns.forEach((ele, i) => {
      if (props1.data.length > 0 && ele.accessor == "passScore") {
        this.columnStyles.push({
          Element: data => [{ tag: "h6", elementProps: {}, text: data.Header }],
          Cell: data => [
            {
              tag: "input",
              elementProps: {
                class: "gradescale__tablescore__radio",
                type: "radio",
                id: "radio_4",
                defaultChecked: data.value,
                disabled,
                name: ele.accessor
              }
            },
            {
              tag: "label",
              elementProps: {
                class: "gradescale__tablescore",
                htmlFor: "radio_4"
              },
              elements: [
                {
                  tag: "span",
                  elementProps: { class: "gradescale__tablescore__radiobtn" }
                }
              ]
            }
          ]
        });
      } else if (
        typeof ele.Validation !== "undefined" &&
        Object.keys(ele.Validation).length > 0
      ) {
        this.columnStyles.push({
          Element: data => [{ tag: "h6", elementProps: {}, text: data.Header }],
          Cell: data => [
            {
              tag: "input",
              elementProps: {
                type: "text",
                disabled:
                  typeof this.props.inputBoxStatus !== "undefined" &&
                  this.props.inputBoxStatus
                    ? 1
                    : 0,
                class:
                  `form-control form-label ${this.validateInput_UpdateStyle(
                    data,
                    ele["accessor"]
                  )}` + `${[data.valid]}`,
                key: `${[data.index]}_${[data.id]}_${[
                  ele.accessor
                ]}_${(Math.random() * 1000) | 0}_${(Math.random() * 10000) |
                  0}`,
                id: `${[data.index]}_${[data.id]}_${ele["accessor"]}`,
                defaultValue: data.value,
                onBlur: e => {
                  this.handleonBlur(e, data);
                }
              }
            },
            {
              tag: "div",
              elementProps: { class: "bg__warninginput--hover" },
              elements: [
                {
                  tag: "p",
                  elementProps: {},
                  text: `Range:${data.condition.min}-${
                    data.condition.max
                  },Text:${data.condition.enumerate}`
                }
              ]
            }
          ]
        });
      } else if (
        typeof ele.Activites !== "undefined" &&
        ele.Activites.length > 0
      ) {
        this.columnStyles.push({
          nestedElement: nestedElementFunc(ele.Activites, props1),
          nestedCell: nestedCellFunc(ele.Activites, props1, i)
        });
      } else if (props1.data.length > 0 && ele.accessor === "remarks") {
        this.columnStyles.push({
          Element: data => [{ tag: "h6", elementProps: {}, text: data.Header }],
          Cell: data => [
            {
              tag: "input",
              elementProps: {
                value: data.value,
                class: "gradescale__tableremark",
                type: "text",
                placeholder: data.value,
                disabled
              }
            }
          ]
        });
      } else if (
        props1.data.length > 0 &&
        (ele.accessor == "minScore" || ele.accessor == "maxScore")
      ) {
        this.columnStyles.push({
          Element: data => [{ tag: "h6", elementProps: {}, text: data.Header }],
          Cell: data => [
            {
              tag: "input",
              elementProps: {
                value: data.value,
                class: "gradescale__tablescore__minmax",
                type: "text",
                name: "pin",
                maxlenth: "4",
                size: "4",
                placeholder: "0"
              }
            }
          ]
        });
      } else if (
        props1.data.length > 0 &&
        ele.accessor == "categoryAndPattern"
      ) {
        this.columnStyles.push({
          Element: data => [{ tag: "h6", elementProps: {}, text: data.Header }],
          Cell: data => [
            {
              tag: "div",
              elementProps: {
                value: data.value,
                class: "btn-group gradescale__select",
                id: "select__category"
              },
              elements: [
                {
                  tag: "div",
                  elementProps: {
                    class:
                      "gradescale__dropdown-toggle gradescale__select--toggle",
                    "aria-haspopup": "true",
                    "aria-expanded": "false",
                    "data-toggle": "dropdown"
                  },
                  elements: [
                    {
                      tag: "span",
                      elementProps: {
                        id: "gradescale__selected"
                      },
                      text: data.value,
                      elements: [
                        {
                          tag: "span",
                          text: data.value
                        },
                        {
                          tag: "span",
                          elementProps: {
                            class: "option__placeholder"
                          },
                          elements: [
                            {
                              tag: "span",
                              elementProps: {
                                class: data.className
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      tag: "span",
                      elementProps: {
                        class: "caret gradescale__caret"
                      },
                      elements: [
                        {
                          tag: "i",
                          text: "expand_more",
                          elementProps: {
                            class: "material-icons arrow-change gradescale__arw"
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        });
      } else if (props1.data.length > 0 && ele.accessor == "gradeColor") {
        this.columnStyles.push({
          Element: data => [{ tag: "h6", elementProps: {}, text: data.Header }],
          Cell: data => [
            {
              tag: "div",
              elementProps: {
                class: "gradescale__tablecolorlist"
              },
              elements: [
                {
                  tag: "div",
                  elementProps: {
                    class: `gradescale__tablecolor ${data.value}`
                  }
                }
              ]
            }
          ]
        });
      } else {
        this.columnStyles.push({
          Element: data => [{ tag: "h6", elementProps: {}, text: data.Header }],
          Cell: data => [
            {
              tag: "div",
              elementProps: {  },
              text: data.value
            }
          ]
        });
      }
    });
  };

  render() {
    const { gradeList } = this.props;
    if (
      typeof gradeList !== "undefined" &&
      Object.keys(gradeList).length > 0
    ) {
      toAddStylesForColumns(this.columnStyles, gradeList);
      console.log(gradeList);
    }
    return (
  <div className="ptgrd__template" id="ptgrade__level">
    <div className="row">
        <div className="col-12">
            <div className="grade__headingsec d-flex" aria-label="The current active template is s “T1” ">
                <div className="col-lg-9 col-md-6 col-sm-7 col-5 p-0">
                <h6 className="grade__heading">{this.props.title}</h6>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-5 col-7 p-0 align-self-center d-flex justify-content-end">
                <button type="button" className="btn btn-primary btn__primary--detail" onClick={this.props.showAssign}>View details / Assign <i className="material-icons detail__icon">keyboard_arrow_right</i></button>
                <i className="material-icons grd__icon">more_vert</i>
                </div>
            </div>
        </div>
    </div>

    <div id="grd_preview"> 
    <div className="row preview__row">
        <div className="col-lg-2 col-md-2 col-sm-12 col-12 align-self-center"> 
            <div className="grade__previewsec">
                <h6 className="grade__previewheading">Preview</h6>
            </div>
        </div>
        <div className="col-lg-10 col-md-10 col-sm-12 col-12 align-self-center"> 
            <div className="grade__scale" aria-label="The grade scales for T1 template are: G3; from 0 to 35, G2; from 36 to 70, G1; from 71 to 100">
                <GradeColorScale title={this.props.title}/>
            </div>
        </div>
    </div>
</div>

      <div className="row">
        <div className="col-12">
          <div className="gradescale gradetable-container">
            <div className="gradetable-container__body">
              <GridComponent studentList={this.props.gradeList} classNameList={this.classNameList}/>
            </div>
          </div>
        </div>
      </div>
    </div>

    );
  }
}
const mapDispatchToProps = { getGradeScaleData };
const mapStateToProps = state => {
  return {
    gradeList:
      state.gradeScaleReducer && typeof state.gradeScaleReducer.gradeList !== "undefined"
        ? state.gradeScaleReducer.gradeList.gradeList
        : {},
    APIResponseStatus:
    state.gradeScaleReducer && typeof state.gradeScaleReducer.APIResponseStatus !== "undefined"
        ? state.gradeScaleReducer.APIResponseStatus
        : {}
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ErrorHandle(GradeScaleTable));
