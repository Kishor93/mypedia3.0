import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux'
import { getDataForBulkUpload, onEditDisableMarkApprovalBtn, postStudentData, updateStudentData, updateMarkApprovalStatus } from '../../../state/studentData/studentAction'
import MarksTable from "../MarksTable";
import {calculateSumOfMarks} from '../../../util/util.js';
import './index.css';
import NotificationPopup from '../NotificationPopup/NotificationPopup.js';
import ErrorHandle from '../ErrorHandle';
import Loader from '../../features/Loader/Loader';
class MarksBulkUpload extends Component {

    constructor(props) {
        super(props)
        this.OnSaveClick = this.OnSaveClick.bind(this);
        this.OnChangeData = this.OnChangeData.bind(this);
        this.UpdateInputValidState = this.UpdateInputValidState.bind(this);
        this.OnSubmitApprovalClick = this.OnSubmitApprovalClick.bind(this);     
        this.sucessStudentMarkList = [];
        this.failureStudentMarkList = [];
        this.validInputStateSuccessList = [];
        this.validInputStateFailureList = [];
        this.state = { sucessTabActive: 1, failureTabClicked: false, NotificationPopup: false, notifiy: { msg: '', status: 1 }, loading: false };
    }

    handleonClick(e, data) {
        e.preventDefault();
        if (data == 'success')
            this.setState({ sucessTabActive: 1 });
        else
            this.setState({ sucessTabActive: 0, failureTabClicked: true });
    }

    OnChangeData(IsInputValid, arrOffset, inputVal) {
        let key = arrOffset[0] + "_" + arrOffset[1] + "_" + arrOffset[2];
        if (this.state.sucessTabActive) {
            this.validInputStateSuccessList[key] = IsInputValid ? 'valid' : 'Invalid';
            if (typeof this.sucessStudentMarkList.data[arrOffset[0]][arrOffset[2]] !== 'undefined' && this.sucessStudentMarkList.data[arrOffset[0]][arrOffset[2]]['value'] != inputVal) {
                this.sucessStudentMarkList.data[arrOffset[0]][arrOffset[2]]['value'] = inputVal;
                this.props.onEditDisableMarkApprovalBtn();
            }else if(typeof arrOffset[3] !== 'undefined' && this.sucessStudentMarkList.data[arrOffset[0]][arrOffset[3]][arrOffset[2]]['value'] != inputVal){
                this.sucessStudentMarkList.data[arrOffset[0]][arrOffset[3]][arrOffset[2]]['value'] = inputVal;
                this.props.onEditDisableMarkApprovalBtn();
            }
            if (IsInputValid) {
                delete (this.validInputStateSuccessList[key]);

            }
        }
        else {
            this.validInputStateFailureList[key] = IsInputValid ? 'valid' : 'Invalid';
            if (typeof this.failureStudentMarkList.data[arrOffset[0]][arrOffset[2]] !== 'undefined' && this.failureStudentMarkList.data[arrOffset[0]][arrOffset[2]]['value'] != inputVal) {
                this.failureStudentMarkList.data[arrOffset[0]][arrOffset[2]]['value'] = inputVal;
                this.props.onEditDisableMarkApprovalBtn();
            }else if(typeof arrOffset[3] !== 'undefined' && this.failureStudentMarkList.data[arrOffset[0]][arrOffset[3]][arrOffset[2]]['value'] != inputVal){
                this.failureStudentMarkList.data[arrOffset[0]][arrOffset[3]][arrOffset[2]]['value'] = inputVal;
                this.props.onEditDisableMarkApprovalBtn();
            }
            if (IsInputValid) {
                delete (this.validInputStateFailureList[key]);
            }

        }

    }

    UpdateInputValidState(key) {
        if (this.state.sucessTabActive)
            this.validInputStateSuccessList[key] = 'Invalid';
        else
            this.validInputStateFailureList[key] = 'Invalid';
    }

    componentDidMount() {
       if(this.props.totalMarkProps.constructor === Object && Object.keys(this.props.totalMarkProps).length > 0)
       this.props.getDataForBulkUpload(this.props.lineItemID);
    }

    componentDidUpdate() {
        let statusArr = new Array({
            "approve_pending": { 'msg': 'Marks Already Submitted for Approval', 'popstatus': 1 },
            "requested": { 'msg': 'Already Request raised for Report generation', 'popstatus': 1 },
            "report_generated": { 'msg': 'Already Report generated', 'popstatus': 1 },
            "request_failed": { 'msg': 'Request raised for Report generation failed!', 'popstatus': 3 },
            "approved": { 'msg': 'Marks Already Approved', 'popstatus': 1 },
        });
        if (statusArr[0][this.props.apiStatus] && !this.props.postStatus.markApproved) {
            this.props.updateMarkApprovalStatus(true);
            this.setState({ NotificationPopup: true, notifiy: { msg: statusArr[0][this.props.apiStatus]['msg'], status: statusArr[0][this.props.apiStatus]['popstatus'] } })
        }
    }

    closeNotificationpopup = () => this.setState((prevState) => ({ NotificationPopup: !prevState.NotificationPopup }))

    OnSubmitApprovalClick() {
        this.setState({ loading: true })
        let postJsonData = '', encodedJsonData = '', lockEnabled = true;
        let apiParams = {
            "metadata": [
                {
                    "schoolid": window.schoolId,
                    "classid": window.classSectionId,
                    "lineitemid": this.props.lineItemID,
                    "apiname": "MARKENTRY_API",
                    "module": "MARKENTRY",
                    "action": "WRITE",
                    "transactionstatus": "approve_pending"
                }
            ],
            "key": "REACT_API_STG",
            "date": new Date().getTime(),
            "token": "null"
        };
        apiParams = btoa(JSON.stringify(apiParams));
        let apiMsg = 'Marks submitted for Approval Successfully';
        this.props.updateStudentData(this.sucessStudentMarkList, this.failureStudentMarkList);
        postJsonData = { successList: this.sucessStudentMarkList, failureList: this.failureStudentMarkList };
        encodedJsonData = btoa(JSON.stringify(postJsonData));
        // {"code":"200","status":"Success","msg":"Marks data captured successfully","data":""}
        // {"code":null,"status":"Error","msg":"Invalid JSON format,data object not defined","data":""}
        let url = '/SchoolNetTeacherPortal/API/serviceprovider';
        axios.post(url, {
            mark_data: encodedJsonData,
            data: apiParams
        }
        ).then(response => {
            if (response.data.code != '200') {
                lockEnabled = false;
                apiMsg = 'Marks submitted for Approval Unsucessful';
            }
            else if (response.data.code == '200')
                this.props.updateMarkApprovalStatus(true);

            this.setState({ NotificationPopup: true, notifiy: { msg: apiMsg, status: 1 }, loading: false })
        }).catch(error => {
            console.log(error);
            this.setState({ NotificationPopup: true, notifiy: { msg: "Network Problem.Try Again Later", status: 3 }, loading: false })
        });

    }

    OnSaveClick() {
        this.setState({ loading: true })
        const { postStatus, sucessStudentMarkList, failureStudentMarkList,totalMarkProps,gradeList,rubricProps } = this.props;
        let successValidateCnt = Object.keys(this.validInputStateSuccessList).length;
        let failureValidateCnt = Object.keys(this.validInputStateFailureList).length;
        if (!this.state.failureTabClicked && Object.keys(failureStudentMarkList.data).length > 0)
            this.setState({ NotificationPopup: true, notifiy: { msg: 'Please Check Marks in Failure List', status: 3 }, loading: false })
        else if (postStatus.postData)
            this.setState({ NotificationPopup: true, notifiy: { msg: 'Marks Already saved', status: 1 }, loading: false });
        else if (!successValidateCnt && !failureValidateCnt) {
            let postJsonData = '', encodedJsonData = '';
            let apiParams = {
                "metadata": [
                    {
                        "schoolid": window.schoolId,
                        "classid": window.classSectionId,
                        "lineitemid": this.props.lineItemID,
                        "apiname": "MARKENTRY_API",
                        "module": "MARKENTRY",
                        "action": "WRITE",
                        "transactionstatus": "saved"
                    }
                ],
                "key": "REACT_API_STG",
                "date": new Date().getTime(),
                "token": "null"
            };
            apiParams = btoa(JSON.stringify(apiParams));
            let apiMsg = 'Marks save operation unsuccessful';
            const { data } = this.failureStudentMarkList;
            if (typeof data !== 'undefined' && data.length > 0) {
                //let i =  Object.keys(this.sucessStudentMarkList.data).length;

                let tmpArray1 = this.sucessStudentMarkList.data;
                let tmpArray2 = tmpArray1.concat(data);
                tmpArray2.sort(function (a, b) {
                    return (a['rollNo']['value'] < b['rollNo']['value'] ? -1 : 1);

                });
                let dataArr = [];
                let i = 0;
                for (let key in tmpArray2) {
                    if (tmpArray2.hasOwnProperty(key)) {
                        dataArr[i] = tmpArray2[key];
                        for (let key1 in dataArr[i]) {
                            if (dataArr[i].hasOwnProperty(key1) && typeof dataArr[i][key1]['index'] !== 'undefined') {
                                dataArr[i][key1]['index'] = i;
                            }
                        }
                        i++;
                    }
                }
                this.sucessStudentMarkList.data = dataArr;
                this.failureStudentMarkList.data = [];
            }
            this.sucessStudentMarkList.data = calculateSumOfMarks(this.sucessStudentMarkList.data,totalMarkProps,gradeList,rubricProps)
            this.props.updateStudentData(this.sucessStudentMarkList, this.failureStudentMarkList);
            postJsonData = { successList: this.sucessStudentMarkList, failureList: this.failureStudentMarkList };
            encodedJsonData = btoa(JSON.stringify(postJsonData));
            let url = '/SchoolNetTeacherPortal/API/serviceprovider';
            axios.post(url, {
                mark_data: encodedJsonData,
                data: apiParams
            }
            ).then(response => {
                if (response.data.code == '200') {
                    this.props.postStudentData();
                    apiMsg = 'Marks saved Successfully';
                }
                this.setState({ NotificationPopup: true, notifiy: { msg: apiMsg, status: 1 }, loading: false })
            })
                .catch(error => {
                    console.log(error);
                    this.setState({ NotificationPopup: true, notifiy: { msg: "Network Problem.Try Again Later", status: 3 }, loading: false })
                });
        }
        else if (successValidateCnt && failureValidateCnt) {
            this.setState({ NotificationPopup: true, notifiy: { msg: 'Invalid marks found in Successfull and Failure List', status: 3 }, loading: false })
        }
        else if (successValidateCnt && !failureValidateCnt) {
            this.setState({ NotificationPopup: true, notifiy: { msg: 'Invalid marks found in Successfull List', status: 3 }, loading: false })
        }
        else if (!successValidateCnt && failureValidateCnt) {
            this.setState({ NotificationPopup: true, notifiy: { msg: 'Invalid marks found in Failure List', status: 3 }, loading: false })
        }
        else {
            this.setState({ NotificationPopup: true, notifiy: { msg: 'Enter valid marks to save', status: 3 }, loading: false })
        }

    }


    render() {
        const { marks, sucessStudentMarkList, failureStudentMarkList, postStatus, totalMarkProps,gradeList,rubricProps } = this.props;
        let enableApprovalBtn = '', saveBtn = '', btnSucessState = '', btnFailureState = '', lockiconStatus = false;
        let successListCnt = 0, failureListCnt = 0;
        this.sucessStudentMarkList = sucessStudentMarkList;
        this.failureStudentMarkList = failureStudentMarkList;
        enableApprovalBtn = postStatus.postData && !postStatus.markApproved ? 'active' : 'disabled';
        saveBtn = postStatus.markApproved ? 'disabled' : 'active';
        lockiconStatus = postStatus.markApproved ? true : false;
        if (this.state.sucessTabActive) {
            btnSucessState = " active show";
        }
        else {
            btnFailureState = " active show";
        }
        console.time("performance test");
        if (sucessStudentMarkList.data !== undefined){
            successListCnt = Object.keys(sucessStudentMarkList.data).length;
            if(successListCnt>0)
            this.sucessStudentMarkList.data = calculateSumOfMarks(this.sucessStudentMarkList.data,totalMarkProps,gradeList,rubricProps)
        }
        if (failureStudentMarkList.data !== undefined){
            failureListCnt = Object.keys(failureStudentMarkList.data).length;
            if(failureListCnt>0)
            this.failureStudentMarkList.data = calculateSumOfMarks(this.failureStudentMarkList.data,totalMarkProps,gradeList,rubricProps)
        }
        console.timeEnd("performance test");
        const marksEntryType = 'assessment';//will come from props i.e store
        let show = this.state.loading || typeof sucessStudentMarkList.data === undefined ? "show" : "";
        return (
            <div>
                <Loader loading={show} />
                {this.state.NotificationPopup ? <NotificationPopup text={this.state.notifiy.msg} closepopup={this.closeNotificationpopup} status={this.state.notifiy.status} /> : ""}

                <div className="markentryNavigation__student d-flex justify-content-between">
                    <div className="d-flex">
                        <h6 className="unitestHeading studentCountColor mr-3" tabIndex={0} aria-label="Marks have been entered for N out M students">{successListCnt + failureListCnt} <small>Students</small></h6>
                        <div className="uploadnav__button">
                            <ul className="auploadnav__buttontab nav">
                                <li className="uploadnav__buttontablist">
                                    <a onClick={(e) => { this.handleonClick(e, 'success') }} className={"uploadnav__buttontabitem uploadnav__buttontabitem--success nav-item " + btnSucessState} data-toggle="tab" href="#successful" role="tab" aria-controls="successful" aria-selected="true" tabIndex>Successful<span className="countBadge countBadge--color">{successListCnt}</span></a>
                                </li>
                                <li className="uploadnav__buttontablist">
                                    <a onClick={(e) => { this.handleonClick(e, 'failure') }} className={"uploadnav__buttontabitem uploadnav__buttontabitem--failure nav-item" + btnFailureState} data-toggle="tab" href="#failure" role="tab" aria-controls="failure" aria-selected="false" tabIndex>Failure<span className="countBadge countBadge--color">{failureListCnt}</span></a>
                                </li>
                            </ul>
                        </div>


                    </div>
                    <div>
                        {(failureListCnt || (successListCnt && this.state.sucessTabActive)) ?
                            <div className="markentryNavigation__rightbutton">
                                <ul className="markentryNavigation__buttontab d-flex justify-content-end">
                                    <li className="markentryNavigation__buttontablist" tabIndex={0} aria-label="Unlock to enter marks / Lock to save marks"><i className={lockiconStatus ? 'material-icons markentry__submitbutton-lockicon updated' : 'material-icons markentry__submitbutton-lockicon'}>lock</i></li>
                                    <li className="markentryNavigation__buttontablist" tabIndex={0} aria-label="Click to submit marks"><button className={"markentry__submitbutton  " + enableApprovalBtn} onClick={this.OnSubmitApprovalClick}>Submit for Approval</button></li>
                                    <li className="markentryNavigation__buttontablist" tabIndex={0} aria-label="Click to save marks"><button className={"markentry__submitbutton " + saveBtn} onClick={this.OnSaveClick} >Save Marks</button></li>
                                    {/* <li className="markentryNavigation__buttontablist" tabIndex={0} aria-label="Click to view statistics"><span className="markentry__submit--assessmentcontainer"><img className="markentry__submit--assessmenticon" src="images/mark__assessments.svg" /></span></li> */}
                                </ul>
                            </div>
                            : ""}
                    </div>
                </div>
                <div className="markentry__tabtext__details">
                    <div id="bulk__upload" role="tabpanel" aria-labelledby="bulk__upload">
                        <MarksTable studentList={this.state.sucessTabActive ? sucessStudentMarkList : failureStudentMarkList} inputBoxStatus={postStatus.markApproved} UpdateInputValidState={this.UpdateInputValidState} OnChangeData={this.OnChangeData} marksEntryType={marksEntryType} marks={marks} {...this.props} />
                    </div>
                </div>
            </div>
        )
    }
}


const mapState = (state) => {
    return {
        totalMarkProps:  typeof state.studentReducer.totalMarkProperties !== 'undefined'? state.studentReducer.totalMarkProperties :{},
        rubricProps: typeof state.studentReducer.rubricProps !== 'undefined'? state.studentReducer.rubricProps :{},
        gradeList : typeof state.studentReducer.gradeList !== 'undefined' ? state.studentReducer.gradeList :{},
        sucessStudentMarkList: typeof state.studentReducer.sucessStudentMarkList !== 'undefined' ? state.studentReducer.sucessStudentMarkList : {},
        failureStudentMarkList: typeof state.studentReducer.failureStudentMarkList !== 'undefined' ? state.studentReducer.failureStudentMarkList : {},
        postStatus: typeof state.studentReducer.postStatus !== 'undefined' ? state.studentReducer.postStatus : {},
        lineItemID: state.courseReducer.summary.length > 0 ? state.courseReducer.summary[0].lineItemId : 0,
        apiStatus: typeof state.studentReducer.APIStatus !== 'undefined' ? state.studentReducer.APIStatus : 'NA',
        APIResponseStatus: state.courseReducer.APIResponseStatus && state.studentReducer.APIResponseStatus ? true : false
    }
}

export default connect(mapState, { updateMarkApprovalStatus, postStudentData, updateStudentData, onEditDisableMarkApprovalBtn, getDataForBulkUpload })(ErrorHandle(MarksBulkUpload));
