import React, { Component } from 'react'
import { connect } from 'react-redux';
import _ from 'lodash';
import GridComponent, { GridValidations } from '../GridComponent/GridComponent';
import {getDataForStudentReport,updateStudentData} from '../../../state/studentData/studentAction';
import {toAddStylesForColumns} from '../../../util/util';
import '../../features/MarksTable/index.css';

var validate  =  new GridValidations();

class StudentReports extends Component {

    componentDidMount () {
        this.props.getDataForStudentReport();
    }
    componentWillUnmount() {
        this.props.updateStudentData(validate.changedData);
    }

    render () {
        const { marksEntryType, studentList } = this.props // will come from parent which in turn will be connected to store. Reusable by marks approval page
        if (typeof studentList !== 'undefined' && Object.keys(studentList).length > 0) {toAddStylesForColumns(columnStyles,studentList)}
        return (<div className="marksentry__table">
                <div className="container-fluid" id="mark__container">
                    <div className="row">
                        <div className="col-12 p-0">
                            {(typeof studentList !== 'undefined' && Object.keys(studentList).length > 0)?
                                <GridComponent {...this.props}/> : ''}
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

const mapState = (state) => {
    return {studentList: typeof state.studentReducer.studentReport !== 'undefined' ? state.studentReducer.studentReport : {}}
}

export default connect(mapState, {updateStudentData})(StudentReports)






GridValidations.prototype.onBlur =  function(e){
    let targetDom = document.getElementById(e.target.id),[id,accessor] = e.target.id.split("_"),getIndex = _.findIndex(this.changedData,{id,accessor}),value = targetDom.value,{index} = validate.getData();
    getIndex !== -1?this.changedData[getIndex].value = value:this.changedData.push({id,accessor,value,index});
    let validationRangeAndString = this.validationRangeAndString(value);
    return !validationRangeAndString && targetDom.className.indexOf('bg__warninginput') === -1?targetDom.classList.add('bg__warninginput'):validationRangeAndString && targetDom.className.indexOf('bg__warninginput') !== -1?targetDom.classList.remove('bg__warninginput'):'';
}


var columnStyles = [
    {'Element':{'tag':'input','elementProps':{'type':'checkbox','class':'form-control form-label',"onClick":(e)=>{console.log(e)}}},'Cell':(data)=>[{'tag':'input','elementProps':{'type':'checkbox','class':'form-control form-label',"id":`${[data.value]}`,"onClick":(e)=>{console.log(e)}}}]},
    {'Element':{'tag':'h6','elementProps':{}},'Cell':(data)=>[{'tag':'img','elementProps':{'class':'student__content--avatar','src':data.image}},{'tag':'h6','elementProps':{'class':'student__content student__contentdetails'},'elements':{'tag':'span','elementProps':{'class':'student__content--name'},'text':data.name}}]},
    {'Element':{'tag':'h6','elementProps':{}},'Cell':(data)=>[{'tag':'h6','elementProps':{'class':'student__content student__contentdetails'},'elements':{'tag':'span','elementProps':{'class':'student__content--name'},'text':data.value}}]},
    {'Element':{'tag':'h6','elementProps':{}},'Cell':(data)=>[{'tag':'h6','elementProps':{'class':'student__content student__contentdetails'},'elements':{'tag':'span','elementProps':{'class':'student__content--name'},'text':data.value}}]},
    {'Element':{'tag':'h6','elementProps':{}},'Cell':(data)=>[{'tag':'h6','elementProps':{'class':'student__content student__contentdetails'},'elements':{'tag':'span','elementProps':{'class':'student__content--name'},'text':data.value}}]},
    {'Element':{'tag':'h6','elementProps':{}},'Cell':(data)=>[{'tag':'h6','elementProps':{'class':'student__content student__contentdetails'},'elements':{'tag':'span','elementProps':{'class':'student__content--name'},'text':data.value}}]},
    {'Element':{'tag':'h6','elementProps':{}},'Cell':(data)=>[{'tag':'h6','elementProps':{'class':'student__content student__contentdetails'},'elements':{'tag':'span','elementProps':{'class':'student__content--name'},'text':data.value}}]}
];