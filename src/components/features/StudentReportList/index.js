import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import GridComponent, {GridValidations} from '../GridComponent/GridComponent';
import {getDataForStudentReport, updateStudentData} from '../../../state/studentData/studentAction';
import {getPrintableReport, getTeacherData, resetPrintableReport} from "../../../state/reportData/reportAction";
import {toAddStylesForColumns} from '../../../util/util.js';
import './index.css';
import NotificationPopup from '../NotificationPopup/NotificationPopup.js'
import FileViewer from "../FileViewer";
import {downloadFiles} from "../../../util/util";
import ErrorHandle from '../../features/ErrorHandle';
import Loader from '../../features/Loader/Loader';

class StudentReportList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            reportViewerIsOpen: false,
            printPreview: false,
            fileList: [],
            downloadParam: '',
            printParam: '',NotificationPopup: false, notifiy: { msg: '', status: 1 }, previewModalIsOpen: false,loading:true
        };
        this.selectedPDF = [];
        this.state.selectedCnt = 0;
        //this.state.loading = true;

        this.columnStyles = [
            { 'Element': (data)=>[{'tag':'label','elementProps':{'class':"checkbox__table"},'elements':[{ 'tag': 'input', 'elementProps': { 'type': 'checkbox', 'class': 'form-control form-label',"id":"selectedAll", "onClick": (e) => { this.validateSelectInput(e,'selectedAll') } } },{'tag':'span','elementProps':{'class':'checkmark__table'}}]}],'Cell': (data) => [{'tag':'label','elementProps':{'class':"checkbox__table"},'elements':[{ 'tag': 'input', 'elementProps': { 'type': 'checkbox', 'class': 'form-control form-label checkboxClass','disabled':data.link?false:true, "id": `${[data.value]}`, "onClick": (e) => { this.validateSelectInput(e, data) } } },{'tag':'span','elementProps':{'class':'checkmark__table'}}]}] },
            { 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': { 'class': "" },'text':data.Header}], 'Cell': (data) => [{ 'tag': 'img', 'elementProps': { 'class': 'student__content--avatar', 'src': data.image } }, { 'tag': 'h6', 'elementProps': { 'class': 'student__content' }, 'elements': [{ 'tag': 'span', 'elementProps': { 'class': 'student__content--name' }, 'text': data.name } ]}] },
            { 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': { 'class': "" },'text':data.Header }], 'Cell': (data) => [{ 'tag': 'h6', 'elementProps': { 'class': 'text-center' }, 'text': data.value }] },
            { 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': { 'class': "" },'text':data.Header }], 'Cell': (data) => [{ 'tag': 'h6', 'elementProps': { 'class': 'text-center' }, 'text': data.value }] },
            { 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': { 'class': "" },'text':data.Header }], 'Cell': (data) => [{ 'tag': 'h6', 'elementProps': { 'class': 'text-center' }, 'text': data.value }] },
            { 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': { 'class': "" },'text':data.Header }], 'Cell': (data) => [{ 'tag': 'h6', 'elementProps': { 'class': 'text-center' }, 'text': data.value }] },
            { 'Element': (data)=>[{ 'tag': 'h6', 'elementProps': { 'class': "" },'text':data.Header }], 'Cell': (data) => [{ 'tag': 'img', 'elementProps': { 'class': 'student__contentpdf--avatar-hide', 'src': "images/pdf__icon.svg" } }, { 'tag': 'h6', 'elementProps': { 'class': 'pdf_image_text' }, 'elements': [{ 'tag': 'span', 'elementProps': { 'class': 'student__content--name1', "id": `${[data.id]}` }, 'text': data.name } ]}, { "tag": "span", "elementProps": { "class": "spinner-bg" }, "elements": [{ "tag": "img", "elementProps": { "class": "spinner-active", "id": "spinner", "src": "images/spinner.gif" } }] }] }
        ];
    }

    validateSelectInput(e, data) {
        let targetDom = document.getElementById(e.target.id);
        if(data === 'selectedAll'){
            this.selectedPDF.length = 0;
            let checkboxDOM = document.getElementsByClassName('form-control form-label checkboxClass');
           for(let i=0;i<checkboxDOM.length;i++){
                if(targetDom.checked && this.props.studentList.data[i].id){
                    checkboxDOM[i].checked = true;
                    this.selectedPDF.push(this.props.studentList.data[i].id)
                }else if(!targetDom.checked || !this.props.studentList.data[i].id){
                    checkboxDOM[i].checked = false;
                }
            }
            if(!targetDom.checked){this.selectedPDF.length = 0}
            this.setState({selectedCnt:targetDom.checked?this.selectedPDF.length:0});
            return true;
        }
        let getIndex = _.findIndex(this.selectedPDF, data);
        if (targetDom.checked && getIndex === -1) {
            this.selectedPDF.push(data);
        } else if (!targetDom.checked && getIndex !== -1) {
            if(document.getElementById('selectedAll').checked){
                document.getElementById('selectedAll').checked = false;
            }
            this.selectedPDF.splice(getIndex, 1);
        }
        this.setState({ selectedCnt: this.selectedPDF.length });

        var multiplePdfReq = [];
        for (let j = 0; j < this.selectedPDF.length; j++) {
            const answer_array = this.selectedPDF[j].link.split('/');
            var last_element = answer_array[answer_array.length - 1];
            last_element = last_element.replace('.pdf', '');
            multiplePdfReq.push(last_element);
        }
        if (multiplePdfReq.length > 0) {
            multiplePdfReq.join();
            this.setState({ downloadParam: multiplePdfReq, printParam: multiplePdfReq });
        }
    }
    closeNotificationpopup = () => this.setState((prevState) => ({ NotificationPopup: !prevState.NotificationPopup }))


    openModal = (fileList, printPreview) => {
        console.log('this.selectedPDF',this.selectedPDF);
        if(fileList.length == 0){
            this.setState({ NotificationPopup: true, notifiy: { msg: 'Please Select Atleast One Student', status: 3 } })
            return false;
        }
        var singlePdfReq = [];
        for (let j = 0; j < fileList.length; j++) {
            const answer_array = fileList[j].file.split('/');
            var last_element = answer_array[answer_array.length - 1];
            last_element = last_element.replace('.pdf', '');
            singlePdfReq.push(last_element);
        }
        this.setState({reportViewerIsOpen: true, printPreview: printPreview, fileList: fileList, downloadParam: singlePdfReq, printParam: singlePdfReq });
    };

    closeModal = () => {
        this.setState({ reportViewerIsOpen: false, printPreview:false });
    };

    getFileList = (selectedStudent) => {
        return selectedStudent.map((student) => {
            return {
                fileName: student.value,
                name: student.name,
                file: student.link,
                ext: ".pdf"
            }
        });
    };

    onDownload = () => {
        if(this.selectedPDF.length === 0){
            this.setState({ NotificationPopup: true, notifiy: { msg: 'Please Select Atleast One Student?', status: 3 } })
            return false;
        }
        this.setState({ NotificationPopup: true, notifiy: { msg: 'Download Initiated! Please Wait', status: 1 } })
        downloadFiles(this.getFileList(this.selectedPDF));
    };

    componentWillReceiveProps(nextProps) {
        if (typeof nextProps.studentList !== 'undefined' && Object.keys(nextProps.studentList).length > 0) {
            this.setState({loading:false,NotificationPopup: false},() => {
                let count = 0;
                for (var i = 0; i < document.getElementsByClassName('spinner-bg').length; i++) {
                    if(nextProps.studentList.data[i]['id']['link']) {
                        count++;
                        document.getElementsByClassName('spinner-bg')[i].style.display = "none";
                        document.getElementsByClassName('student__contentpdf--avatar-hide')[i].style.display = "inline-block";
                        document.getElementsByClassName('pdf_image_text')[i].style.display = "inline-block";
                        document.getElementsByClassName('student__content--name1')[i].style.display = "list-item";
                    }
                }
                if(count !== nextProps.studentList.data.length && document.getElementById('selectedAll')){
                    document.getElementById('selectedAll').disabled = true;
                }else if(document.getElementById('selectedAll')){
                    document.getElementById('selectedAll').disabled = false;
                }
            })
        }else if((typeof nextProps.studentList === 'string' || Object.keys(nextProps.studentList).length === 0 || typeof nextProps.teacherDetails.data === 'string')) {
            this.setState({loading:false});
        }else if(Object.keys(nextProps.studentList).length == 0 && !this.state.loading){
            this.setState({loading:true});
        }
    }
    render() {
        const {
            teacherDetails
        } = this.props;
        const { reportViewerIsOpen, fileList, printPreview } = this.state;

        var teacherData=teacherDetails.data;
        console.log('teacherData', teacherData);
        if(!this.props.report_generated_status){
            return <NotificationPopup text={"Student report is not generated."} status={2} />
        }
        if (typeof this.props.studentList !== 'undefined' && Object.keys(this.props.studentList).length > 0) { toAddStylesForColumns(this.columnStyles, this.props.studentList) }
        return (
            <div className="home-latest">
                <Loader loading={this.state.loading?"show":""}/>
                {this.state.NotificationPopup ? <NotificationPopup text={this.state.notifiy.msg} closepopup={this.closeNotificationpopup} status={this.state.notifiy.status} /> : ""}
                <div className="tab-content">
                    <div className="tab-pane active" id="mark__approval__tab--1" role="tabpanel">
                        <div className="mark__approvalsub">
                            <div className="row">
                                <div className="col-12">
                                    <div className="mark__approvalsub--subjheading d-flex" aria-label="">
                                        <div className="col-4 p-0">
                                        <span className="mark__approvalsub--number">{`${window.className} ${window.sectionName}`}</span>
                                            <h6 className="mark__approvalsub--heading"
                                                aria-label="The total number of report cards generated for <assessment name> are <n>">Report
                                                Cards Generated</h6>
                                            <span className="mark__approvalsub--number">{typeof this.props.studentList !== 'undefined' && Object.keys(this.props.studentList).length > 0 ? this.props.studentList.data.length : 0}</span>

                                        </div>
                                        <div className="col-8 align-self-center d-flex justify-content-end">
                                            <h6 className="mark__approvalsub--para"><b>{this.state.selectedCnt}</b> students selected</h6>
                                            <button onClick={() => this.openModal(this.getFileList(this.selectedPDF))} className={`btn ${this.state.selectedCnt > 0 ? "marks--approval__bluebutton" : "white-disabled"}`} data-toggle="modal"
                                                    data-target="#studentreportPopup"
                                                    aria-label="View report card of the selected students">View
                                            </button>
                                            {reportViewerIsOpen && <FileViewer modalIsOpen={reportViewerIsOpen}
                                                                               closeModal={this.closeModal}
                                                                               fileList={fileList}
                                                                               printPreview={printPreview}
                                                                               defaultActive={0}/>}
                                            <button onClick={this.onDownload} className={`btn ${this.state.selectedCnt > 0 ? "marks--approval__bluebutton" : "white-disabled"}`}
                                                    aria-label="Download report card of the selected students"
                                            >Download
                                            </button>
                                            <button onClick={() => this.openModal(this.getFileList(this.selectedPDF), true)} className={`btn ${this.state.selectedCnt > 0 ? "marks--approval__bluebutton" : "white-disabled"}`}
                                                    aria-label="Print report card of the selected students"
                                            >Print
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {teacherData !=''?
                                <div className="col-12 d-inline-flex student--report__greybg student--report__border px-4">
                                    <div className="col-3">
                                        <div className=" row">
                                            <div className="col-6 mt-3 mb-2 p-0">
                                                <p><span className="student--report__detailstext ">Class Teacher: </span>
                                                </p>
                                            </div>
                                            <div className="col-6 mt-3 mb-2 p-0">
                                                <p><span
                                                    className="student--report__detailstext--2 ">{teacherData.TeacherDetails[0].teacherName ?teacherData.TeacherDetails[0].teacherName:''}</span>
                                                </p>
                                            </div>
                                            <div className="col-6 mb-3 p-0">
                                                <p><span className="student--report__detailstext " aria-label="">Approved on: </span>
                                                </p>
                                            </div>
                                            <div className="col-6 mb-3 p-0">
                                                <p><span
                                                    className="student--report__detailstext--2">{teacherData.TeacherDetails[0].approvedDate ?teacherData.TeacherDetails[0].approvedDate:''}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-3 center-text student--report__borderleft">
                                        <p className="mt-3 mb-2"><span className="student--report__detailstext "
                                                                       aria-label="">Report Generated By</span>
                                        </p>
                                        <p className="mb-3"><span
                                            className="student--report__detailstext--2 "> {teacherData.TeacherDetails[0].teacherName ?teacherData.TeacherDetails[0].teacherName:''}, {teacherData.TeacherDetails[0].reportGeneatedDate ?teacherData.TeacherDetails[0].reportGeneatedDate:''}</span>
                                        </p>
                                    </div>
                                    <div className="col-2 center-text student--report__borderleft">
                                        <p className="mt-3 mb-2"><span className="student--report__detailstext"
                                                                       aria-label="">Grade scale :</span></p>
                                        <p className="mb-3"><span
                                            className="student--report__detailstext--2">{teacherData.TeacherDetails[0].graderScale}</span>
                                        </p>
                                    </div>
                                    <div className="col-4  center-text student--report__borderleft">
                                        <p className="mt-3 mb-2"><span className="student--report__detailstext "
                                                                       aria-label=" Click to change the report template.">Report Template Applied</span>
                                        </p>
                                        <a className="student--report__detailstext--link my-3" href=""><span
                                            className="student--report__detailstext--2 ">Standard Pearson Template - With Graph</span></a>
                                    </div>
                                </div> :''}
                            <div className="col-12 p-0 student__report__table">
                                {(typeof this.props.studentList !== 'undefined' && Object.keys(this.props.studentList).length > 0)?
                                    <GridComponent {...this.props}/> : ''}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapState = (state) => {
    return {
        studentList: typeof state.studentReducer.studentReport !== 'undefined' ? state.studentReducer.studentReport : {},
        APIResponseStatus:state.studentReducer.APIResponseStatus

    }
}

export default connect(mapState, { getDataForStudentReport, updateStudentData, getPrintableReport, resetPrintableReport,getTeacherData })(ErrorHandle(StudentReportList))
