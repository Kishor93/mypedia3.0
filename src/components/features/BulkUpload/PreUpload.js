
import React, {Component} from 'react';
import axios from 'axios';
import {Validation} from '../../../util/util.js';
import DragNDrop from '../DragNDrop/index';
import { connect } from 'react-redux';
import {getDataForBulkUpload,uploadedDataResponse,bulkUploadCompleteStatus} from '../../../state/studentData/studentAction';
import NotificationPopup from '../../features/NotificationPopup/NotificationPopup';
import ErrorHandle from '../ErrorHandle';
import Loader from '../../features/Loader/Loader';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '550px',
    padding: '0px'
  }
};


class PreUpload extends Component {
    constructor(props){
        super(props);
        this.file = "images/img-bulk-upload.png";
        this.state = {initialpopup :false,uploadstatusbar:false,NotificationPopup:true,notifiy:{msg:'For the completed assessment of your subject, you can enter marks here for all students via bulk upload or individual students.',status:1},status:1}
      //  this.popupmessage = 'For the completed assessment of your subject,you can enter marks herefor all students via bulk upload or individual students.'
    }
    
closepopup = ()=>this.setState((prevState)=>({NotificationPopup:!prevState.NotificationPopup}));
  componentDidMount () {
    this.props.getDataForBulkUpload(this.props.lineItemID);
  }
  componentDidUpdate(){
    let statusArr = new Array({
      "approve_pending": { 'msg': 'Marks Already Submitted for Approval', 'popstatus': 1 },
      "requested": { 'msg': 'Already Request raised for Report generation', 'popstatus': 1 },
      "report_generated": { 'msg': 'Already Report generated', 'popstatus': 1 },
      "request_failed": { 'msg': 'Request raised for Report generation failed!', 'popstatus': 3 },
      "approved": { 'msg': 'Marks Already Approved', 'popstatus': 1 },
  });
    if(statusArr[0][this.props.studentlist.status] && !this.state.initialpopup) {
     this.setState({ NotificationPopup: true, notifiy: { msg: statusArr[0][this.props.studentlist.status]['msg'], status: statusArr[0][this.props.studentlist.status]['popstatus'] },initialpopup:true })
     
    }
    
 }


handleFile = (fileObj) => {
    var reader = new FileReader();
    this.fileName = fileObj.name;
    this.fileType = fileObj.type;
    let fileType =  this.fileName.split('.').pop();
    if(fileType !== 'xls' && fileType !== 'XLS'){
      this.setState({uploadstatusbar:false,NotificationPopup:true,notifiy:{msg:'System expects XLS file format file so please upload XLS format to validate the data',status:3}})
      return false;
    }
    
    if (fileObj.size >= 26214400) {
      this.setState({uploadstatusbar:false,NotificationPopup:true,notifiy:{msg:'File Size should be less than 25 MB',status:3}})
      return false;
    }
    reader.onloadstart = (event) => {
      console.log('File onloadstart');
    };
    reader.onprogress = (event) => {
      console.log('File onprogress');
    };
    reader.onloadend = (event) => {
        //const schooldata = this.props.studentReducer.studentList;
        const schooldata = this.props.studentlist;
        var validate = new Validation(event);
        validate.getDataArray();       
        let uploadData = validate.validateMarkentryRecordsNew(schooldata);
        this.setState({uploadstatusbar:false});
        if(typeof uploadData === 'object'){
          let apiParams = {
            "metadata": [
              {
                "schoolid":  window.schoolId,
                "classid": window.classSectionId,
                "lineitemid": this.props.lineItemID,
                "apiname": "",
                "module": "MARKENTRY",
                "action": "WRITE",
                "transactionstatus": "uploaded"
              }
            ],
            "key": "REACT_API_STG",
            "date": new Date().getTime(),
            "token": "null"
           };
           apiParams = btoa(JSON.stringify(apiParams));
           uploadData =  btoa(JSON.stringify(uploadData));
          let url = '/SchoolNetTeacherPortal/API/serviceprovider';
          let apiMsg = '';
          axios.post(url, {
            mark_data: uploadData,
            data : apiParams
        }
        )
        .then(response => {
          if(response.data.code == '200'){
            this.props.uploadedDataResponse(response.data.data);
           this.props.bulkUploadCompleteStatus();
          }else{
            this.setState({ NotificationPopup: true, notifiy: { msg: "Upload Failed.. Try again Later", status:3 } })
          }         
         // apiMsg : 'Marks save operation unsuccessful';                        
          
      })
      .catch(error => {
        //throw (error);
        this.setState({ NotificationPopup: true, notifiy: { msg: "Network Problem.Try Again Later", status:3 } })
    });
          //
        }else{
         document.getElementById('markentry__upload').value = ''; 
        //window.location.reload();
        this.setState({ NotificationPopup: true, notifiy: { msg: uploadData, status:3 } })
        }
    };
    reader.onload = (e) => {
      console.log('File onload');
    }
    //reader.readAsDataURL(fileObj.file);
    reader.readAsBinaryString(fileObj.file);
  }
  onFileChange = (event) => {
    this.setState({uploadstatusbar:true})
    var reader = new FileReader();
    if (event.target.files && event.target.files[0]) {
      let fileName = event.target.files[0].name;
      let fileType =  fileName.split('.').pop();
      if(fileType !== 'xls' && fileType !== 'XLS'){
        this.setState({uploadstatusbar:false,NotificationPopup:true,notifiy:{msg:'System expects XLS file format file so please upload XLS format to validate the data',status:3}})
        //alert('xls only allowed');
        return false;
      }
      if (event.target.files[0].size >= 26214400) {
        this.setState({uploadstatusbar:false})
        //alert('File Size should be less than 25 MB');
        this.setState({uploadstatusbar:false,NotificationPopup:true,notifiy:{msg:'File Size should be less than 25 MB',status:3}})
        return false;
      }
      reader.onload = (e) => {
            console.log('File On Load');
      };
      reader.onloadstart = (event) => {
        console.log('File On Start');
      };
      reader.onprogress = (event) => {
        console.log('File InProgress');
      };
      reader.onloadend = (event) => {
        const schooldata = this.props.studentlist;
       // console.log(schooldata);return false;
        var validate = new Validation(event);
        validate.getDataArray();
        //let uploadData = validate.validateMarkentryRecords(schooldata);
         let uploadData = validate.validateMarkentryRecordsNew(schooldata);
        this.setState({uploadstatusbar:false})
        if(typeof uploadData === 'object'){
          let apiParams = {
            "metadata": [
              {
                "schoolid":  window.schoolId,
                "classid": window.classSectionId,
                "lineitemid": this.props.lineItemID,
                "apiname": "",
                "module": "MARKENTRY",
                "action": "WRITE",
                "transactionstatus": "uploaded"
              }
            ],
            "key": "REACT_API_STG",
            "date": new Date().getTime(),
            "token": "null"
           };
           apiParams = btoa(JSON.stringify(apiParams));
           uploadData =  btoa(JSON.stringify(uploadData));
          let url = '/SchoolNetTeacherPortal/API/serviceprovider';
          let apiMsg = '';
          axios.post(url, {
            mark_data: uploadData,
            data : apiParams
        }
        )
        .then(response => {
          if(response.data.code == '200'){
            this.props.uploadedDataResponse(response.data.data);
           this.props.bulkUploadCompleteStatus();
          }else{
            this.setState({ NotificationPopup: true, notifiy: { msg: "Upload Failed.. Try again Later", status:3 } })
          }         
         // apiMsg : 'Marks save operation unsuccessful';                        
          
      })
      .catch(error => {
        //throw (error);
        this.setState({ NotificationPopup: true, notifiy: { msg: "Network Problem.Try Again Later", status:3 } })
    });
          //
        }else{
         document.getElementById('markentry__upload').value = ''; 
        //window.location.reload();
        this.setState({ NotificationPopup: true, notifiy: { msg: uploadData, status:3 } })
        }
        
      }
      reader.readAsBinaryString(event.target.files[0]);
    }
  }
    render() {
      let displayUpload = Object.keys(this.props.studentlist).length > 0 && (this.props.studentlist.status != 'approved'&& this.props.studentlist.status != 'approve_pending' && this.props.studentlist.status != "requested" && this.props.studentlist.status != 'report_generated')
      let show = typeof this.props.studentlist.status ==='undefined'?'show':'';
        return (
          <div className="tab-content markentry__tabtext__details">
            {/* <div className={`loading__spinner ${show}`}>
              <img src ="/images/spinner.gif" className="loading__spinnerimg" alt="spinner img"/>
            </div> */}

            <Loader loading={show}/>
            {this.state.NotificationPopup?<NotificationPopup text={this.state.notifiy.msg} closepopup ={this.closepopup} status ={this.state.notifiy.status} />:""}
            
            <div className="tab-pane active show" id="bulk__upload" role="tabpanel" aria-labelledby="bulk__upload">
              <div className="markentry__bulkupload">
                <div className="markentry__bulkupload__entrytemp">
                  <div className="markentry__bulkupload__markwarp mx-auto">
                                                    <span className="markentry__bulkupload__markwarplink">
                                                        <img src="images/shape.svg"
                                                             className="markentry__bulkupload__marimg"
                                                             alt="shape image" />Marks Entry Template
                                                             
                                                    </span>
                    <ul className="markentry__bulkupload__detailslink">
                      <li className="markentry__bulkupload-list">
                        <a className="markentry__bulkupload-listlink markentry__bulkupload-leftline" href={`/SchoolNetTeacherPortal/API/serviceprovider?class_id=${window.classSectionId}&ins_id=${window.schoolId}&line_item_id=${this.props.lineItemID?this.props.lineItemID:''}&class_name=${window.className}&sec_name=${window.sectionName}&qpname=${this.props.qpName?this.props.qpName:''}&subject=${this.props.subject?this.props.subject:''}&opt=xlsx&apiname=DownloadeTemplate`}
                        target="_blank" tabIndex="">
                          <img src="images/download.svg" className="markentry__bulkupload__marimg" alt="download image" />
                            Download
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                  {displayUpload?<div className="markentry__bulkupload__entrytemp__dragdrop">
                  <div className="markentry__bulkupload__entrytemp--images">
                    <DragNDrop customStyle={customStyles} onFileChange={this.handleFile} file={this.file} type={this.fileType} />
                  </div>

                  {/* <!-- Upload Progress Bar Strat --> */}
                                                {this.state.uploadstatusbar?<div class="upload__window">
                                                    <div class="upload__name">
                                                        <h4 class="upload__head">file uploading</h4>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="upload__loader">
                                                                <div class="upload__percentage text-right mb-2">25%</div>
                                                                <div class="upload__loader--outline">
                                                                    <div class="upload__loader--inline"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="upload__btn mt-4 mr-2">
                                                                <button class="upload__btn--close"><i class="upload__btn--icon material-icons">close</i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>:""}
                                                {/* <!-- Upload Progress Bar End --> */}

                  <span className="d-block markentry__bulkupload__entrytemp__fileinput">Drag &amp; Drop or
                                                    <label htmlFor="markentry__upload"
                                                           className="markentry__bulkupload__entrytemp__uploadlabel">click to browse</label>
                                                    <input type="file" id="markentry__upload"
                                                           className="markentry__bulkupload__entrytemp__bulkinput" onChange={this.onFileChange} />
                                                </span>
                  <hr className="markentry__hrline" />
                    <div className="markentry__bulkupload__entrytemp__templateformat">
                      <ul className="markentry__bulkupload__entrytemp__templateformat--list">
                        <li className="markentry__bulkupload__entrytemp--listitem markentry__borderright">File
                          format <span className="addstudent__landingtemplateformat__filesvariant">.xls</span>
                        </li>
                        <li className="markentry__bulkupload__entrytemp--listitem">Size max 25Mb</li>
                      </ul>
                    </div>
                </div>:''}

              </div>
            </div>
          </div>
        );
    }
}
const mapStateToProps = (state) => {
//return state;
return {subject:state.courseReducer.summary.length > 0?state.courseReducer.summary[0].Subject:'',
qpName:state.courseReducer.summary.length > 0?state.courseReducer.summary[0].title:'',
lineItemID:state.courseReducer.summary.length > 0?state.courseReducer.summary[0].lineItemId:'',
studentlist:state.studentReducer.studentList,
APIResponseStatus:state.studentReducer.APIResponseStatus&&state.courseReducer.APIResponseStatus?true:false};
};
const mapdispatchtoprops = { getDataForBulkUpload,uploadedDataResponse,bulkUploadCompleteStatus }
export default connect(mapStateToProps,mapdispatchtoprops)(ErrorHandle(PreUpload));
