import React, {Component} from 'react';
import PreUpload from "./PreUpload";
import PostUpload from "./PostUpload";
import './index.css';
import {connect} from 'react-redux';
import ErrorHandle from '../ErrorHandle';

class BulkUpload extends Component {

constructor(props){
    super(props);
}

    // state = {
    //   isUploadComplete:false
    // }
    // uploadComplete = () => this.setState((prevState)=>({isUploadComplete: !prevState.isUploadComplete}));

    render() {
        //const isUploadComplete = false;//comes from props i.e store
        return (
            <div>
                {!this.props.bulkuploadcomplete ? <PreUpload {...this.props} /> : <PostUpload {...this.props}/>}
            </div>
        )
    }
}
const mapState = (state)=>{
    return{bulkuploadcomplete:state.studentReducer.bulkuploadcomplete,APIResponseStatus:state.studentReducer.APIResponseStatus}
}
export default connect(mapState,null)(ErrorHandle(BulkUpload))
