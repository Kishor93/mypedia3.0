import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getStudentData} from '../../../state/studentData/studentAction';
import MarksBulkUpload from "../../features/MarksBulkUpload";
import ErrorHandle from '../ErrorHandle';

class PostUpload extends Component {

    constructor(props) {
        super(props);
        this.state = {
            view: 'success'
        }
    }
    // componentDidMount() {
    //     this.props.getStudentData();
    // }

    render() {
        const {marks} = this.props;
        const marksEntryType = 'assessment';//will come from props

        //Calculate failure and success and separate in two arrays
        const failure = ['failure'], success =['success'];

        return (
            <MarksBulkUpload/>
        )
    }
}
const mapState = (state)=>{
    return {studentList: typeof state.studentReducer.studentList !== 'undefined' ? state.studentReducer.studentList : {},APIResponseStatus:state.studentReducer.APIResponseStatus}
}
export default connect(null,{getStudentData})(ErrorHandle(PostUpload));
