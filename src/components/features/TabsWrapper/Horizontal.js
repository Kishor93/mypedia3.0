import React, {Component} from 'react';
import {Nav, Tab} from "react-bootstrap";
import Index from './index';
import {bulkUploadCompleteFalse} from '../../../state/studentData/studentAction'
import {connect} from 'react-redux';

class Horizontal extends Component {

    constructor(props){
        super(props);
        this.handleSelect = this.handleSelect.bind(this);
    }

 handleSelect(id){
     if(id == 'bulkUpload' && this.props.bulkuploadcomplete)
     this.props.bulkUploadCompleteFalse();
 }


    render() {
        const {id, defaultActiveKey, tabs, navStyles, navLinkStyles,navLinkStylesParent} = this.props;
        return (
            <Tab.Container id={id} defaultActiveKey={defaultActiveKey} onSelect={this.handleSelect}>
                <Nav className={[navStyles].filter(Boolean).join(' ')}>
                    { tabs && tabs.map((tab) => {
                        return (
                            <Nav.Item className={[navLinkStylesParent]}>
                                <Nav.Link className={[navLinkStyles, tab.linkStyle ].filter(Boolean).join(' ')} eventKey={tab.eventKey}>
                                    {tab.navComponent}
                                </Nav.Link>
                            </Nav.Item>
                        );
                    })}
                </Nav>
                <Tab.Content>
                    { tabs && tabs.map((tab)=>{
                        if (tab.contentComponent.tabs) {
                            return (
                                <Tab.Pane eventKey={tab.eventKey}>
                                    <Index data={tab.contentComponent}/>
                                </Tab.Pane>
                            )
                        } else {
                            return (
                                <Tab.Pane eventKey={tab.eventKey}>
                                    {tab.contentComponent}
                                </Tab.Pane>
                            )
                        }
                    })}
                </Tab.Content>
            </Tab.Container>
        )
    }
}

const mapState = (state)=>{
    return{bulkuploadcomplete:state.studentReducer.bulkuploadcomplete}
}

export default connect(mapState, {bulkUploadCompleteFalse})(Horizontal)


