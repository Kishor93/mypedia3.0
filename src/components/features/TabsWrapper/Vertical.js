import React, {Component} from 'react';
import {Nav, Tab, Row, Col} from "react-bootstrap";
import Index from './index';

class Vertical extends Component {

    render() {
        const {id, defaultActiveKey, tabs, navStyles, navLinkStyles, contentCol, navCol,navLinkStylesParent} = this.props;
        return (
            <Tab.Container id={id} defaultActiveKey={defaultActiveKey}>
                <Row>
                    <Col sm={navCol}>
                        <Nav className={[navStyles, "flex-column"].filter(Boolean).join(' ')}>
                            {tabs && tabs.map((tab) => {
                                return (
                                	<Nav.Item className={[navLinkStylesParent]}>
                                        <Nav.Link className={[navLinkStyles, tab.linkStyle ].filter(Boolean).join(' ')} eventKey={tab.eventKey}>
                                            {tab.navComponent}
                                        </Nav.Link>
                                    </Nav.Item>
                                );
                            })}
                        </Nav>
                    </Col>
                    <Col sm={contentCol}>
                        <Tab.Content>
                            {tabs && tabs.map((tab) => {
                                if (tab.contentComponent.tabs) {
                                    return (
                                        <Tab.Pane eventKey={tab.eventKey}>
                                            <Index data={tab.contentComponent}/>
                                        </Tab.Pane>
                                    )
                                } else {
                                    return (
                                        <Tab.Pane eventKey={tab.eventKey}>
                                            {tab.contentComponent}
                                        </Tab.Pane>
                                    )
                                }
                            })}
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        )
    }
}

export default Vertical;

