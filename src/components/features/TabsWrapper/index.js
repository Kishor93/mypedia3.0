import React, {Component} from 'react';
import VerticalTab from "./Vertical";
import Horizontal from "./Horizontal";

class TabsWrapper extends Component {

    render() {
        const {data} = this.props;
        return (
            <div>
                {data && data.orientation === 'vertical' ? <VerticalTab {...data}/> : <Horizontal {...data}/>}
            </div>
        )
    }
}

export default TabsWrapper;

