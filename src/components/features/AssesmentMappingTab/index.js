import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import './index.css';
import {Nav, Tab} from 'react-bootstrap';
import {getAssessmentQpSubject} from '../../../state/assessmentQpList/assessmentQpListAction'
import AssesmentMappingSubjects from '../AssesmentMappingSubjects';
import ErrorHandle from '../ErrorHandle';

class AssesmentDetailsTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedId: 0,
            selectedIndex: 0
        }
    }
    handleSelect = (value) => {
        let [id, index] = value.split('_');
        this.setState({ selectedId: id, selectedIndex: index }, () =>{
            this.props.getAssessmentQpSubject(id)
        })
    }

    render() {
        const { assesmentDetails } = this.props;
        return (
            <Tab.Container id="markentry__tab" defaultActiveKey="tab1">
                <div className="d-flex">
                    <React.Fragment>
                        <Nav className="nav col-2 p-0 flex-column mark__approval__ul" onSelect={this.handleSelect}>
                            {typeof assesmentDetails !== 'undefined' && assesmentDetails.map((values, i) => {
                                return (
                                    <Nav.Item className="mark__approval__li" key={`${values.lineItemId}_${i}`}>
                                        <Nav.Link className="mark__approval__li--link" eventKey={`${values.lineItemId}_${i}`}>
                                            <span className="mark__approval__li--title">{values.assesmentName} </span>
                                            <span className="mark__approval__tab--assessment">{values.assesmentType}</span>
                                            <span className="mark__approval__tab--imgcn"><img src="images/calendar.svg" alt=""/></span>
                                            <span className="mark__approval__tab--date"><p className="mark__approval__tab--datestrt">{values.dateOfExam}</p></span>
                                        </Nav.Link>
                                    </Nav.Item>
                                );
                            })}
                        </Nav>
                        {this.state.selectedId ? <Tab.Content className="col-10">
                                {typeof this.props.assessmentQpSubjects !== 'undefined' && Object.keys(this.props.assessmentQpSubjects).length > 0 ?
                                    typeof assesmentDetails !== 'undefined' && assesmentDetails.map((values, i) => {
                                        return (
                                            <Tab.Pane eventKey={`${values.lineItemId}_${i}`} key={`${values.lineItemId}_${i}`}>
                                                <div className="col-12">
                                                    <div className="col-12">
                                                        <div className="marks--approval__template">
                                                            <div className="row">
                                                            
                                                                <div className="col-lg-4 col-md-4 col-sm-12 col-12 flex-column p-0">
                                                                <span className="marks-approval__subject--number mr-2"> {`${window.className} ${window.sectionName}`}</span>
                                                                    <span className="marks--approval__boldtext" aria-label="">Subjects<span className="marks-approval__subject--number ">{typeof this.props.assessmentQpSubjects.assesmentSubjectDetails !== 'undefined' && this.props.assessmentQpSubjects.assesmentSubjectDetails.subjects.length}</span></span>

                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <AssesmentMappingSubjects assesmentSubjects={this.props.assessmentQpSubjects.assesmentSubjectDetails} lineItemId={this.state.selectedId} key={this.state.selectedId}/>
                                                </div>
                                            </Tab.Pane>
                                        );
                                    }) : ''
                                }

                            </Tab.Content>
                            :
                            <div className="tab-content col-lg-10 col-md-8 col-sm-12 col-12">
                                <section className="student__rpcard col-xl-10 col-lg-10 col-md-10 col-sm-10 col-12">
                                    <section className="student__rpcardnoexam">
                                        <div className="student__rpcardnoexambox">
                                            <p className="student__rptext mb-4">No exam selected</p>
                                            <p className="student__rptext student__rptext--second">Select the exam to view its details</p>
                                            <img className="d-block m-3" src="images/img_downarrow.png" alt="" />
                                        </div>
                                    </section>
                                </section>
                            </div>}
                    </React.Fragment>
                </div>
            </Tab.Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        assessmentQpSubjects: Object.keys(state.assessmentQpListReducer.assessmentQpSubjects).length > 0 ? state.assessmentQpListReducer.assessmentQpSubjects: {},
        APIResponseStatus:state.assessmentQpListReducer.APIResponseStatus?true:false
    };
};
const mapDispatchToprops = { getAssessmentQpSubject }
export default connect(mapStateToProps, mapDispatchToprops)(ErrorHandle(AssesmentDetailsTab));