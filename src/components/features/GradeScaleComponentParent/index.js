import React from "react";
import { connect } from 'react-redux';
import './index.css';
import ErrorHandle from "../ErrorHandle";
import GradeScale from '../GradeScale';
import GradeScaleCategory from '../GradeScaleCategory';
import GradeScaleHeader from '../GradeScaleHeader';
import GradeScaleSubject from '../GradeScaleSubject';
import examList from './exam-list.json';
import headerComponents from './header.json';
import subjectList from './grade-subject-list.json';
import { getGradeScaleData, updateGradeScaleData } from '../../../state/gradeAssignList/gradeAssignAction';
import NotificationPopup from '../NotificationPopup/NotificationPopup';
import NoCategorySelected from '../NoCategorySelected';

class GradeScaleComponentParent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            originalStudentList: JSON.parse(JSON.stringify(this.props.gradeScaleList)),
            backupStudentList: JSON.parse(JSON.stringify(this.props.gradeScaleList)),
            originalSubjectList: JSON.parse(JSON.stringify(subjectList)),
            backupSubjectList: JSON.parse(JSON.stringify(subjectList)),
            studentList: this.props.gradeScaleList,
            examList: examList,
            headerComponents: headerComponents,
            currentCategories: examList.categories.length ? new Array(examList.categories.length).fill(-1) : [],
            notificationPopup: false,
            notify: {
                msg: '',
                status: 1
            },
            subjectList: subjectList,
            currentClass: 0,
        }
    }

    componentDidMount() {
        this.props.getGradeScaleData()
            .then(() => {
                this.setState({
                    studentList: this.props.gradeScaleList,
                    originalStudentList: JSON.parse(JSON.stringify(this.props.gradeScaleList)),
                    backupStudentList: JSON.parse(JSON.stringify(this.props.gradeScaleList)),
                })
            })
    }

    changeStudentList = (newList, mount) => {
        this.setState({
            studentList: newList,
            originalStudentList: mount == "mount" ? JSON.parse(JSON.stringify(newList)) : []
        });
    }

    changeSubjectList = (newList) => {
        this.setState({
            subjectList: newList
        })
    }

    selectClass = (e, index) => {
        this.setState({
            currentClass: index
        });
    }

    closePopup = () => this.setState((prevState) => ({ notificationPopup: !prevState.notificationPopup }));

    handleCategoryChange = (e, level, current) => {
        let newCurrentCategories = this.state.currentCategories;
        newCurrentCategories[level] = current;
        this.setState({
            currentCategories: newCurrentCategories
        })
    }

    handleAssignClick = (e) => {
        let that = this;
        if (this.state.currentCategories[0] !== 2 && this.state.currentCategories[0] !== -1) {
            this.changeScaleStatus("apply");
            setTimeout(() => {
                that.changeScaleStatus("updated");
                this.props.updateGradeScaleData(this.state.studentList);
                if (this.state.currentCategories[0] == 1) {
                    this.setState({
                        notify: {
                            msg: '"6 Point scale" successfully applied to selected classes and sections.',
                            status: 1
                        },
                        notificationPopup: true
                    })
                }
            }, 3000);
        }
        else if (this.state.currentCategories[0] === 2) {
            this.changeSubjectStatus("apply");
            setTimeout(() => {
                that.changeSubjectStatus("updated");
            }, 3000);
        }
    }

    handleCancelClick = (e) => {
        var that = this;
        if (this.state.currentCategories[0] !== 2 && this.state.currentCategories[0] !== -1) {
            this.changeScaleStatus("apply", JSON.parse(JSON.stringify(this.state.originalStudentList)));
            setTimeout(() => {
                this.changeScaleStatus("updated", JSON.parse(JSON.stringify(this.state.originalStudentList)));
            }, 3000);
        }
        else if (this.state.currentCategories[0] === 2) {
            this.changeSubjectStatus("apply", JSON.parse(JSON.stringify(this.state.originalSubjectList)));
            that.forceUpdate();
            setTimeout(() => {
                that.changeSubjectStatus("updated", JSON.parse(JSON.stringify(this.state.originalSubjectList)));
                that.forceUpdate();
            }, 3000);
        }
    }

    changeScaleStatus = (status, originalStudentList) => {
        var newList = originalStudentList ? originalStudentList : this.state.studentList;
        if (newList.length) {
            newList.forEach((classObject, classIndex) => {
                if (classObject.columns.length) {
                    classObject.columns.forEach((column, index) => {
                        if (column.checked === this.state.backupStudentList[classIndex].columns[index].checked) {
                        }
                        else {
                            if (column.checked) {
                                classObject.data[0][column.accessor].status = (status == "apply") ? "apply" : "updated";
                            }
                            else {
                                classObject.data[0][column.accessor].status = (status == "apply") ? "apply" : "set";
                            }
                        }
                    })
                }
            })
        }
        this.setState({
            studentList: newList,
            backupStudentList: status == "updated" ? JSON.parse(JSON.stringify(newList)) : this.state.backupStudentList
        });
    }

    changeSubjectStatus = (status, originalSubjectList) => {
        var newList = originalSubjectList ? JSON.parse(JSON.stringify(originalSubjectList)) : this.state.subjectList;
        if (newList.classes && newList.classes.length) {
            //console.log(this.state.currentClass);
            //console.log(newList.classes[this.state.currentClass]);
            newList.classes[this.state.currentClass] !== undefined && newList.classes[this.state.currentClass].subjects && newList.classes[this.state.currentClass].subjects.length && newList.classes[this.state.currentClass].subjects.forEach((subject, index) => {
                if (subject.checked === this.state.backupSubjectList.classes[this.state.currentClass].subjects[index].checked) {
                    console.log(subject.checked);
                    console.log(this.state.backupSubjectList.classes[this.state.currentClass].subjects[index].checked);
                }
                else {
                    if (subject.checked) {
                        subject.status = (status === "apply") ? "apply" : "updated";
                    }
                    else {
                        subject.status = (status === "apply") ? "apply" : "set";
                    }
                }
            })
        }
        this.setState({
            subjectList: newList,
            backupSubjectList: status == "updated" ? JSON.parse(JSON.stringify(newList)) : this.state.backupSubjectList
        });
    }

    switchGradeAssignViews = (currentCategory) => {
        switch (currentCategory) {
            case -1:
                return <NoCategorySelected />;
            case 0:
                return <GradeScale studentList={this.state.studentList}
                    changeStudentList={this.changeStudentList}
                />;
            case 1:
                return <GradeScale studentList={this.state.studentList}
                    changeStudentList={this.changeStudentList}
                    classSection={true}
                />;
            case 2:
                return <GradeScaleSubject subjectList={this.state.subjectList}
                    changeSubjectList={this.changeSubjectList}
                    currentClass={this.state.currentClass}
                    selectClass={this.selectClass}
                />;
            case 3:
                return <GradeScale studentList={this.state.studentList}
                    changeStudentList={this.changeStudentList}
                />;
            default:
                return <NoCategorySelected />;
        }
    }

    render() {
        return (
            <div className="assign__grade__template">
                {this.state.notificationPopup ? <NotificationPopup text={this.state.notify.msg} closepopup={this.closePopup} status={this.state.notify.status} /> : ""}
                <GradeScaleHeader headerComponents={this.state.headerComponents}
                    handleAssign={this.handleAssignClick}
                    handleCancel={this.handleCancelClick}
                    handleBack={this.props.goBack}
                />
                <GradeScaleCategory examList={this.state.examList}
                    handleCategoryChange={this.handleCategoryChange}
                    currentCategories={this.state.currentCategories}
                />
                {this.switchGradeAssignViews(this.state.currentCategories[0])}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        gradeScaleList: typeof state.gradeAssignReducer !== 'undefined' && state.gradeAssignReducer.gradeScaleList ? state.gradeAssignReducer.gradeScaleList : {},
        APIResponseStatus: state.gradeAssignReducer.APIResponseStatus
    };
};

const mapdispatchtoprops = { getGradeScaleData, updateGradeScaleData };
export default connect(mapStateToProps, mapdispatchtoprops)(ErrorHandle(GradeScaleComponentParent));