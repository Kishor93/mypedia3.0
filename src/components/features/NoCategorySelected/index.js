import React from 'react';
import ErrorHandle from '../ErrorHandle';

class NoCategorySelected extends React.Component {
    render(){
        return(
            <div class="row">
                <div className="col-12">
                    <div className="assign__grade__template__body" id="grade__scale__assign__category__select">
                        <div className="assign__grade__template__body__section" style={{ backgroundImage: 'url(images/bg__report.svg)' }}>
                            <div className="assign__grade__template__body__section__category">
                                <img src="images/category-vector.svg" />
                                <p className="assign__grade__template__body__section__category__text mt-4 mb-5">No category selected</p>
                                <p className="assign__grade__template__body__section__category__section">Select any category to populate the data</p>
                                <img className="d-block assign__grade__template__body__section__category__img" src="images/up-arrow-icon.svg" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ErrorHandle(NoCategorySelected);