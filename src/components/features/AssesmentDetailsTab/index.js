import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import './index.css';
import {Nav, Tab} from 'react-bootstrap';
import {getAssesmentSubjects} from '../../../state/assesmentSubjects/assesmentSubjectAction'
import AssesmentSubjects from '../AssesmentSubjects';
import Popup from '../Popup';
import GenerateReportPopupContent from '../GenerateReportPopupContent';
import StudentReportList from '../../features/StudentReportList';
import {getDataForStudentReport} from '../../../state/studentData/studentAction';
import {getTeacherData} from '../../../state/reportData/reportAction';
import ErrorHandle from '../ErrorHandle';
import FileViewer from '../FileViewer'


class AssesmentDetailsTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            modalPreviewIsOpen: false,
            selectedId: 0,
            selectedIndex: 0
        }
    }
    componentDidMount() {
        this.props.getTeacherData();
    }
    openModal = () => {
        this.setState({ modalIsOpen: true });
    }

    closeModal = () => {
        this.setState({ modalIsOpen: false });
    }
    handleSelect = (value) => {
        let [id, index] = value.split('_');
        this.setState({ selectedId: id, selectedIndex: index }, () =>{
            if(typeof this.props.generateReport === 'function'){
                this.props.getAssesmentSubjects(id)
            }
            else{this.props.getDataForStudentReport(id);this.props.getTeacherData(id);}

        })
    }
    openPreviewModal = () => {
        this.setState({ modalPreviewIsOpen: true });
    }
    closePreviewModal = () => {
        this.setState({ modalPreviewIsOpen: false });
    }

    render() {
        const { assesmentDetails, generateReport, generateReportResponse, teacherDetails, studentDetails } = this.props;
        const { modalIsOpen, modalPreviewIsOpen } = this.state;
        const fileList = [
            {fileName: "Report Template", name: "Report Template", file: "/SchoolNetTeacherPortal/API/serviceprovider?apiname=PreviewReportTemplate", ext: ".pdf"}
        ];
        return (
            <Tab.Container id="markentry__tab" defaultActiveKey="tab1">
                <div className="d-flex">
                    <React.Fragment>
                        <Nav className="nav col-2 p-0 flex-column mark__approval__ul" onSelect={this.handleSelect}>
                            {typeof assesmentDetails !== 'undefined' && assesmentDetails.map((values, i) => {
                                return (
                                    <Nav.Item className="mark__approval__li" key={`${values.lineItemId}_${i}`}>
                                        <Nav.Link className="mark__approval__li--link" eventKey={`${values.lineItemId}_${i}`}>
                                            <span className="mark__approval__li--title">{values.assesmentName} </span>
                                            <span className="mark__approval__tab--assessment">{values.assesmentType}</span>
                                            <span className="mark__approval__tab--imgcn"><img src="images/calendar.svg" alt=""/></span>
                                            <span className="mark__approval__tab--date"><p className="mark__approval__tab--datestrt">{values.dateOfExam}</p></span>
                                        </Nav.Link>
                                    </Nav.Item>
                                );
                            })}
                        </Nav>
                        {this.state.selectedId ? <Tab.Content className="col-10">
                                {typeof this.props.assesmentSubjects !== 'undefined' && Object.keys(this.props.assesmentSubjects).length > 0 && typeof generateReport === 'function' ?
                                    typeof assesmentDetails !== 'undefined' && assesmentDetails.map((values, i) => {
                                        console.log('genReport', typeof generateReport)
                                        return (
                                            <Tab.Pane eventKey={`${values.lineItemId}_${i}`} key={`${values.lineItemId}_${i}`}>
                                                <div className="col-12">
                                                    <div className="col-12">
                                                        <div className="marks--approval__template">
                                                            <div className="row">

                                                                <div className="col-lg-4 col-md-4 col-sm-12 col-12 flex-column p-0">
                                                                <span className="marks-approval__subject--number mr-2"> {`${window.className} ${window.sectionName}`}</span>
                                                                    <span className="marks--approval__boldtext" aria-label="">Subjects<span className="marks-approval__subject--number ">{typeof this.props.assesmentSubjects.assesmentSubjectDetails[0] !== 'undefined' && this.props.assesmentSubjects.assesmentSubjectDetails[0].subjects.length}</span></span>

                                                                </div>
                                                                {Object.keys(teacherDetails).length > 0 ? <div className="col-lg-8 col-md-8 col-sm-12 col-12 p-0 d-flex justify-content-end">
                                                                    <span className="marks--approval__date" aria-label="">Date of Exam <span className="marks-approval__date--value ">{values.dateOfExam}</span></span>
                                                                    <button onClick={this.openPreviewModal} className="btn marks--approval__bluebutton"  data-toggle="modal"
                                                                            data-target="#studentreportPopup">Preview</button>
                                                                    {modalPreviewIsOpen && <FileViewer  modalIsOpen={modalPreviewIsOpen}
                                                                                                        closeModal={this.closePreviewModal}
                                                                                                        fileList={fileList}
                                                                                                        defaultActive={0}/>}
                                                                    <button onClick={this.openModal} className="btn display marks--approval__yellowbutton" disabled={!((_.every(this.props.assesmentSubjects.assesmentSubjectDetails[0].subjects,{'approved':1})) && this.props.assesmentSubjects.assesmentSubjectDetails[0].subjects[0]['report_status'] !== 'requested')}>Generate Report</button>
                                                                    <Popup modalIsOpen={modalIsOpen} closeModal={this.closeModal} id="modal__preview">
                                                                        <GenerateReportPopupContent generateReportResponse={generateReportResponse} generateReport={generateReport} closeModal={this.closeModal} selectedId={this.state.selectedId} reportStatus={this.props.assesmentSubjects.assesmentSubjectDetails[0].subjects[0]['report_status']}/>
                                                                    </Popup>
                                                                </div> : ''}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <AssesmentSubjects assesmentSubjects={this.props.assesmentSubjects.assesmentSubjectDetails} lineItemId={this.state.selectedId} key={this.state.selectedId}/>
                                                </div>
                                            </Tab.Pane>
                                        );
                                    }) : typeof generateReport !== 'function'?<StudentReportList teacherDetails={teacherDetails} lineItemId={this.state.selectedId}  report_generated_status={assesmentDetails[this.state.selectedIndex]['report_generation_status']}/>:''
                                }

                            </Tab.Content>
                            :
                            <div className="tab-content col-lg-10 col-md-8 col-sm-12 col-12">
                                <section className="student__rpcard col-xl-10 col-lg-10 col-md-10 col-sm-10 col-12">
                                    <section className="student__rpcardnoexam">
                                        <div className="student__rpcardnoexambox">
                                            <p className="student__rptext mb-4">No exam selected</p>
                                            <p className="student__rptext student__rptext--second">Select the exam to view its details</p>
                                            <img className="d-block m-3" src="images/img_downarrow.png" alt="" />
                                        </div>
                                    </section>
                                </section>
                            </div>}
                    </React.Fragment>
                </div>
            </Tab.Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        assesmentSubjects: typeof state.assesmentSubjectReducer !== 'undefined' ? state.assesmentSubjectReducer.assesmentSubjects : {},
        teacherDetails: state.reportReducer.teacherDetails ? state.reportReducer.teacherDetails : {},
        generateReportResponse:state.reportReducer.generateReportResponse?state.reportReducer.generateReportResponse:null,
        APIResponseStatus:state.assesmentSubjectReducer.APIResponseStatus && state.reportReducer.APIResponseStatus?true:false
    };
};
const mapDispatchToprops = { getTeacherData, getDataForStudentReport ,getAssesmentSubjects }
export default connect(mapStateToProps, mapDispatchToprops)(ErrorHandle(AssesmentDetailsTab));
