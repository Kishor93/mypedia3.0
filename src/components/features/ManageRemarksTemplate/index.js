import React from 'react';
import TabsWrapper from "../TabsWrapper";
import ErrorHandle from '../ErrorHandle';
import GradeRemarksTemplate from '../GradeRemarksTemplate';

class ManageRemarksTemplate extends React.Component {
    render() {
        const data = {
            orientation: "vertical",
            defaultActiveKey: "gradeRemarks",
            navStyles: "nav-item nav-link nav__tab nav",
            navLinkStyles: "nav-link tab__content--link active",
            navCol: "nav col-lg-3 col-md-4 col-sm-12 col-12 px--0 flex-column",
            contentCol: "tab-content col-lg-9 col-md-8 col-sm-12 col-12 px--0",
            tabs: [
                {
                    "eventKey": "gradeRemarks",
                    "navComponent": "Grade Remarks",
                    "contentComponent": <GradeRemarksTemplate />
                },
                {
                    "eventKey": "subjectRemarks",
                    "navComponent": "Subject Remarks",
                    "contentComponent": "<SubjectRemarks/>"
                },
                {
                    "eventKey": "commonRemarks",
                    "navComponent": "Common Remarks",
                    "contentComponent": "<CommonRemarks/>"
                }
            ]
        };
        return (
            <TabsWrapper data={data} />
        )
    }
}

export default ErrorHandle(ManageRemarksTemplate);
