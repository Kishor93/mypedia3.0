import React, { Component } from 'react';
import ErrorHandle from '../ErrorHandle';
import AccordionWrapper from '../AccordionWrapper';
import AccordionContent from '../AccordionContent';
// import AccordionContent from '../AccordionContent'

class CreateNewRemark extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "data": {
                buttonLabel: 'Create New',
                descText: 'You can create new remark template and use them at corresponding places.',
                ariaLabel: 'Create a new remark template'
            },
            showContent: false
        };
    }

    viewmoredetails = () => this.setState((prevState) => ({ showContent: !prevState.showContent }))

    render() {
        const data = {
            "defaultActiveKey": "Untitled",
            "accStyle": "card",
            "createNew": true,
            "accordions": [
                {
                    "id": "collapseOne",
                    "headerId": "headingOne",
                    "headerTag": "h6",
                    "headingContent": "Untitled",
                    "headerStyles": "remark__heading"
                }
            ]
        };
        return (
            <div>
                <div>
                    <div className="createnew__remarks">
                        <div className="row">
                            <div className="col-12 d-flex">
                                <div className="create__new col-lg-3 col-md-4 col-sm-4 col-5 p-0">
                                    <h6 className="remarks__heading" tabindex="" aria-label={`${this.state.data.ariaLabel}`} onClick={this.viewmoredetails}><i className="material-icons createnew__icon">add</i>{this.state.data.buttonLabel}</h6>
                                </div>
                                <div className="col-lg-9 col-md-8 col-sm-8 col-7 align-self-center justify-content-end">
                                    <p className="createnew__para">{this.state.data.descText}</p>
                                    {/* <AccordionWrapper data={data} />   */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.showContent ?
                    <div className="accordion" id="remarks_accordion_1">
                        <AccordionContent {...data} />
                    </div> : ""
                }
            </div>)
    }

}

export default ErrorHandle(CreateNewRemark);
