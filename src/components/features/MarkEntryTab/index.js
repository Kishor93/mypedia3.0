import React, {Component} from 'react';
import BulkUpload from "../../features/BulkUpload";
import MarksBulkUpload from "../../features/MarksBulkUpload";
import './index.css';
import TabsWrapper from "../TabsWrapper";
import ErrorHandle from '../ErrorHandle';

class MarkEntryTab extends Component {

    render() {
        const data = {
            "orientation": "horizontal",
            "defaultActiveKey": "bulkUpload",
            "navStyles": "markentry__tab",
            "navLinkStyles": "markentry__tablink",
            tabs: [
                {
                    "eventKey": "bulkUpload",
                    "navComponent": "Bulk Upload",
                    "contentComponent": <BulkUpload/>,
                },
                {
                    "eventKey": "enterMarks",
                    "navComponent": "Individual Upload",
                    "contentComponent": <MarksBulkUpload/>
                }
            ]
        };
        return (
            <TabsWrapper data={data}/>
        )
    }
}

export default ErrorHandle(MarkEntryTab);

